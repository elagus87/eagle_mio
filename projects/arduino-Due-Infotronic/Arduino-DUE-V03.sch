<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="7.5.0">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="1" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="4" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="14" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="2" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="5" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="13" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="12" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="1" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="7" fill="1" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="yes"/>
<layer number="101" name="Patch_Top" color="7" fill="1" visible="no" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="no" active="yes"/>
<layer number="103" name="tMap" color="7" fill="1" visible="yes" active="yes"/>
<layer number="104" name="Name" color="7" fill="1" visible="no" active="yes"/>
<layer number="105" name="tPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="106" name="bPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="107" name="Crop" color="7" fill="1" visible="yes" active="yes"/>
<layer number="108" name="fp8" color="7" fill="1" visible="no" active="yes"/>
<layer number="109" name="fp9" color="7" fill="1" visible="no" active="yes"/>
<layer number="110" name="110" color="7" fill="1" visible="yes" active="yes"/>
<layer number="111" name="111" color="7" fill="1" visible="yes" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="113" name="ReferenceLS" color="7" fill="1" visible="yes" active="yes"/>
<layer number="116" name="Patch_BOT" color="7" fill="1" visible="no" active="yes"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="yes" active="yes"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="no" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="no" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="no" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="no" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="no" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="no" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="yes" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="no" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="yes" active="yes"/>
<layer number="199" name="Contour" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="no" active="no"/>
<layer number="201" name="201bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="202" name="202bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="203" name="203bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="204" name="204bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="205" name="205bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="207" name="207bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="208" name="208bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="215" name="215bmp" color="16" fill="1" visible="no" active="yes"/>
<layer number="216" name="216bmp" color="17" fill="1" visible="no" active="yes"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="yes"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="yes"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="yes"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="yes"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="yes"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="yes"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="yes"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="yes"/>
<layer number="248" name="Housing" color="7" fill="1" visible="yes" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="254" name="cooling" color="7" fill="1" visible="no" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="supply1">
<packages>
</packages>
<symbols>
<symbol name="+5V">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<text x="-2.54" y="-5.08" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="+5V" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="GND">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
<symbol name="+3V3">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<text x="-2.54" y="-5.08" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="+3V3" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="+5V" prefix="P+">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="+5V" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="GND" prefix="GND">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="+3V3" prefix="+3V3">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="+3V3" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SmartPrj">
<packages>
<package name="2X18">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-22.86" y1="-1.905" x2="-22.225" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-22.225" y1="-2.54" x2="-20.955" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-20.955" y1="-2.54" x2="-20.32" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-20.32" y1="-1.905" x2="-19.685" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-19.685" y1="-2.54" x2="-18.415" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-18.415" y1="-2.54" x2="-17.78" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-17.78" y1="-1.905" x2="-17.145" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-17.145" y1="-2.54" x2="-15.875" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-15.875" y1="-2.54" x2="-15.24" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-15.24" y1="-1.905" x2="-14.605" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-14.605" y1="-2.54" x2="-13.335" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-13.335" y1="-2.54" x2="-12.7" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="-1.905" x2="-12.065" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-12.065" y1="-2.54" x2="-10.795" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-10.795" y1="-2.54" x2="-10.16" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="-1.905" x2="-9.525" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-9.525" y1="-2.54" x2="-8.255" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-8.255" y1="-2.54" x2="-7.62" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-22.86" y1="-1.905" x2="-22.86" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-22.86" y1="1.905" x2="-22.225" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-22.225" y1="2.54" x2="-20.955" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-20.955" y1="2.54" x2="-20.32" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-20.32" y1="1.905" x2="-19.685" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-19.685" y1="2.54" x2="-18.415" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-18.415" y1="2.54" x2="-17.78" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-17.78" y1="1.905" x2="-17.145" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-17.145" y1="2.54" x2="-15.875" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-15.875" y1="2.54" x2="-15.24" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-15.24" y1="1.905" x2="-14.605" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-14.605" y1="2.54" x2="-13.335" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-13.335" y1="2.54" x2="-12.7" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="1.905" x2="-12.065" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-12.065" y1="2.54" x2="-10.795" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-10.795" y1="2.54" x2="-10.16" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="1.905" x2="-9.525" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-9.525" y1="2.54" x2="-8.255" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-8.255" y1="2.54" x2="-7.62" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-6.985" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-6.985" y1="2.54" x2="-5.715" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="2.54" x2="-5.08" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.905" x2="-4.445" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="2.54" x2="-3.175" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="2.54" x2="-2.54" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="1.905" x2="-1.905" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="2.54" x2="-0.635" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="2.54" x2="0" y2="1.905" width="0.1524" layer="21"/>
<wire x1="0" y1="1.905" x2="0.635" y2="2.54" width="0.1524" layer="21"/>
<wire x1="0.635" y1="2.54" x2="1.905" y2="2.54" width="0.1524" layer="21"/>
<wire x1="1.905" y1="2.54" x2="2.54" y2="1.905" width="0.1524" layer="21"/>
<wire x1="2.54" y1="1.905" x2="3.175" y2="2.54" width="0.1524" layer="21"/>
<wire x1="3.175" y1="2.54" x2="4.445" y2="2.54" width="0.1524" layer="21"/>
<wire x1="4.445" y1="2.54" x2="5.08" y2="1.905" width="0.1524" layer="21"/>
<wire x1="5.08" y1="1.905" x2="5.715" y2="2.54" width="0.1524" layer="21"/>
<wire x1="5.715" y1="2.54" x2="6.985" y2="2.54" width="0.1524" layer="21"/>
<wire x1="7.62" y1="1.905" x2="6.985" y2="2.54" width="0.1524" layer="21"/>
<wire x1="7.62" y1="1.905" x2="8.255" y2="2.54" width="0.1524" layer="21"/>
<wire x1="9.525" y1="2.54" x2="8.255" y2="2.54" width="0.1524" layer="21"/>
<wire x1="9.525" y1="2.54" x2="10.16" y2="1.905" width="0.1524" layer="21"/>
<wire x1="10.16" y1="1.905" x2="10.795" y2="2.54" width="0.1524" layer="21"/>
<wire x1="12.065" y1="2.54" x2="10.795" y2="2.54" width="0.1524" layer="21"/>
<wire x1="12.065" y1="2.54" x2="12.7" y2="1.905" width="0.1524" layer="21"/>
<wire x1="12.7" y1="1.905" x2="13.335" y2="2.54" width="0.1524" layer="21"/>
<wire x1="14.605" y1="2.54" x2="13.335" y2="2.54" width="0.1524" layer="21"/>
<wire x1="14.605" y1="2.54" x2="15.24" y2="1.905" width="0.1524" layer="21"/>
<wire x1="15.24" y1="1.905" x2="15.875" y2="2.54" width="0.1524" layer="21"/>
<wire x1="17.145" y1="2.54" x2="15.875" y2="2.54" width="0.1524" layer="21"/>
<wire x1="17.145" y1="2.54" x2="17.78" y2="1.905" width="0.1524" layer="21"/>
<wire x1="17.78" y1="1.905" x2="18.415" y2="2.54" width="0.1524" layer="21"/>
<wire x1="19.685" y1="2.54" x2="18.415" y2="2.54" width="0.1524" layer="21"/>
<wire x1="19.685" y1="2.54" x2="20.32" y2="1.905" width="0.1524" layer="21"/>
<wire x1="20.32" y1="1.905" x2="20.955" y2="2.54" width="0.1524" layer="21"/>
<wire x1="22.225" y1="2.54" x2="20.955" y2="2.54" width="0.1524" layer="21"/>
<wire x1="22.225" y1="2.54" x2="22.86" y2="1.905" width="0.1524" layer="21"/>
<wire x1="22.86" y1="-1.905" x2="22.225" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="22.225" y1="-2.54" x2="20.955" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="20.32" y1="-1.905" x2="20.955" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="20.32" y1="-1.905" x2="19.685" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="19.685" y1="-2.54" x2="18.415" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="17.78" y1="-1.905" x2="18.415" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="17.78" y1="-1.905" x2="17.145" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="15.875" y1="-2.54" x2="17.145" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="15.875" y1="-2.54" x2="15.24" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="15.24" y1="-1.905" x2="14.605" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="13.335" y1="-2.54" x2="14.605" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="13.335" y1="-2.54" x2="12.7" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="12.7" y1="-1.905" x2="12.065" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="12.065" y1="-2.54" x2="10.795" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="10.16" y1="-1.905" x2="10.795" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="10.16" y1="-1.905" x2="9.525" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="9.525" y1="-2.54" x2="8.255" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="7.62" y1="-1.905" x2="8.255" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="7.62" y1="-1.905" x2="6.985" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="6.985" y1="-2.54" x2="5.715" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-1.905" x2="5.715" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-1.905" x2="4.445" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="4.445" y1="-2.54" x2="3.175" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="3.175" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="1.905" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-2.54" x2="0.635" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="0" y1="-1.905" x2="0.635" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="0" y1="-1.905" x2="-0.635" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-2.54" x2="-1.905" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-1.905" x2="-1.905" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-1.905" x2="-3.175" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="-2.54" x2="-4.445" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-1.905" x2="-4.445" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-1.905" x2="-5.715" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="-2.54" x2="-6.985" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="-1.905" x2="-6.985" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-20.32" y1="1.905" x2="-20.32" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-17.78" y1="1.905" x2="-17.78" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-15.24" y1="1.905" x2="-15.24" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="1.905" x2="-12.7" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="1.905" x2="-10.16" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.905" x2="-5.08" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="1.905" x2="-2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="0" y1="1.905" x2="0" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="2.54" y1="1.905" x2="2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="5.08" y1="1.905" x2="5.08" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="7.62" y1="1.905" x2="7.62" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="10.16" y1="1.905" x2="10.16" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="12.7" y1="1.905" x2="12.7" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="15.24" y1="1.905" x2="15.24" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="17.78" y1="1.905" x2="17.78" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="20.32" y1="1.905" x2="20.32" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="22.86" y1="1.905" x2="22.86" y2="-1.905" width="0.1524" layer="21"/>
<pad name="1" x="-21.59" y="-1.27" drill="0.95" diameter="1.6764" shape="octagon"/>
<pad name="2" x="-21.59" y="1.27" drill="0.95" diameter="1.6764" shape="octagon"/>
<pad name="3" x="-19.05" y="-1.27" drill="0.95" diameter="1.6764" shape="octagon"/>
<pad name="4" x="-19.05" y="1.27" drill="0.95" diameter="1.6764" shape="octagon"/>
<pad name="5" x="-16.51" y="-1.27" drill="0.95" diameter="1.6764" shape="octagon"/>
<pad name="6" x="-16.51" y="1.27" drill="0.95" diameter="1.6764" shape="octagon"/>
<pad name="7" x="-13.97" y="-1.27" drill="0.95" diameter="1.6764" shape="octagon"/>
<pad name="8" x="-13.97" y="1.27" drill="0.95" diameter="1.6764" shape="octagon"/>
<pad name="9" x="-11.43" y="-1.27" drill="0.95" diameter="1.6764" shape="octagon"/>
<pad name="10" x="-11.43" y="1.27" drill="0.95" diameter="1.6764" shape="octagon"/>
<pad name="11" x="-8.89" y="-1.27" drill="0.95" diameter="1.6764" shape="octagon"/>
<pad name="12" x="-8.89" y="1.27" drill="0.95" diameter="1.6764" shape="octagon"/>
<pad name="13" x="-6.35" y="-1.27" drill="0.95" diameter="1.6764" shape="octagon"/>
<pad name="14" x="-6.35" y="1.27" drill="0.95" diameter="1.6764" shape="octagon"/>
<pad name="15" x="-3.81" y="-1.27" drill="0.95" diameter="1.6764" shape="octagon"/>
<pad name="16" x="-3.81" y="1.27" drill="0.95" diameter="1.6764" shape="octagon"/>
<pad name="17" x="-1.27" y="-1.27" drill="0.95" diameter="1.6764" shape="octagon"/>
<pad name="18" x="-1.27" y="1.27" drill="0.95" diameter="1.6764" shape="octagon"/>
<pad name="19" x="1.27" y="-1.27" drill="0.95" diameter="1.6764" shape="octagon"/>
<pad name="20" x="1.27" y="1.27" drill="0.95" diameter="1.6764" shape="octagon"/>
<pad name="21" x="3.81" y="-1.27" drill="0.95" diameter="1.6764" shape="octagon"/>
<pad name="22" x="3.81" y="1.27" drill="0.95" diameter="1.6764" shape="octagon"/>
<pad name="23" x="6.35" y="-1.27" drill="0.95" diameter="1.6764" shape="octagon"/>
<pad name="24" x="6.35" y="1.27" drill="0.95" diameter="1.6764" shape="octagon"/>
<pad name="25" x="8.89" y="-1.27" drill="0.95" diameter="1.6764" shape="octagon"/>
<pad name="26" x="8.89" y="1.27" drill="0.95" diameter="1.6764" shape="octagon"/>
<pad name="27" x="11.43" y="-1.27" drill="0.95" diameter="1.6764" shape="octagon"/>
<pad name="28" x="11.43" y="1.27" drill="0.95" diameter="1.6764" shape="octagon"/>
<pad name="29" x="13.97" y="-1.27" drill="0.95" diameter="1.6764" shape="octagon"/>
<pad name="30" x="13.97" y="1.27" drill="0.95" diameter="1.6764" shape="octagon"/>
<pad name="31" x="16.51" y="-1.27" drill="0.95" diameter="1.6764" shape="octagon"/>
<pad name="32" x="16.51" y="1.27" drill="0.95" diameter="1.6764" shape="octagon"/>
<pad name="33" x="19.05" y="-1.27" drill="0.95" diameter="1.6764" shape="octagon"/>
<pad name="34" x="19.05" y="1.27" drill="0.95" diameter="1.6764" shape="octagon"/>
<pad name="35" x="21.59" y="-1.27" drill="0.95" diameter="1.6764" shape="octagon"/>
<pad name="36" x="21.59" y="1.27" drill="0.95" diameter="1.6764" shape="octagon"/>
<text x="-22.86" y="3.175" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-22.86" y="-4.445" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-21.844" y1="-1.524" x2="-21.336" y2="-1.016" layer="51"/>
<rectangle x1="-21.844" y1="1.016" x2="-21.336" y2="1.524" layer="51"/>
<rectangle x1="-19.304" y1="1.016" x2="-18.796" y2="1.524" layer="51"/>
<rectangle x1="-19.304" y1="-1.524" x2="-18.796" y2="-1.016" layer="51"/>
<rectangle x1="-16.764" y1="1.016" x2="-16.256" y2="1.524" layer="51"/>
<rectangle x1="-16.764" y1="-1.524" x2="-16.256" y2="-1.016" layer="51"/>
<rectangle x1="-14.224" y1="1.016" x2="-13.716" y2="1.524" layer="51"/>
<rectangle x1="-11.684" y1="1.016" x2="-11.176" y2="1.524" layer="51"/>
<rectangle x1="-9.144" y1="1.016" x2="-8.636" y2="1.524" layer="51"/>
<rectangle x1="-14.224" y1="-1.524" x2="-13.716" y2="-1.016" layer="51"/>
<rectangle x1="-11.684" y1="-1.524" x2="-11.176" y2="-1.016" layer="51"/>
<rectangle x1="-9.144" y1="-1.524" x2="-8.636" y2="-1.016" layer="51"/>
<rectangle x1="-6.604" y1="1.016" x2="-6.096" y2="1.524" layer="51"/>
<rectangle x1="-6.604" y1="-1.524" x2="-6.096" y2="-1.016" layer="51"/>
<rectangle x1="-4.064" y1="1.016" x2="-3.556" y2="1.524" layer="51"/>
<rectangle x1="-4.064" y1="-1.524" x2="-3.556" y2="-1.016" layer="51"/>
<rectangle x1="-1.524" y1="1.016" x2="-1.016" y2="1.524" layer="51"/>
<rectangle x1="-1.524" y1="-1.524" x2="-1.016" y2="-1.016" layer="51"/>
<rectangle x1="1.016" y1="1.016" x2="1.524" y2="1.524" layer="51"/>
<rectangle x1="1.016" y1="-1.524" x2="1.524" y2="-1.016" layer="51"/>
<rectangle x1="3.556" y1="1.016" x2="4.064" y2="1.524" layer="51"/>
<rectangle x1="3.556" y1="-1.524" x2="4.064" y2="-1.016" layer="51"/>
<rectangle x1="6.096" y1="1.016" x2="6.604" y2="1.524" layer="51"/>
<rectangle x1="6.096" y1="-1.524" x2="6.604" y2="-1.016" layer="51"/>
<rectangle x1="8.636" y1="1.016" x2="9.144" y2="1.524" layer="51"/>
<rectangle x1="8.636" y1="-1.524" x2="9.144" y2="-1.016" layer="51"/>
<rectangle x1="11.176" y1="1.016" x2="11.684" y2="1.524" layer="51"/>
<rectangle x1="11.176" y1="-1.524" x2="11.684" y2="-1.016" layer="51"/>
<rectangle x1="13.716" y1="1.016" x2="14.224" y2="1.524" layer="51"/>
<rectangle x1="13.716" y1="-1.524" x2="14.224" y2="-1.016" layer="51"/>
<rectangle x1="16.256" y1="1.016" x2="16.764" y2="1.524" layer="51"/>
<rectangle x1="16.256" y1="-1.524" x2="16.764" y2="-1.016" layer="51"/>
<rectangle x1="18.796" y1="1.016" x2="19.304" y2="1.524" layer="51"/>
<rectangle x1="18.796" y1="-1.524" x2="19.304" y2="-1.016" layer="51"/>
<rectangle x1="21.336" y1="1.016" x2="21.844" y2="1.524" layer="51"/>
<rectangle x1="21.336" y1="-1.524" x2="21.844" y2="-1.016" layer="51"/>
</package>
<package name="2X18/90">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-22.86" y1="-1.905" x2="-20.32" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-20.32" y1="-1.905" x2="-20.32" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-20.32" y1="0.635" x2="-22.86" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-22.86" y1="0.635" x2="-22.86" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-21.59" y1="6.985" x2="-21.59" y2="1.27" width="0.762" layer="21"/>
<wire x1="-20.32" y1="-1.905" x2="-17.78" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-17.78" y1="-1.905" x2="-17.78" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-17.78" y1="0.635" x2="-20.32" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-19.05" y1="6.985" x2="-19.05" y2="1.27" width="0.762" layer="21"/>
<wire x1="-17.78" y1="-1.905" x2="-15.24" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-15.24" y1="-1.905" x2="-15.24" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-15.24" y1="0.635" x2="-17.78" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-16.51" y1="6.985" x2="-16.51" y2="1.27" width="0.762" layer="21"/>
<wire x1="-15.24" y1="-1.905" x2="-12.7" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="-1.905" x2="-12.7" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="0.635" x2="-15.24" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-13.97" y1="6.985" x2="-13.97" y2="1.27" width="0.762" layer="21"/>
<wire x1="-12.7" y1="-1.905" x2="-10.16" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="-1.905" x2="-10.16" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="0.635" x2="-12.7" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-11.43" y1="6.985" x2="-11.43" y2="1.27" width="0.762" layer="21"/>
<wire x1="-10.16" y1="-1.905" x2="-7.62" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="-1.905" x2="-7.62" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="0.635" x2="-10.16" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-8.89" y1="6.985" x2="-8.89" y2="1.27" width="0.762" layer="21"/>
<wire x1="-7.62" y1="-1.905" x2="-5.08" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-1.905" x2="-5.08" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0.635" x2="-7.62" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-6.35" y1="6.985" x2="-6.35" y2="1.27" width="0.762" layer="21"/>
<wire x1="-5.08" y1="-1.905" x2="-2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-1.905" x2="-2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="0.635" x2="-5.08" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="6.985" x2="-3.81" y2="1.27" width="0.762" layer="21"/>
<wire x1="-2.54" y1="-1.905" x2="0" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="0" y1="-1.905" x2="0" y2="0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="0.635" x2="-2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="6.985" x2="-1.27" y2="1.27" width="0.762" layer="21"/>
<wire x1="0" y1="-1.905" x2="2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="0.635" x2="0" y2="0.635" width="0.1524" layer="21"/>
<wire x1="1.27" y1="6.985" x2="1.27" y2="1.27" width="0.762" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="5.08" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-1.905" x2="5.08" y2="0.635" width="0.1524" layer="21"/>
<wire x1="5.08" y1="0.635" x2="2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="3.81" y1="6.985" x2="3.81" y2="1.27" width="0.762" layer="21"/>
<wire x1="5.08" y1="-1.905" x2="7.62" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="7.62" y1="-1.905" x2="7.62" y2="0.635" width="0.1524" layer="21"/>
<wire x1="7.62" y1="0.635" x2="5.08" y2="0.635" width="0.1524" layer="21"/>
<wire x1="6.35" y1="6.985" x2="6.35" y2="1.27" width="0.762" layer="21"/>
<wire x1="7.62" y1="-1.905" x2="10.16" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="10.16" y1="-1.905" x2="10.16" y2="0.635" width="0.1524" layer="21"/>
<wire x1="10.16" y1="0.635" x2="7.62" y2="0.635" width="0.1524" layer="21"/>
<wire x1="8.89" y1="6.985" x2="8.89" y2="1.27" width="0.762" layer="21"/>
<wire x1="10.16" y1="-1.905" x2="12.7" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="12.7" y1="-1.905" x2="12.7" y2="0.635" width="0.1524" layer="21"/>
<wire x1="12.7" y1="0.635" x2="10.16" y2="0.635" width="0.1524" layer="21"/>
<wire x1="11.43" y1="6.985" x2="11.43" y2="1.27" width="0.762" layer="21"/>
<wire x1="12.7" y1="-1.905" x2="15.24" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="15.24" y1="-1.905" x2="15.24" y2="0.635" width="0.1524" layer="21"/>
<wire x1="15.24" y1="0.635" x2="12.7" y2="0.635" width="0.1524" layer="21"/>
<wire x1="13.97" y1="6.985" x2="13.97" y2="1.27" width="0.762" layer="21"/>
<wire x1="15.24" y1="-1.905" x2="17.78" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="17.78" y1="-1.905" x2="17.78" y2="0.635" width="0.1524" layer="21"/>
<wire x1="17.78" y1="0.635" x2="15.24" y2="0.635" width="0.1524" layer="21"/>
<wire x1="16.51" y1="6.985" x2="16.51" y2="1.27" width="0.762" layer="21"/>
<wire x1="17.78" y1="-1.905" x2="20.32" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="20.32" y1="-1.905" x2="20.32" y2="0.635" width="0.1524" layer="21"/>
<wire x1="20.32" y1="0.635" x2="17.78" y2="0.635" width="0.1524" layer="21"/>
<wire x1="19.05" y1="6.985" x2="19.05" y2="1.27" width="0.762" layer="21"/>
<wire x1="20.32" y1="-1.905" x2="22.86" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="22.86" y1="-1.905" x2="22.86" y2="0.635" width="0.1524" layer="21"/>
<wire x1="22.86" y1="0.635" x2="20.32" y2="0.635" width="0.1524" layer="21"/>
<wire x1="21.59" y1="6.985" x2="21.59" y2="1.27" width="0.762" layer="21"/>
<pad name="2" x="-21.59" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="4" x="-19.05" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="6" x="-16.51" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="8" x="-13.97" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="10" x="-11.43" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="12" x="-8.89" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="14" x="-6.35" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="16" x="-3.81" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="18" x="-1.27" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="20" x="1.27" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="22" x="3.81" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="24" x="6.35" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="26" x="8.89" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="28" x="11.43" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="30" x="13.97" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="32" x="16.51" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="34" x="19.05" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="36" x="21.59" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="1" x="-21.59" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="3" x="-19.05" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="5" x="-16.51" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="7" x="-13.97" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="9" x="-11.43" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="11" x="-8.89" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="13" x="-6.35" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="15" x="-3.81" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="17" x="-1.27" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="19" x="1.27" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="21" x="3.81" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="23" x="6.35" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="25" x="8.89" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="27" x="11.43" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="29" x="13.97" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="31" x="16.51" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="33" x="19.05" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="35" x="21.59" y="-6.35" drill="1.016" shape="octagon"/>
<text x="-23.495" y="-3.81" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="24.765" y="-3.81" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-21.971" y1="0.635" x2="-21.209" y2="1.143" layer="21"/>
<rectangle x1="-19.431" y1="0.635" x2="-18.669" y2="1.143" layer="21"/>
<rectangle x1="-16.891" y1="0.635" x2="-16.129" y2="1.143" layer="21"/>
<rectangle x1="-14.351" y1="0.635" x2="-13.589" y2="1.143" layer="21"/>
<rectangle x1="-11.811" y1="0.635" x2="-11.049" y2="1.143" layer="21"/>
<rectangle x1="-9.271" y1="0.635" x2="-8.509" y2="1.143" layer="21"/>
<rectangle x1="-6.731" y1="0.635" x2="-5.969" y2="1.143" layer="21"/>
<rectangle x1="-4.191" y1="0.635" x2="-3.429" y2="1.143" layer="21"/>
<rectangle x1="-1.651" y1="0.635" x2="-0.889" y2="1.143" layer="21"/>
<rectangle x1="0.889" y1="0.635" x2="1.651" y2="1.143" layer="21"/>
<rectangle x1="3.429" y1="0.635" x2="4.191" y2="1.143" layer="21"/>
<rectangle x1="5.969" y1="0.635" x2="6.731" y2="1.143" layer="21"/>
<rectangle x1="8.509" y1="0.635" x2="9.271" y2="1.143" layer="21"/>
<rectangle x1="11.049" y1="0.635" x2="11.811" y2="1.143" layer="21"/>
<rectangle x1="13.589" y1="0.635" x2="14.351" y2="1.143" layer="21"/>
<rectangle x1="16.129" y1="0.635" x2="16.891" y2="1.143" layer="21"/>
<rectangle x1="18.669" y1="0.635" x2="19.431" y2="1.143" layer="21"/>
<rectangle x1="21.209" y1="0.635" x2="21.971" y2="1.143" layer="21"/>
<rectangle x1="-21.971" y1="-2.921" x2="-21.209" y2="-1.905" layer="21"/>
<rectangle x1="-19.431" y1="-2.921" x2="-18.669" y2="-1.905" layer="21"/>
<rectangle x1="-21.971" y1="-5.461" x2="-21.209" y2="-4.699" layer="21"/>
<rectangle x1="-21.971" y1="-4.699" x2="-21.209" y2="-2.921" layer="51"/>
<rectangle x1="-19.431" y1="-4.699" x2="-18.669" y2="-2.921" layer="51"/>
<rectangle x1="-19.431" y1="-5.461" x2="-18.669" y2="-4.699" layer="21"/>
<rectangle x1="-16.891" y1="-2.921" x2="-16.129" y2="-1.905" layer="21"/>
<rectangle x1="-14.351" y1="-2.921" x2="-13.589" y2="-1.905" layer="21"/>
<rectangle x1="-16.891" y1="-5.461" x2="-16.129" y2="-4.699" layer="21"/>
<rectangle x1="-16.891" y1="-4.699" x2="-16.129" y2="-2.921" layer="51"/>
<rectangle x1="-14.351" y1="-4.699" x2="-13.589" y2="-2.921" layer="51"/>
<rectangle x1="-14.351" y1="-5.461" x2="-13.589" y2="-4.699" layer="21"/>
<rectangle x1="-11.811" y1="-2.921" x2="-11.049" y2="-1.905" layer="21"/>
<rectangle x1="-11.811" y1="-5.461" x2="-11.049" y2="-4.699" layer="21"/>
<rectangle x1="-11.811" y1="-4.699" x2="-11.049" y2="-2.921" layer="51"/>
<rectangle x1="-9.271" y1="-2.921" x2="-8.509" y2="-1.905" layer="21"/>
<rectangle x1="-6.731" y1="-2.921" x2="-5.969" y2="-1.905" layer="21"/>
<rectangle x1="-9.271" y1="-5.461" x2="-8.509" y2="-4.699" layer="21"/>
<rectangle x1="-9.271" y1="-4.699" x2="-8.509" y2="-2.921" layer="51"/>
<rectangle x1="-6.731" y1="-4.699" x2="-5.969" y2="-2.921" layer="51"/>
<rectangle x1="-6.731" y1="-5.461" x2="-5.969" y2="-4.699" layer="21"/>
<rectangle x1="-4.191" y1="-2.921" x2="-3.429" y2="-1.905" layer="21"/>
<rectangle x1="-1.651" y1="-2.921" x2="-0.889" y2="-1.905" layer="21"/>
<rectangle x1="-4.191" y1="-5.461" x2="-3.429" y2="-4.699" layer="21"/>
<rectangle x1="-4.191" y1="-4.699" x2="-3.429" y2="-2.921" layer="51"/>
<rectangle x1="-1.651" y1="-4.699" x2="-0.889" y2="-2.921" layer="51"/>
<rectangle x1="-1.651" y1="-5.461" x2="-0.889" y2="-4.699" layer="21"/>
<rectangle x1="0.889" y1="-2.921" x2="1.651" y2="-1.905" layer="21"/>
<rectangle x1="0.889" y1="-5.461" x2="1.651" y2="-4.699" layer="21"/>
<rectangle x1="0.889" y1="-4.699" x2="1.651" y2="-2.921" layer="51"/>
<rectangle x1="3.429" y1="-2.921" x2="4.191" y2="-1.905" layer="21"/>
<rectangle x1="5.969" y1="-2.921" x2="6.731" y2="-1.905" layer="21"/>
<rectangle x1="3.429" y1="-5.461" x2="4.191" y2="-4.699" layer="21"/>
<rectangle x1="3.429" y1="-4.699" x2="4.191" y2="-2.921" layer="51"/>
<rectangle x1="5.969" y1="-4.699" x2="6.731" y2="-2.921" layer="51"/>
<rectangle x1="5.969" y1="-5.461" x2="6.731" y2="-4.699" layer="21"/>
<rectangle x1="8.509" y1="-2.921" x2="9.271" y2="-1.905" layer="21"/>
<rectangle x1="11.049" y1="-2.921" x2="11.811" y2="-1.905" layer="21"/>
<rectangle x1="8.509" y1="-5.461" x2="9.271" y2="-4.699" layer="21"/>
<rectangle x1="8.509" y1="-4.699" x2="9.271" y2="-2.921" layer="51"/>
<rectangle x1="11.049" y1="-4.699" x2="11.811" y2="-2.921" layer="51"/>
<rectangle x1="11.049" y1="-5.461" x2="11.811" y2="-4.699" layer="21"/>
<rectangle x1="13.589" y1="-2.921" x2="14.351" y2="-1.905" layer="21"/>
<rectangle x1="13.589" y1="-5.461" x2="14.351" y2="-4.699" layer="21"/>
<rectangle x1="13.589" y1="-4.699" x2="14.351" y2="-2.921" layer="51"/>
<rectangle x1="16.129" y1="-2.921" x2="16.891" y2="-1.905" layer="21"/>
<rectangle x1="18.669" y1="-2.921" x2="19.431" y2="-1.905" layer="21"/>
<rectangle x1="16.129" y1="-5.461" x2="16.891" y2="-4.699" layer="21"/>
<rectangle x1="16.129" y1="-4.699" x2="16.891" y2="-2.921" layer="51"/>
<rectangle x1="18.669" y1="-4.699" x2="19.431" y2="-2.921" layer="51"/>
<rectangle x1="18.669" y1="-5.461" x2="19.431" y2="-4.699" layer="21"/>
<rectangle x1="21.209" y1="-2.921" x2="21.971" y2="-1.905" layer="21"/>
<rectangle x1="21.209" y1="-5.461" x2="21.971" y2="-4.699" layer="21"/>
<rectangle x1="21.209" y1="-4.699" x2="21.971" y2="-2.921" layer="51"/>
</package>
<package name="2X03">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-3.81" y1="-1.905" x2="-3.175" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="-2.54" x2="-1.27" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-1.905" x2="-0.635" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-2.54" x2="1.27" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-1.905" x2="-3.81" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="1.905" x2="-3.175" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="2.54" x2="-1.905" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="2.54" x2="-1.27" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="1.905" x2="-0.635" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="2.54" x2="0.635" y2="2.54" width="0.1524" layer="21"/>
<wire x1="0.635" y1="2.54" x2="1.27" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="1.905" x2="-1.27" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="1.27" y1="1.905" x2="1.27" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-2.54" x2="0.635" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="-2.54" x2="-1.905" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-1.905" x2="1.905" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-2.54" x2="3.81" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="1.27" y1="1.905" x2="1.905" y2="2.54" width="0.1524" layer="21"/>
<wire x1="1.905" y1="2.54" x2="3.175" y2="2.54" width="0.1524" layer="21"/>
<wire x1="3.175" y1="2.54" x2="3.81" y2="1.905" width="0.1524" layer="21"/>
<wire x1="3.81" y1="1.905" x2="3.81" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-2.54" x2="3.175" y2="-2.54" width="0.1524" layer="21"/>
<pad name="1" x="-2.54" y="-1.27" drill="0.95" diameter="1.55" shape="octagon"/>
<pad name="2" x="-2.54" y="1.27" drill="0.95" diameter="1.55" shape="octagon"/>
<pad name="3" x="0" y="-1.27" drill="0.95" diameter="1.55" shape="octagon"/>
<pad name="4" x="0" y="1.27" drill="0.95" diameter="1.55" shape="octagon"/>
<pad name="5" x="2.54" y="-1.27" drill="0.95" diameter="1.55" shape="octagon"/>
<pad name="6" x="2.54" y="1.27" drill="0.95" diameter="1.55" shape="octagon"/>
<text x="-3.81" y="3.175" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.81" y="-4.445" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.794" y1="-1.524" x2="-2.286" y2="-1.016" layer="51"/>
<rectangle x1="-2.794" y1="1.016" x2="-2.286" y2="1.524" layer="51"/>
<rectangle x1="-0.254" y1="1.016" x2="0.254" y2="1.524" layer="51"/>
<rectangle x1="-0.254" y1="-1.524" x2="0.254" y2="-1.016" layer="51"/>
<rectangle x1="2.286" y1="1.016" x2="2.794" y2="1.524" layer="51"/>
<rectangle x1="2.286" y1="-1.524" x2="2.794" y2="-1.016" layer="51"/>
</package>
<package name="FIDUCIA-MOUNT">
<circle x="0" y="0" radius="0.5" width="2.1844" layer="29"/>
<circle x="0" y="0" radius="1.5" width="0.127" layer="41"/>
<smd name="P$1" x="0" y="0" dx="1.016" dy="1.016" layer="1" roundness="100"/>
</package>
<package name="1X08">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="5.715" y1="1.27" x2="6.985" y2="1.27" width="0.1524" layer="21"/>
<wire x1="6.985" y1="1.27" x2="7.62" y2="0.635" width="0.1524" layer="21"/>
<wire x1="7.62" y1="0.635" x2="7.62" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="7.62" y1="-0.635" x2="6.985" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="2.54" y1="0.635" x2="3.175" y2="1.27" width="0.1524" layer="21"/>
<wire x1="3.175" y1="1.27" x2="4.445" y2="1.27" width="0.1524" layer="21"/>
<wire x1="4.445" y1="1.27" x2="5.08" y2="0.635" width="0.1524" layer="21"/>
<wire x1="5.08" y1="0.635" x2="5.08" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-0.635" x2="4.445" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="4.445" y1="-1.27" x2="3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="2.54" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="5.715" y1="1.27" x2="5.08" y2="0.635" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-0.635" x2="5.715" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="6.985" y1="-1.27" x2="5.715" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="1.27" x2="-0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0" y2="0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="0.635" x2="0" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="-0.635" x2="-0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0" y1="0.635" x2="0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.905" y2="1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="1.27" x2="2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="0.635" x2="2.54" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-0.635" x2="1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="0" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0.635" x2="-4.445" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="1.27" x2="-3.175" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="1.27" x2="-2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="0.635" x2="-2.54" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-0.635" x2="-3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="-1.27" x2="-4.445" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="-1.27" x2="-5.08" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="1.27" x2="-2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-0.635" x2="-1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-1.27" x2="-1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-9.525" y1="1.27" x2="-8.255" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-8.255" y1="1.27" x2="-7.62" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="0.635" x2="-7.62" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="-0.635" x2="-8.255" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="0.635" x2="-6.985" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-6.985" y1="1.27" x2="-5.715" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="1.27" x2="-5.08" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0.635" x2="-5.08" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-0.635" x2="-5.715" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="-1.27" x2="-6.985" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-6.985" y1="-1.27" x2="-7.62" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="0.635" x2="-10.16" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-9.525" y1="1.27" x2="-10.16" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="-0.635" x2="-9.525" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-8.255" y1="-1.27" x2="-9.525" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="8.255" y1="1.27" x2="9.525" y2="1.27" width="0.1524" layer="21"/>
<wire x1="9.525" y1="1.27" x2="10.16" y2="0.635" width="0.1524" layer="21"/>
<wire x1="10.16" y1="0.635" x2="10.16" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="10.16" y1="-0.635" x2="9.525" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="8.255" y1="1.27" x2="7.62" y2="0.635" width="0.1524" layer="21"/>
<wire x1="7.62" y1="-0.635" x2="8.255" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="9.525" y1="-1.27" x2="8.255" y2="-1.27" width="0.1524" layer="21"/>
<pad name="1" x="-8.89" y="0" drill="0.85" diameter="1.4224" shape="long" rot="R90"/>
<pad name="2" x="-6.35" y="0" drill="0.85" diameter="1.4224" shape="long" rot="R90"/>
<pad name="3" x="-3.81" y="0" drill="0.85" diameter="1.4224" shape="long" rot="R90"/>
<pad name="4" x="-1.27" y="0" drill="0.85" diameter="1.4224" shape="long" rot="R90"/>
<pad name="5" x="1.27" y="0" drill="0.85" diameter="1.4224" shape="long" rot="R90"/>
<pad name="6" x="3.81" y="0" drill="0.85" diameter="1.4224" shape="long" rot="R90"/>
<pad name="7" x="6.35" y="0" drill="0.85" diameter="1.4224" shape="long" rot="R90"/>
<pad name="8" x="8.89" y="0" drill="0.85" diameter="1.4224" shape="long" rot="R90"/>
<text x="-10.2362" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-10.16" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="6.096" y1="-0.254" x2="6.604" y2="0.254" layer="51"/>
<rectangle x1="3.556" y1="-0.254" x2="4.064" y2="0.254" layer="51"/>
<rectangle x1="1.016" y1="-0.254" x2="1.524" y2="0.254" layer="51"/>
<rectangle x1="-1.524" y1="-0.254" x2="-1.016" y2="0.254" layer="51"/>
<rectangle x1="-4.064" y1="-0.254" x2="-3.556" y2="0.254" layer="51"/>
<rectangle x1="-6.604" y1="-0.254" x2="-6.096" y2="0.254" layer="51"/>
<rectangle x1="-9.144" y1="-0.254" x2="-8.636" y2="0.254" layer="51"/>
<rectangle x1="8.636" y1="-0.254" x2="9.144" y2="0.254" layer="51"/>
</package>
<package name="1X08-TH">
<wire x1="5.715" y1="1.27" x2="6.985" y2="1.27" width="0.1524" layer="21"/>
<wire x1="6.985" y1="1.27" x2="7.62" y2="0.635" width="0.1524" layer="21"/>
<wire x1="7.62" y1="0.635" x2="7.62" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="7.62" y1="-0.635" x2="6.985" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="2.54" y1="0.635" x2="3.175" y2="1.27" width="0.1524" layer="21"/>
<wire x1="3.175" y1="1.27" x2="4.445" y2="1.27" width="0.1524" layer="21"/>
<wire x1="4.445" y1="1.27" x2="5.08" y2="0.635" width="0.1524" layer="21"/>
<wire x1="5.08" y1="0.635" x2="5.08" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-0.635" x2="4.445" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="4.445" y1="-1.27" x2="3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="2.54" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="5.715" y1="1.27" x2="5.08" y2="0.635" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-0.635" x2="5.715" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="6.985" y1="-1.27" x2="5.715" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="1.27" x2="-0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0" y2="0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="0.635" x2="0" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="-0.635" x2="-0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0" y1="0.635" x2="0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.905" y2="1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="1.27" x2="2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="0.635" x2="2.54" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-0.635" x2="1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="0" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0.635" x2="-4.445" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="1.27" x2="-3.175" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="1.27" x2="-2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="0.635" x2="-2.54" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-0.635" x2="-3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="-1.27" x2="-4.445" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="-1.27" x2="-5.08" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="1.27" x2="-2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-0.635" x2="-1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-1.27" x2="-1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-9.525" y1="1.27" x2="-8.255" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-8.255" y1="1.27" x2="-7.62" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="0.635" x2="-7.62" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="-0.635" x2="-8.255" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="0.635" x2="-6.985" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-6.985" y1="1.27" x2="-5.715" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="1.27" x2="-5.08" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0.635" x2="-5.08" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-0.635" x2="-5.715" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="-1.27" x2="-6.985" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-6.985" y1="-1.27" x2="-7.62" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="0.635" x2="-10.16" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-9.525" y1="1.27" x2="-10.16" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="-0.635" x2="-9.525" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-8.255" y1="-1.27" x2="-9.525" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="8.255" y1="1.27" x2="9.525" y2="1.27" width="0.1524" layer="21"/>
<wire x1="9.525" y1="1.27" x2="10.16" y2="0.635" width="0.1524" layer="21"/>
<wire x1="10.16" y1="0.635" x2="10.16" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="10.16" y1="-0.635" x2="9.525" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="8.255" y1="1.27" x2="7.62" y2="0.635" width="0.1524" layer="21"/>
<wire x1="7.62" y1="-0.635" x2="8.255" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="9.525" y1="-1.27" x2="8.255" y2="-1.27" width="0.1524" layer="21"/>
<pad name="1" x="-8.89" y="0" drill="0.95" diameter="1.4224" shape="long" rot="R90"/>
<pad name="2" x="-6.35" y="0" drill="0.95" diameter="1.4224" shape="long" rot="R90"/>
<pad name="3" x="-3.81" y="0" drill="0.95" diameter="1.4224" shape="long" rot="R90"/>
<pad name="4" x="-1.27" y="0" drill="0.95" diameter="1.4224" shape="long" rot="R90"/>
<pad name="5" x="1.27" y="0" drill="0.95" diameter="1.4224" shape="long" rot="R90"/>
<pad name="6" x="3.81" y="0" drill="0.95" diameter="1.4224" shape="long" rot="R90"/>
<pad name="7" x="6.35" y="0" drill="0.95" diameter="1.4224" shape="long" rot="R90"/>
<pad name="8" x="8.89" y="0" drill="0.95" diameter="1.4224" shape="long" rot="R90"/>
<text x="-10.2362" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-10.16" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="6.096" y1="-0.254" x2="6.604" y2="0.254" layer="51"/>
<rectangle x1="3.556" y1="-0.254" x2="4.064" y2="0.254" layer="51"/>
<rectangle x1="1.016" y1="-0.254" x2="1.524" y2="0.254" layer="51"/>
<rectangle x1="-1.524" y1="-0.254" x2="-1.016" y2="0.254" layer="51"/>
<rectangle x1="-4.064" y1="-0.254" x2="-3.556" y2="0.254" layer="51"/>
<rectangle x1="-6.604" y1="-0.254" x2="-6.096" y2="0.254" layer="51"/>
<rectangle x1="-9.144" y1="-0.254" x2="-8.636" y2="0.254" layer="51"/>
<rectangle x1="8.636" y1="-0.254" x2="9.144" y2="0.254" layer="51"/>
</package>
<package name="1X10">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="7.62" y1="0.635" x2="8.255" y2="1.27" width="0.1524" layer="21"/>
<wire x1="8.255" y1="1.27" x2="9.525" y2="1.27" width="0.1524" layer="21"/>
<wire x1="9.525" y1="1.27" x2="10.16" y2="0.635" width="0.1524" layer="21"/>
<wire x1="10.16" y1="0.635" x2="10.16" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="10.16" y1="-0.635" x2="9.525" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="9.525" y1="-1.27" x2="8.255" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="8.255" y1="-1.27" x2="7.62" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="3.175" y1="1.27" x2="4.445" y2="1.27" width="0.1524" layer="21"/>
<wire x1="4.445" y1="1.27" x2="5.08" y2="0.635" width="0.1524" layer="21"/>
<wire x1="5.08" y1="0.635" x2="5.08" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-0.635" x2="4.445" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="5.08" y1="0.635" x2="5.715" y2="1.27" width="0.1524" layer="21"/>
<wire x1="5.715" y1="1.27" x2="6.985" y2="1.27" width="0.1524" layer="21"/>
<wire x1="6.985" y1="1.27" x2="7.62" y2="0.635" width="0.1524" layer="21"/>
<wire x1="7.62" y1="0.635" x2="7.62" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="7.62" y1="-0.635" x2="6.985" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="6.985" y1="-1.27" x2="5.715" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="5.715" y1="-1.27" x2="5.08" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="0.635" x2="0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.905" y2="1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="1.27" x2="2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="0.635" x2="2.54" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-0.635" x2="1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="0" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="3.175" y1="1.27" x2="2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-0.635" x2="3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="4.445" y1="-1.27" x2="3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="1.27" x2="-3.175" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="1.27" x2="-2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="0.635" x2="-2.54" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-0.635" x2="-3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="0.635" x2="-1.905" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="1.27" x2="-0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0" y2="0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="0.635" x2="0" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="-0.635" x2="-0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-1.27" x2="-1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="-1.27" x2="-2.54" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="0.635" x2="-6.985" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-6.985" y1="1.27" x2="-5.715" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="1.27" x2="-5.08" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0.635" x2="-5.08" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-0.635" x2="-5.715" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="-1.27" x2="-6.985" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-6.985" y1="-1.27" x2="-7.62" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="1.27" x2="-5.08" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-0.635" x2="-4.445" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="-1.27" x2="-4.445" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-12.065" y1="1.27" x2="-10.795" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-10.795" y1="1.27" x2="-10.16" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="0.635" x2="-10.16" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="-0.635" x2="-10.795" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="0.635" x2="-9.525" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-9.525" y1="1.27" x2="-8.255" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-8.255" y1="1.27" x2="-7.62" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="0.635" x2="-7.62" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="-0.635" x2="-8.255" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-8.255" y1="-1.27" x2="-9.525" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-9.525" y1="-1.27" x2="-10.16" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="0.635" x2="-12.7" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-12.065" y1="1.27" x2="-12.7" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="-0.635" x2="-12.065" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-10.795" y1="-1.27" x2="-12.065" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="10.795" y1="1.27" x2="12.065" y2="1.27" width="0.1524" layer="21"/>
<wire x1="12.065" y1="1.27" x2="12.7" y2="0.635" width="0.1524" layer="21"/>
<wire x1="12.7" y1="0.635" x2="12.7" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="12.7" y1="-0.635" x2="12.065" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="10.795" y1="1.27" x2="10.16" y2="0.635" width="0.1524" layer="21"/>
<wire x1="10.16" y1="-0.635" x2="10.795" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="12.065" y1="-1.27" x2="10.795" y2="-1.27" width="0.1524" layer="21"/>
<pad name="1" x="-11.43" y="0" drill="0.85" diameter="1.4224" shape="long" rot="R90"/>
<pad name="2" x="-8.89" y="0" drill="0.85" diameter="1.4224" shape="long" rot="R90"/>
<pad name="3" x="-6.35" y="0" drill="0.85" diameter="1.4224" shape="long" rot="R90"/>
<pad name="4" x="-3.81" y="0" drill="0.85" diameter="1.4224" shape="long" rot="R90"/>
<pad name="5" x="-1.27" y="0" drill="0.85" diameter="1.4224" shape="long" rot="R90"/>
<pad name="6" x="1.27" y="0" drill="0.85" diameter="1.4224" shape="long" rot="R90"/>
<pad name="7" x="3.81" y="0" drill="0.85" diameter="1.4224" shape="long" rot="R90"/>
<pad name="8" x="6.35" y="0" drill="0.85" diameter="1.4224" shape="long" rot="R90"/>
<pad name="9" x="8.89" y="0" drill="0.85" diameter="1.4224" shape="long" rot="R90"/>
<pad name="10" x="11.43" y="0" drill="0.85" diameter="1.4224" shape="long" rot="R90"/>
<text x="-12.7762" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-12.7" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="8.636" y1="-0.254" x2="9.144" y2="0.254" layer="51"/>
<rectangle x1="6.096" y1="-0.254" x2="6.604" y2="0.254" layer="51"/>
<rectangle x1="3.556" y1="-0.254" x2="4.064" y2="0.254" layer="51"/>
<rectangle x1="1.016" y1="-0.254" x2="1.524" y2="0.254" layer="51"/>
<rectangle x1="-1.524" y1="-0.254" x2="-1.016" y2="0.254" layer="51"/>
<rectangle x1="-4.064" y1="-0.254" x2="-3.556" y2="0.254" layer="51"/>
<rectangle x1="-6.604" y1="-0.254" x2="-6.096" y2="0.254" layer="51"/>
<rectangle x1="-9.144" y1="-0.254" x2="-8.636" y2="0.254" layer="51"/>
<rectangle x1="-11.684" y1="-0.254" x2="-11.176" y2="0.254" layer="51"/>
<rectangle x1="11.176" y1="-0.254" x2="11.684" y2="0.254" layer="51"/>
</package>
<package name="1X10/90">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-12.7" y1="-1.905" x2="-10.16" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="-1.905" x2="-10.16" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="0.635" x2="-12.7" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="0.635" x2="-12.7" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-11.43" y1="6.985" x2="-11.43" y2="1.27" width="0.762" layer="21"/>
<wire x1="-10.16" y1="-1.905" x2="-7.62" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="-1.905" x2="-7.62" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="0.635" x2="-10.16" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-8.89" y1="6.985" x2="-8.89" y2="1.27" width="0.762" layer="21"/>
<wire x1="-7.62" y1="-1.905" x2="-5.08" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-1.905" x2="-5.08" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0.635" x2="-7.62" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-6.35" y1="6.985" x2="-6.35" y2="1.27" width="0.762" layer="21"/>
<wire x1="-5.08" y1="-1.905" x2="-2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-1.905" x2="-2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="0.635" x2="-5.08" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="6.985" x2="-3.81" y2="1.27" width="0.762" layer="21"/>
<wire x1="-2.54" y1="-1.905" x2="0" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="0" y1="-1.905" x2="0" y2="0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="0.635" x2="-2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="6.985" x2="-1.27" y2="1.27" width="0.762" layer="21"/>
<wire x1="0" y1="-1.905" x2="2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="0.635" x2="0" y2="0.635" width="0.1524" layer="21"/>
<wire x1="1.27" y1="6.985" x2="1.27" y2="1.27" width="0.762" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="5.08" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-1.905" x2="5.08" y2="0.635" width="0.1524" layer="21"/>
<wire x1="5.08" y1="0.635" x2="2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="3.81" y1="6.985" x2="3.81" y2="1.27" width="0.762" layer="21"/>
<wire x1="5.08" y1="-1.905" x2="7.62" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="7.62" y1="-1.905" x2="7.62" y2="0.635" width="0.1524" layer="21"/>
<wire x1="7.62" y1="0.635" x2="5.08" y2="0.635" width="0.1524" layer="21"/>
<wire x1="6.35" y1="6.985" x2="6.35" y2="1.27" width="0.762" layer="21"/>
<wire x1="7.62" y1="-1.905" x2="10.16" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="10.16" y1="-1.905" x2="10.16" y2="0.635" width="0.1524" layer="21"/>
<wire x1="10.16" y1="0.635" x2="7.62" y2="0.635" width="0.1524" layer="21"/>
<wire x1="8.89" y1="6.985" x2="8.89" y2="1.27" width="0.762" layer="21"/>
<wire x1="10.16" y1="-1.905" x2="12.7" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="12.7" y1="-1.905" x2="12.7" y2="0.635" width="0.1524" layer="21"/>
<wire x1="12.7" y1="0.635" x2="10.16" y2="0.635" width="0.1524" layer="21"/>
<wire x1="11.43" y1="6.985" x2="11.43" y2="1.27" width="0.762" layer="21"/>
<pad name="1" x="-11.43" y="-3.81" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="-8.89" y="-3.81" drill="1.016" shape="long" rot="R90"/>
<pad name="3" x="-6.35" y="-3.81" drill="1.016" shape="long" rot="R90"/>
<pad name="4" x="-3.81" y="-3.81" drill="1.016" shape="long" rot="R90"/>
<pad name="5" x="-1.27" y="-3.81" drill="1.016" shape="long" rot="R90"/>
<pad name="6" x="1.27" y="-3.81" drill="1.016" shape="long" rot="R90"/>
<pad name="7" x="3.81" y="-3.81" drill="1.016" shape="long" rot="R90"/>
<pad name="8" x="6.35" y="-3.81" drill="1.016" shape="long" rot="R90"/>
<pad name="9" x="8.89" y="-3.81" drill="1.016" shape="long" rot="R90"/>
<pad name="10" x="11.43" y="-3.81" drill="1.016" shape="long" rot="R90"/>
<text x="-13.335" y="-3.81" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="14.605" y="-4.445" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-11.811" y1="0.635" x2="-11.049" y2="1.143" layer="21"/>
<rectangle x1="-9.271" y1="0.635" x2="-8.509" y2="1.143" layer="21"/>
<rectangle x1="-6.731" y1="0.635" x2="-5.969" y2="1.143" layer="21"/>
<rectangle x1="-4.191" y1="0.635" x2="-3.429" y2="1.143" layer="21"/>
<rectangle x1="-1.651" y1="0.635" x2="-0.889" y2="1.143" layer="21"/>
<rectangle x1="0.889" y1="0.635" x2="1.651" y2="1.143" layer="21"/>
<rectangle x1="3.429" y1="0.635" x2="4.191" y2="1.143" layer="21"/>
<rectangle x1="5.969" y1="0.635" x2="6.731" y2="1.143" layer="21"/>
<rectangle x1="8.509" y1="0.635" x2="9.271" y2="1.143" layer="21"/>
<rectangle x1="11.049" y1="0.635" x2="11.811" y2="1.143" layer="21"/>
<rectangle x1="-11.811" y1="-2.921" x2="-11.049" y2="-1.905" layer="21"/>
<rectangle x1="-9.271" y1="-2.921" x2="-8.509" y2="-1.905" layer="21"/>
<rectangle x1="-6.731" y1="-2.921" x2="-5.969" y2="-1.905" layer="21"/>
<rectangle x1="-4.191" y1="-2.921" x2="-3.429" y2="-1.905" layer="21"/>
<rectangle x1="-1.651" y1="-2.921" x2="-0.889" y2="-1.905" layer="21"/>
<rectangle x1="0.889" y1="-2.921" x2="1.651" y2="-1.905" layer="21"/>
<rectangle x1="3.429" y1="-2.921" x2="4.191" y2="-1.905" layer="21"/>
<rectangle x1="5.969" y1="-2.921" x2="6.731" y2="-1.905" layer="21"/>
<rectangle x1="8.509" y1="-2.921" x2="9.271" y2="-1.905" layer="21"/>
<rectangle x1="11.049" y1="-2.921" x2="11.811" y2="-1.905" layer="21"/>
</package>
<package name="1X10-TH">
<wire x1="7.62" y1="0.635" x2="8.255" y2="1.27" width="0.1524" layer="21"/>
<wire x1="8.255" y1="1.27" x2="9.525" y2="1.27" width="0.1524" layer="21"/>
<wire x1="9.525" y1="1.27" x2="10.16" y2="0.635" width="0.1524" layer="21"/>
<wire x1="10.16" y1="0.635" x2="10.16" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="10.16" y1="-0.635" x2="9.525" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="9.525" y1="-1.27" x2="8.255" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="8.255" y1="-1.27" x2="7.62" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="3.175" y1="1.27" x2="4.445" y2="1.27" width="0.1524" layer="21"/>
<wire x1="4.445" y1="1.27" x2="5.08" y2="0.635" width="0.1524" layer="21"/>
<wire x1="5.08" y1="0.635" x2="5.08" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-0.635" x2="4.445" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="5.08" y1="0.635" x2="5.715" y2="1.27" width="0.1524" layer="21"/>
<wire x1="5.715" y1="1.27" x2="6.985" y2="1.27" width="0.1524" layer="21"/>
<wire x1="6.985" y1="1.27" x2="7.62" y2="0.635" width="0.1524" layer="21"/>
<wire x1="7.62" y1="0.635" x2="7.62" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="7.62" y1="-0.635" x2="6.985" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="6.985" y1="-1.27" x2="5.715" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="5.715" y1="-1.27" x2="5.08" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="0.635" x2="0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.905" y2="1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="1.27" x2="2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="0.635" x2="2.54" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-0.635" x2="1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="0" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="3.175" y1="1.27" x2="2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-0.635" x2="3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="4.445" y1="-1.27" x2="3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="1.27" x2="-3.175" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="1.27" x2="-2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="0.635" x2="-2.54" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-0.635" x2="-3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="0.635" x2="-1.905" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="1.27" x2="-0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0" y2="0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="0.635" x2="0" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="-0.635" x2="-0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-1.27" x2="-1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="-1.27" x2="-2.54" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="0.635" x2="-6.985" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-6.985" y1="1.27" x2="-5.715" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="1.27" x2="-5.08" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0.635" x2="-5.08" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-0.635" x2="-5.715" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="-1.27" x2="-6.985" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-6.985" y1="-1.27" x2="-7.62" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="1.27" x2="-5.08" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-0.635" x2="-4.445" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="-1.27" x2="-4.445" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-12.065" y1="1.27" x2="-10.795" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-10.795" y1="1.27" x2="-10.16" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="0.635" x2="-10.16" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="-0.635" x2="-10.795" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="0.635" x2="-9.525" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-9.525" y1="1.27" x2="-8.255" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-8.255" y1="1.27" x2="-7.62" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="0.635" x2="-7.62" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="-0.635" x2="-8.255" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-8.255" y1="-1.27" x2="-9.525" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-9.525" y1="-1.27" x2="-10.16" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="0.635" x2="-12.7" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-12.065" y1="1.27" x2="-12.7" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="-0.635" x2="-12.065" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-10.795" y1="-1.27" x2="-12.065" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="10.795" y1="1.27" x2="12.065" y2="1.27" width="0.1524" layer="21"/>
<wire x1="12.065" y1="1.27" x2="12.7" y2="0.635" width="0.1524" layer="21"/>
<wire x1="12.7" y1="0.635" x2="12.7" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="12.7" y1="-0.635" x2="12.065" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="10.795" y1="1.27" x2="10.16" y2="0.635" width="0.1524" layer="21"/>
<wire x1="10.16" y1="-0.635" x2="10.795" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="12.065" y1="-1.27" x2="10.795" y2="-1.27" width="0.1524" layer="21"/>
<pad name="1" x="-11.43" y="0" drill="0.95" diameter="1.4224" shape="long" rot="R90"/>
<pad name="2" x="-8.89" y="0" drill="0.95" diameter="1.4224" shape="long" rot="R90"/>
<pad name="3" x="-6.35" y="0" drill="0.95" diameter="1.4224" shape="long" rot="R90"/>
<pad name="4" x="-3.81" y="0" drill="0.95" diameter="1.4224" shape="long" rot="R90"/>
<pad name="5" x="-1.27" y="0" drill="0.95" diameter="1.4224" shape="long" rot="R90"/>
<pad name="6" x="1.27" y="0" drill="0.95" diameter="1.4224" shape="long" rot="R90"/>
<pad name="7" x="3.81" y="0" drill="0.95" diameter="1.4224" shape="long" rot="R90"/>
<pad name="8" x="6.35" y="0" drill="0.95" diameter="1.4224" shape="long" rot="R90"/>
<pad name="9" x="8.89" y="0" drill="0.95" diameter="1.4224" shape="long" rot="R90"/>
<pad name="10" x="11.43" y="0" drill="0.95" diameter="1.4224" shape="long" rot="R90"/>
<text x="-12.7762" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-12.7" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="8.636" y1="-0.254" x2="9.144" y2="0.254" layer="51"/>
<rectangle x1="6.096" y1="-0.254" x2="6.604" y2="0.254" layer="51"/>
<rectangle x1="3.556" y1="-0.254" x2="4.064" y2="0.254" layer="51"/>
<rectangle x1="1.016" y1="-0.254" x2="1.524" y2="0.254" layer="51"/>
<rectangle x1="-1.524" y1="-0.254" x2="-1.016" y2="0.254" layer="51"/>
<rectangle x1="-4.064" y1="-0.254" x2="-3.556" y2="0.254" layer="51"/>
<rectangle x1="-6.604" y1="-0.254" x2="-6.096" y2="0.254" layer="51"/>
<rectangle x1="-9.144" y1="-0.254" x2="-8.636" y2="0.254" layer="51"/>
<rectangle x1="-11.684" y1="-0.254" x2="-11.176" y2="0.254" layer="51"/>
<rectangle x1="11.176" y1="-0.254" x2="11.684" y2="0.254" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="PINH2X18">
<wire x1="-6.35" y1="-25.4" x2="8.89" y2="-25.4" width="0.4064" layer="94"/>
<wire x1="8.89" y1="-25.4" x2="8.89" y2="22.86" width="0.4064" layer="94"/>
<wire x1="8.89" y1="22.86" x2="-6.35" y2="22.86" width="0.4064" layer="94"/>
<wire x1="-6.35" y1="22.86" x2="-6.35" y2="-25.4" width="0.4064" layer="94"/>
<text x="-6.35" y="23.495" size="1.778" layer="95">&gt;NAME</text>
<text x="-6.35" y="-27.94" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-2.54" y="20.32" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="2" x="5.08" y="20.32" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="3" x="-2.54" y="17.78" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="4" x="5.08" y="17.78" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="5" x="-2.54" y="15.24" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="6" x="5.08" y="15.24" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="7" x="-2.54" y="12.7" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="8" x="5.08" y="12.7" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="9" x="-2.54" y="10.16" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="10" x="5.08" y="10.16" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="11" x="-2.54" y="7.62" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="12" x="5.08" y="7.62" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="13" x="-2.54" y="5.08" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="14" x="5.08" y="5.08" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="15" x="-2.54" y="2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="16" x="5.08" y="2.54" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="17" x="-2.54" y="0" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="18" x="5.08" y="0" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="19" x="-2.54" y="-2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="20" x="5.08" y="-2.54" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="21" x="-2.54" y="-5.08" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="22" x="5.08" y="-5.08" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="23" x="-2.54" y="-7.62" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="24" x="5.08" y="-7.62" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="25" x="-2.54" y="-10.16" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="26" x="5.08" y="-10.16" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="27" x="-2.54" y="-12.7" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="28" x="5.08" y="-12.7" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="29" x="-2.54" y="-15.24" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="30" x="5.08" y="-15.24" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="31" x="-2.54" y="-17.78" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="32" x="5.08" y="-17.78" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="33" x="-2.54" y="-20.32" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="34" x="5.08" y="-20.32" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="35" x="-2.54" y="-22.86" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="36" x="5.08" y="-22.86" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
</symbol>
<symbol name="PINH2X3">
<wire x1="-6.35" y1="-5.08" x2="8.89" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="8.89" y1="-5.08" x2="8.89" y2="5.08" width="0.4064" layer="94"/>
<wire x1="8.89" y1="5.08" x2="-6.35" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-6.35" y1="5.08" x2="-6.35" y2="-5.08" width="0.4064" layer="94"/>
<text x="-6.35" y="5.715" size="1.778" layer="95">&gt;NAME</text>
<text x="-6.35" y="-7.62" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-2.54" y="2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="2" x="5.08" y="2.54" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="3" x="-2.54" y="0" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="4" x="5.08" y="0" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="5" x="-2.54" y="-2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="6" x="5.08" y="-2.54" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
</symbol>
<symbol name="FIDUCIAL">
<circle x="0" y="0" radius="2.54" width="0.254" layer="94"/>
</symbol>
<symbol name="PINHD8">
<wire x1="-6.35" y1="-10.16" x2="1.27" y2="-10.16" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-10.16" x2="1.27" y2="12.7" width="0.4064" layer="94"/>
<wire x1="1.27" y1="12.7" x2="-6.35" y2="12.7" width="0.4064" layer="94"/>
<wire x1="-6.35" y1="12.7" x2="-6.35" y2="-10.16" width="0.4064" layer="94"/>
<text x="-6.35" y="13.335" size="1.778" layer="95">&gt;NAME</text>
<text x="-6.35" y="-12.7" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-2.54" y="10.16" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="2" x="-2.54" y="7.62" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="3" x="-2.54" y="5.08" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="4" x="-2.54" y="2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="5" x="-2.54" y="0" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="6" x="-2.54" y="-2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="7" x="-2.54" y="-5.08" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="8" x="-2.54" y="-7.62" visible="pad" length="short" direction="pas" function="dot"/>
</symbol>
<symbol name="PINHD10">
<wire x1="-6.35" y1="-15.24" x2="1.27" y2="-15.24" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-15.24" x2="1.27" y2="12.7" width="0.4064" layer="94"/>
<wire x1="1.27" y1="12.7" x2="-6.35" y2="12.7" width="0.4064" layer="94"/>
<wire x1="-6.35" y1="12.7" x2="-6.35" y2="-15.24" width="0.4064" layer="94"/>
<text x="-6.35" y="13.335" size="1.778" layer="95">&gt;NAME</text>
<text x="-6.35" y="-17.78" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-2.54" y="10.16" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="2" x="-2.54" y="7.62" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="3" x="-2.54" y="5.08" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="4" x="-2.54" y="2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="5" x="-2.54" y="0" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="6" x="-2.54" y="-2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="7" x="-2.54" y="-5.08" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="8" x="-2.54" y="-7.62" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="9" x="-2.54" y="-10.16" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="10" x="-2.54" y="-12.7" visible="pad" length="short" direction="pas" function="dot"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="PINHD-2X18" prefix="JP" uservalue="yes">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<gates>
<gate name="A" symbol="PINH2X18" x="0" y="0"/>
</gates>
<devices>
<device name="" package="2X18">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="10" pad="10"/>
<connect gate="A" pin="11" pad="11"/>
<connect gate="A" pin="12" pad="12"/>
<connect gate="A" pin="13" pad="13"/>
<connect gate="A" pin="14" pad="14"/>
<connect gate="A" pin="15" pad="15"/>
<connect gate="A" pin="16" pad="16"/>
<connect gate="A" pin="17" pad="17"/>
<connect gate="A" pin="18" pad="18"/>
<connect gate="A" pin="19" pad="19"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="20" pad="20"/>
<connect gate="A" pin="21" pad="21"/>
<connect gate="A" pin="22" pad="22"/>
<connect gate="A" pin="23" pad="23"/>
<connect gate="A" pin="24" pad="24"/>
<connect gate="A" pin="25" pad="25"/>
<connect gate="A" pin="26" pad="26"/>
<connect gate="A" pin="27" pad="27"/>
<connect gate="A" pin="28" pad="28"/>
<connect gate="A" pin="29" pad="29"/>
<connect gate="A" pin="3" pad="3"/>
<connect gate="A" pin="30" pad="30"/>
<connect gate="A" pin="31" pad="31"/>
<connect gate="A" pin="32" pad="32"/>
<connect gate="A" pin="33" pad="33"/>
<connect gate="A" pin="34" pad="34"/>
<connect gate="A" pin="35" pad="35"/>
<connect gate="A" pin="36" pad="36"/>
<connect gate="A" pin="4" pad="4"/>
<connect gate="A" pin="5" pad="5"/>
<connect gate="A" pin="6" pad="6"/>
<connect gate="A" pin="7" pad="7"/>
<connect gate="A" pin="8" pad="8"/>
<connect gate="A" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="/90" package="2X18/90">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="10" pad="10"/>
<connect gate="A" pin="11" pad="11"/>
<connect gate="A" pin="12" pad="12"/>
<connect gate="A" pin="13" pad="13"/>
<connect gate="A" pin="14" pad="14"/>
<connect gate="A" pin="15" pad="15"/>
<connect gate="A" pin="16" pad="16"/>
<connect gate="A" pin="17" pad="17"/>
<connect gate="A" pin="18" pad="18"/>
<connect gate="A" pin="19" pad="19"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="20" pad="20"/>
<connect gate="A" pin="21" pad="21"/>
<connect gate="A" pin="22" pad="22"/>
<connect gate="A" pin="23" pad="23"/>
<connect gate="A" pin="24" pad="24"/>
<connect gate="A" pin="25" pad="25"/>
<connect gate="A" pin="26" pad="26"/>
<connect gate="A" pin="27" pad="27"/>
<connect gate="A" pin="28" pad="28"/>
<connect gate="A" pin="29" pad="29"/>
<connect gate="A" pin="3" pad="3"/>
<connect gate="A" pin="30" pad="30"/>
<connect gate="A" pin="31" pad="31"/>
<connect gate="A" pin="32" pad="32"/>
<connect gate="A" pin="33" pad="33"/>
<connect gate="A" pin="34" pad="34"/>
<connect gate="A" pin="35" pad="35"/>
<connect gate="A" pin="36" pad="36"/>
<connect gate="A" pin="4" pad="4"/>
<connect gate="A" pin="5" pad="5"/>
<connect gate="A" pin="6" pad="6"/>
<connect gate="A" pin="7" pad="7"/>
<connect gate="A" pin="8" pad="8"/>
<connect gate="A" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PINHD-2X3" prefix="JP" uservalue="yes">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<gates>
<gate name="A" symbol="PINH2X3" x="0" y="0"/>
</gates>
<devices>
<device name="" package="2X03">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="3" pad="3"/>
<connect gate="A" pin="4" pad="4"/>
<connect gate="A" pin="5" pad="5"/>
<connect gate="A" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="FIDUCIALMOUNT">
<gates>
<gate name="G$1" symbol="FIDUCIAL" x="0" y="0"/>
</gates>
<devices>
<device name="" package="FIDUCIA-MOUNT">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PINHD-1X8" prefix="JP" uservalue="yes">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<gates>
<gate name="A" symbol="PINHD8" x="0" y="0"/>
</gates>
<devices>
<device name="" package="1X08">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="3" pad="3"/>
<connect gate="A" pin="4" pad="4"/>
<connect gate="A" pin="5" pad="5"/>
<connect gate="A" pin="6" pad="6"/>
<connect gate="A" pin="7" pad="7"/>
<connect gate="A" pin="8" pad="8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TH" package="1X08-TH">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="3" pad="3"/>
<connect gate="A" pin="4" pad="4"/>
<connect gate="A" pin="5" pad="5"/>
<connect gate="A" pin="6" pad="6"/>
<connect gate="A" pin="7" pad="7"/>
<connect gate="A" pin="8" pad="8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PINHD-1X10" prefix="JP" uservalue="yes">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<gates>
<gate name="A" symbol="PINHD10" x="0" y="0"/>
</gates>
<devices>
<device name="" package="1X10">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="10" pad="10"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="3" pad="3"/>
<connect gate="A" pin="4" pad="4"/>
<connect gate="A" pin="5" pad="5"/>
<connect gate="A" pin="6" pad="6"/>
<connect gate="A" pin="7" pad="7"/>
<connect gate="A" pin="8" pad="8"/>
<connect gate="A" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="/90" package="1X10/90">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="10" pad="10"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="3" pad="3"/>
<connect gate="A" pin="4" pad="4"/>
<connect gate="A" pin="5" pad="5"/>
<connect gate="A" pin="6" pad="6"/>
<connect gate="A" pin="7" pad="7"/>
<connect gate="A" pin="8" pad="8"/>
<connect gate="A" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TH" package="1X10-TH">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="10" pad="10"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="3" pad="3"/>
<connect gate="A" pin="4" pad="4"/>
<connect gate="A" pin="5" pad="5"/>
<connect gate="A" pin="6" pad="6"/>
<connect gate="A" pin="7" pad="7"/>
<connect gate="A" pin="8" pad="8"/>
<connect gate="A" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="frames">
<description>&lt;b&gt;Frames for Sheet and Layout&lt;/b&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="A4L-LOC">
<wire x1="256.54" y1="3.81" x2="256.54" y2="8.89" width="0.1016" layer="94"/>
<wire x1="256.54" y1="8.89" x2="256.54" y2="13.97" width="0.1016" layer="94"/>
<wire x1="256.54" y1="13.97" x2="256.54" y2="19.05" width="0.1016" layer="94"/>
<wire x1="256.54" y1="19.05" x2="256.54" y2="24.13" width="0.1016" layer="94"/>
<wire x1="161.29" y1="3.81" x2="161.29" y2="24.13" width="0.1016" layer="94"/>
<wire x1="161.29" y1="24.13" x2="215.265" y2="24.13" width="0.1016" layer="94"/>
<wire x1="215.265" y1="24.13" x2="256.54" y2="24.13" width="0.1016" layer="94"/>
<wire x1="246.38" y1="3.81" x2="246.38" y2="8.89" width="0.1016" layer="94"/>
<wire x1="246.38" y1="8.89" x2="256.54" y2="8.89" width="0.1016" layer="94"/>
<wire x1="246.38" y1="8.89" x2="215.265" y2="8.89" width="0.1016" layer="94"/>
<wire x1="215.265" y1="8.89" x2="215.265" y2="3.81" width="0.1016" layer="94"/>
<wire x1="215.265" y1="8.89" x2="215.265" y2="13.97" width="0.1016" layer="94"/>
<wire x1="215.265" y1="13.97" x2="256.54" y2="13.97" width="0.1016" layer="94"/>
<wire x1="215.265" y1="13.97" x2="215.265" y2="19.05" width="0.1016" layer="94"/>
<wire x1="215.265" y1="19.05" x2="256.54" y2="19.05" width="0.1016" layer="94"/>
<wire x1="215.265" y1="19.05" x2="215.265" y2="24.13" width="0.1016" layer="94"/>
<text x="217.17" y="15.24" size="2.54" layer="94">&gt;DRAWING_NAME</text>
<text x="217.17" y="10.16" size="2.286" layer="94">&gt;LAST_DATE_TIME</text>
<text x="230.505" y="5.08" size="2.54" layer="94">&gt;SHEET</text>
<text x="216.916" y="4.953" size="2.54" layer="94">Sheet:</text>
<frame x1="0" y1="0" x2="260.35" y2="179.07" columns="6" rows="4" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="A4L-LOC" prefix="FRAME" uservalue="yes">
<description>&lt;b&gt;FRAME&lt;/b&gt;&lt;p&gt;
DIN A4, landscape with location and doc. field</description>
<gates>
<gate name="G$1" symbol="A4L-LOC" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="LPCXpresso_boards">
<packages>
<package name="LPCXPRESSO_LPC1769">
<wire x1="32.385" y1="-6.35" x2="33.02" y2="-6.985" width="0.1524" layer="21"/>
<wire x1="34.29" y1="-6.985" x2="34.925" y2="-6.35" width="0.1524" layer="21"/>
<wire x1="34.925" y1="-6.35" x2="35.56" y2="-6.985" width="0.1524" layer="21"/>
<wire x1="36.83" y1="-6.985" x2="37.465" y2="-6.35" width="0.1524" layer="21"/>
<wire x1="37.465" y1="-6.35" x2="38.1" y2="-6.985" width="0.1524" layer="21"/>
<wire x1="39.37" y1="-6.985" x2="40.005" y2="-6.35" width="0.1524" layer="21"/>
<wire x1="40.005" y1="-6.35" x2="40.64" y2="-6.985" width="0.1524" layer="21"/>
<wire x1="41.91" y1="-6.985" x2="42.545" y2="-6.35" width="0.1524" layer="21"/>
<wire x1="42.545" y1="-6.35" x2="43.18" y2="-6.985" width="0.1524" layer="21"/>
<wire x1="44.45" y1="-6.985" x2="45.085" y2="-6.35" width="0.1524" layer="21"/>
<wire x1="45.085" y1="-6.35" x2="45.72" y2="-6.985" width="0.1524" layer="21"/>
<wire x1="46.99" y1="-6.985" x2="47.625" y2="-6.35" width="0.1524" layer="21"/>
<wire x1="32.385" y1="-6.35" x2="32.385" y2="-5.08" width="0.1524" layer="21"/>
<wire x1="32.385" y1="-5.08" x2="33.02" y2="-4.445" width="0.1524" layer="21"/>
<wire x1="33.02" y1="-4.445" x2="34.29" y2="-4.445" width="0.1524" layer="21"/>
<wire x1="34.29" y1="-4.445" x2="34.925" y2="-5.08" width="0.1524" layer="21"/>
<wire x1="34.925" y1="-5.08" x2="35.56" y2="-4.445" width="0.1524" layer="21"/>
<wire x1="35.56" y1="-4.445" x2="36.83" y2="-4.445" width="0.1524" layer="21"/>
<wire x1="36.83" y1="-4.445" x2="37.465" y2="-5.08" width="0.1524" layer="21"/>
<wire x1="37.465" y1="-5.08" x2="38.1" y2="-4.445" width="0.1524" layer="21"/>
<wire x1="38.1" y1="-4.445" x2="39.37" y2="-4.445" width="0.1524" layer="21"/>
<wire x1="39.37" y1="-4.445" x2="40.005" y2="-5.08" width="0.1524" layer="21"/>
<wire x1="40.005" y1="-5.08" x2="40.64" y2="-4.445" width="0.1524" layer="21"/>
<wire x1="40.64" y1="-4.445" x2="41.91" y2="-4.445" width="0.1524" layer="21"/>
<wire x1="41.91" y1="-4.445" x2="42.545" y2="-5.08" width="0.1524" layer="21"/>
<wire x1="42.545" y1="-5.08" x2="43.18" y2="-4.445" width="0.1524" layer="21"/>
<wire x1="43.18" y1="-4.445" x2="44.45" y2="-4.445" width="0.1524" layer="21"/>
<wire x1="44.45" y1="-4.445" x2="45.085" y2="-5.08" width="0.1524" layer="21"/>
<wire x1="45.085" y1="-5.08" x2="45.72" y2="-4.445" width="0.1524" layer="21"/>
<wire x1="45.72" y1="-4.445" x2="46.99" y2="-4.445" width="0.1524" layer="21"/>
<wire x1="46.99" y1="-4.445" x2="47.625" y2="-5.08" width="0.1524" layer="21"/>
<wire x1="47.625" y1="-5.08" x2="48.26" y2="-4.445" width="0.1524" layer="21"/>
<wire x1="48.26" y1="-4.445" x2="49.53" y2="-4.445" width="0.1524" layer="21"/>
<wire x1="49.53" y1="-4.445" x2="50.165" y2="-5.08" width="0.1524" layer="21"/>
<wire x1="50.165" y1="-5.08" x2="50.8" y2="-4.445" width="0.1524" layer="21"/>
<wire x1="50.8" y1="-4.445" x2="52.07" y2="-4.445" width="0.1524" layer="21"/>
<wire x1="52.07" y1="-4.445" x2="52.705" y2="-5.08" width="0.1524" layer="21"/>
<wire x1="52.705" y1="-5.08" x2="53.34" y2="-4.445" width="0.1524" layer="21"/>
<wire x1="53.34" y1="-4.445" x2="54.61" y2="-4.445" width="0.1524" layer="21"/>
<wire x1="54.61" y1="-4.445" x2="55.245" y2="-5.08" width="0.1524" layer="21"/>
<wire x1="55.245" y1="-6.35" x2="54.61" y2="-6.985" width="0.1524" layer="21"/>
<wire x1="52.705" y1="-6.35" x2="53.34" y2="-6.985" width="0.1524" layer="21"/>
<wire x1="52.705" y1="-6.35" x2="52.07" y2="-6.985" width="0.1524" layer="21"/>
<wire x1="50.165" y1="-6.35" x2="50.8" y2="-6.985" width="0.1524" layer="21"/>
<wire x1="50.165" y1="-6.35" x2="49.53" y2="-6.985" width="0.1524" layer="21"/>
<wire x1="47.625" y1="-6.35" x2="48.26" y2="-6.985" width="0.1524" layer="21"/>
<wire x1="34.925" y1="-5.08" x2="34.925" y2="-6.35" width="0.1524" layer="21"/>
<wire x1="37.465" y1="-5.08" x2="37.465" y2="-6.35" width="0.1524" layer="21"/>
<wire x1="40.005" y1="-5.08" x2="40.005" y2="-6.35" width="0.1524" layer="21"/>
<wire x1="42.545" y1="-5.08" x2="42.545" y2="-6.35" width="0.1524" layer="21"/>
<wire x1="45.085" y1="-5.08" x2="45.085" y2="-6.35" width="0.1524" layer="21"/>
<wire x1="47.625" y1="-5.08" x2="47.625" y2="-6.35" width="0.1524" layer="21"/>
<wire x1="50.165" y1="-5.08" x2="50.165" y2="-6.35" width="0.1524" layer="21"/>
<wire x1="52.705" y1="-5.08" x2="52.705" y2="-6.35" width="0.1524" layer="21"/>
<wire x1="55.245" y1="-5.08" x2="55.245" y2="-6.35" width="0.1524" layer="21"/>
<wire x1="53.34" y1="-6.985" x2="54.61" y2="-6.985" width="0.1524" layer="21"/>
<wire x1="50.8" y1="-6.985" x2="52.07" y2="-6.985" width="0.1524" layer="21"/>
<wire x1="48.26" y1="-6.985" x2="49.53" y2="-6.985" width="0.1524" layer="21"/>
<wire x1="45.72" y1="-6.985" x2="46.99" y2="-6.985" width="0.1524" layer="21"/>
<wire x1="43.18" y1="-6.985" x2="44.45" y2="-6.985" width="0.1524" layer="21"/>
<wire x1="40.64" y1="-6.985" x2="41.91" y2="-6.985" width="0.1524" layer="21"/>
<wire x1="38.1" y1="-6.985" x2="39.37" y2="-6.985" width="0.1524" layer="21"/>
<wire x1="35.56" y1="-6.985" x2="36.83" y2="-6.985" width="0.1524" layer="21"/>
<wire x1="33.02" y1="-6.985" x2="34.29" y2="-6.985" width="0.1524" layer="21"/>
<wire x1="55.245" y1="-5.08" x2="55.88" y2="-4.445" width="0.1524" layer="21"/>
<wire x1="55.88" y1="-4.445" x2="57.15" y2="-4.445" width="0.1524" layer="21"/>
<wire x1="57.15" y1="-4.445" x2="57.785" y2="-5.08" width="0.1524" layer="21"/>
<wire x1="57.785" y1="-6.35" x2="57.15" y2="-6.985" width="0.1524" layer="21"/>
<wire x1="55.245" y1="-6.35" x2="55.88" y2="-6.985" width="0.1524" layer="21"/>
<wire x1="57.785" y1="-5.08" x2="57.785" y2="-6.35" width="0.1524" layer="21"/>
<wire x1="55.88" y1="-6.985" x2="57.15" y2="-6.985" width="0.1524" layer="21"/>
<wire x1="57.785" y1="-6.35" x2="58.42" y2="-6.985" width="0.1524" layer="21"/>
<wire x1="59.69" y1="-6.985" x2="60.325" y2="-6.35" width="0.1524" layer="21"/>
<wire x1="60.325" y1="-6.35" x2="60.96" y2="-6.985" width="0.1524" layer="21"/>
<wire x1="62.23" y1="-6.985" x2="62.865" y2="-6.35" width="0.1524" layer="21"/>
<wire x1="62.865" y1="-6.35" x2="63.5" y2="-6.985" width="0.1524" layer="21"/>
<wire x1="64.77" y1="-6.985" x2="65.405" y2="-6.35" width="0.1524" layer="21"/>
<wire x1="65.405" y1="-6.35" x2="66.04" y2="-6.985" width="0.1524" layer="21"/>
<wire x1="67.31" y1="-6.985" x2="67.945" y2="-6.35" width="0.1524" layer="21"/>
<wire x1="67.945" y1="-6.35" x2="68.58" y2="-6.985" width="0.1524" layer="21"/>
<wire x1="69.85" y1="-6.985" x2="70.485" y2="-6.35" width="0.1524" layer="21"/>
<wire x1="70.485" y1="-6.35" x2="71.12" y2="-6.985" width="0.1524" layer="21"/>
<wire x1="72.39" y1="-6.985" x2="73.025" y2="-6.35" width="0.1524" layer="21"/>
<wire x1="57.785" y1="-5.08" x2="58.42" y2="-4.445" width="0.1524" layer="21"/>
<wire x1="58.42" y1="-4.445" x2="59.69" y2="-4.445" width="0.1524" layer="21"/>
<wire x1="59.69" y1="-4.445" x2="60.325" y2="-5.08" width="0.1524" layer="21"/>
<wire x1="60.325" y1="-5.08" x2="60.96" y2="-4.445" width="0.1524" layer="21"/>
<wire x1="60.96" y1="-4.445" x2="62.23" y2="-4.445" width="0.1524" layer="21"/>
<wire x1="62.23" y1="-4.445" x2="62.865" y2="-5.08" width="0.1524" layer="21"/>
<wire x1="62.865" y1="-5.08" x2="63.5" y2="-4.445" width="0.1524" layer="21"/>
<wire x1="63.5" y1="-4.445" x2="64.77" y2="-4.445" width="0.1524" layer="21"/>
<wire x1="64.77" y1="-4.445" x2="65.405" y2="-5.08" width="0.1524" layer="21"/>
<wire x1="65.405" y1="-5.08" x2="66.04" y2="-4.445" width="0.1524" layer="21"/>
<wire x1="66.04" y1="-4.445" x2="67.31" y2="-4.445" width="0.1524" layer="21"/>
<wire x1="67.31" y1="-4.445" x2="67.945" y2="-5.08" width="0.1524" layer="21"/>
<wire x1="67.945" y1="-5.08" x2="68.58" y2="-4.445" width="0.1524" layer="21"/>
<wire x1="68.58" y1="-4.445" x2="69.85" y2="-4.445" width="0.1524" layer="21"/>
<wire x1="69.85" y1="-4.445" x2="70.485" y2="-5.08" width="0.1524" layer="21"/>
<wire x1="70.485" y1="-5.08" x2="71.12" y2="-4.445" width="0.1524" layer="21"/>
<wire x1="71.12" y1="-4.445" x2="72.39" y2="-4.445" width="0.1524" layer="21"/>
<wire x1="72.39" y1="-4.445" x2="73.025" y2="-5.08" width="0.1524" layer="21"/>
<wire x1="73.025" y1="-5.08" x2="73.66" y2="-4.445" width="0.1524" layer="21"/>
<wire x1="73.66" y1="-4.445" x2="74.93" y2="-4.445" width="0.1524" layer="21"/>
<wire x1="74.93" y1="-4.445" x2="75.565" y2="-5.08" width="0.1524" layer="21"/>
<wire x1="75.565" y1="-5.08" x2="76.2" y2="-4.445" width="0.1524" layer="21"/>
<wire x1="76.2" y1="-4.445" x2="77.47" y2="-4.445" width="0.1524" layer="21"/>
<wire x1="77.47" y1="-4.445" x2="78.105" y2="-5.08" width="0.1524" layer="21"/>
<wire x1="78.105" y1="-5.08" x2="78.74" y2="-4.445" width="0.1524" layer="21"/>
<wire x1="78.74" y1="-4.445" x2="80.01" y2="-4.445" width="0.1524" layer="21"/>
<wire x1="80.01" y1="-4.445" x2="80.645" y2="-5.08" width="0.1524" layer="21"/>
<wire x1="80.645" y1="-6.35" x2="80.01" y2="-6.985" width="0.1524" layer="21"/>
<wire x1="78.105" y1="-6.35" x2="78.74" y2="-6.985" width="0.1524" layer="21"/>
<wire x1="78.105" y1="-6.35" x2="77.47" y2="-6.985" width="0.1524" layer="21"/>
<wire x1="75.565" y1="-6.35" x2="76.2" y2="-6.985" width="0.1524" layer="21"/>
<wire x1="75.565" y1="-6.35" x2="74.93" y2="-6.985" width="0.1524" layer="21"/>
<wire x1="73.025" y1="-6.35" x2="73.66" y2="-6.985" width="0.1524" layer="21"/>
<wire x1="60.325" y1="-5.08" x2="60.325" y2="-6.35" width="0.1524" layer="21"/>
<wire x1="62.865" y1="-5.08" x2="62.865" y2="-6.35" width="0.1524" layer="21"/>
<wire x1="65.405" y1="-5.08" x2="65.405" y2="-6.35" width="0.1524" layer="21"/>
<wire x1="67.945" y1="-5.08" x2="67.945" y2="-6.35" width="0.1524" layer="21"/>
<wire x1="70.485" y1="-5.08" x2="70.485" y2="-6.35" width="0.1524" layer="21"/>
<wire x1="73.025" y1="-5.08" x2="73.025" y2="-6.35" width="0.1524" layer="21"/>
<wire x1="75.565" y1="-5.08" x2="75.565" y2="-6.35" width="0.1524" layer="21"/>
<wire x1="78.105" y1="-5.08" x2="78.105" y2="-6.35" width="0.1524" layer="21"/>
<wire x1="80.645" y1="-5.08" x2="80.645" y2="-6.35" width="0.1524" layer="21"/>
<wire x1="78.74" y1="-6.985" x2="80.01" y2="-6.985" width="0.1524" layer="21"/>
<wire x1="76.2" y1="-6.985" x2="77.47" y2="-6.985" width="0.1524" layer="21"/>
<wire x1="73.66" y1="-6.985" x2="74.93" y2="-6.985" width="0.1524" layer="21"/>
<wire x1="71.12" y1="-6.985" x2="72.39" y2="-6.985" width="0.1524" layer="21"/>
<wire x1="68.58" y1="-6.985" x2="69.85" y2="-6.985" width="0.1524" layer="21"/>
<wire x1="66.04" y1="-6.985" x2="67.31" y2="-6.985" width="0.1524" layer="21"/>
<wire x1="63.5" y1="-6.985" x2="64.77" y2="-6.985" width="0.1524" layer="21"/>
<wire x1="60.96" y1="-6.985" x2="62.23" y2="-6.985" width="0.1524" layer="21"/>
<wire x1="58.42" y1="-6.985" x2="59.69" y2="-6.985" width="0.1524" layer="21"/>
<wire x1="80.645" y1="-5.08" x2="81.28" y2="-4.445" width="0.1524" layer="21"/>
<wire x1="81.28" y1="-4.445" x2="82.55" y2="-4.445" width="0.1524" layer="21"/>
<wire x1="82.55" y1="-4.445" x2="83.185" y2="-5.08" width="0.1524" layer="21"/>
<wire x1="83.185" y1="-6.35" x2="82.55" y2="-6.985" width="0.1524" layer="21"/>
<wire x1="80.645" y1="-6.35" x2="81.28" y2="-6.985" width="0.1524" layer="21"/>
<wire x1="83.185" y1="-5.08" x2="83.185" y2="-6.35" width="0.1524" layer="21"/>
<wire x1="81.28" y1="-6.985" x2="82.55" y2="-6.985" width="0.1524" layer="21"/>
<wire x1="83.185" y1="-6.35" x2="83.82" y2="-6.985" width="0.1524" layer="21"/>
<wire x1="85.09" y1="-6.985" x2="85.725" y2="-6.35" width="0.1524" layer="21"/>
<wire x1="85.725" y1="-6.35" x2="86.36" y2="-6.985" width="0.1524" layer="21"/>
<wire x1="87.63" y1="-6.985" x2="88.265" y2="-6.35" width="0.1524" layer="21"/>
<wire x1="88.265" y1="-6.35" x2="88.9" y2="-6.985" width="0.1524" layer="21"/>
<wire x1="90.17" y1="-6.985" x2="90.805" y2="-6.35" width="0.1524" layer="21"/>
<wire x1="90.805" y1="-6.35" x2="91.44" y2="-6.985" width="0.1524" layer="21"/>
<wire x1="92.71" y1="-6.985" x2="93.345" y2="-6.35" width="0.1524" layer="21"/>
<wire x1="93.345" y1="-6.35" x2="93.98" y2="-6.985" width="0.1524" layer="21"/>
<wire x1="95.25" y1="-6.985" x2="95.885" y2="-6.35" width="0.1524" layer="21"/>
<wire x1="95.885" y1="-6.35" x2="96.52" y2="-6.985" width="0.1524" layer="21"/>
<wire x1="97.79" y1="-6.985" x2="98.425" y2="-6.35" width="0.1524" layer="21"/>
<wire x1="83.185" y1="-5.08" x2="83.82" y2="-4.445" width="0.1524" layer="21"/>
<wire x1="83.82" y1="-4.445" x2="85.09" y2="-4.445" width="0.1524" layer="21"/>
<wire x1="85.09" y1="-4.445" x2="85.725" y2="-5.08" width="0.1524" layer="21"/>
<wire x1="85.725" y1="-5.08" x2="86.36" y2="-4.445" width="0.1524" layer="21"/>
<wire x1="86.36" y1="-4.445" x2="87.63" y2="-4.445" width="0.1524" layer="21"/>
<wire x1="87.63" y1="-4.445" x2="88.265" y2="-5.08" width="0.1524" layer="21"/>
<wire x1="88.265" y1="-5.08" x2="88.9" y2="-4.445" width="0.1524" layer="21"/>
<wire x1="88.9" y1="-4.445" x2="90.17" y2="-4.445" width="0.1524" layer="21"/>
<wire x1="90.17" y1="-4.445" x2="90.805" y2="-5.08" width="0.1524" layer="21"/>
<wire x1="90.805" y1="-5.08" x2="91.44" y2="-4.445" width="0.1524" layer="21"/>
<wire x1="91.44" y1="-4.445" x2="92.71" y2="-4.445" width="0.1524" layer="21"/>
<wire x1="92.71" y1="-4.445" x2="93.345" y2="-5.08" width="0.1524" layer="21"/>
<wire x1="93.345" y1="-5.08" x2="93.98" y2="-4.445" width="0.1524" layer="21"/>
<wire x1="93.98" y1="-4.445" x2="95.25" y2="-4.445" width="0.1524" layer="21"/>
<wire x1="95.25" y1="-4.445" x2="95.885" y2="-5.08" width="0.1524" layer="21"/>
<wire x1="95.885" y1="-5.08" x2="96.52" y2="-4.445" width="0.1524" layer="21"/>
<wire x1="96.52" y1="-4.445" x2="97.79" y2="-4.445" width="0.1524" layer="21"/>
<wire x1="97.79" y1="-4.445" x2="98.425" y2="-5.08" width="0.1524" layer="21"/>
<wire x1="98.425" y1="-5.08" x2="99.06" y2="-4.445" width="0.1524" layer="21"/>
<wire x1="99.06" y1="-4.445" x2="100.33" y2="-4.445" width="0.1524" layer="21"/>
<wire x1="100.33" y1="-4.445" x2="100.965" y2="-5.08" width="0.1524" layer="21"/>
<wire x1="100.965" y1="-6.35" x2="100.33" y2="-6.985" width="0.1524" layer="21"/>
<wire x1="98.425" y1="-6.35" x2="99.06" y2="-6.985" width="0.1524" layer="21"/>
<wire x1="85.725" y1="-5.08" x2="85.725" y2="-6.35" width="0.1524" layer="21"/>
<wire x1="88.265" y1="-5.08" x2="88.265" y2="-6.35" width="0.1524" layer="21"/>
<wire x1="90.805" y1="-5.08" x2="90.805" y2="-6.35" width="0.1524" layer="21"/>
<wire x1="93.345" y1="-5.08" x2="93.345" y2="-6.35" width="0.1524" layer="21"/>
<wire x1="95.885" y1="-5.08" x2="95.885" y2="-6.35" width="0.1524" layer="21"/>
<wire x1="98.425" y1="-5.08" x2="98.425" y2="-6.35" width="0.1524" layer="21"/>
<wire x1="100.965" y1="-5.08" x2="100.965" y2="-6.35" width="0.1524" layer="21"/>
<wire x1="99.06" y1="-6.985" x2="100.33" y2="-6.985" width="0.1524" layer="21"/>
<wire x1="96.52" y1="-6.985" x2="97.79" y2="-6.985" width="0.1524" layer="21"/>
<wire x1="93.98" y1="-6.985" x2="95.25" y2="-6.985" width="0.1524" layer="21"/>
<wire x1="91.44" y1="-6.985" x2="92.71" y2="-6.985" width="0.1524" layer="21"/>
<wire x1="88.9" y1="-6.985" x2="90.17" y2="-6.985" width="0.1524" layer="21"/>
<wire x1="86.36" y1="-6.985" x2="87.63" y2="-6.985" width="0.1524" layer="21"/>
<wire x1="83.82" y1="-6.985" x2="85.09" y2="-6.985" width="0.1524" layer="21"/>
<wire x1="32.385" y1="16.51" x2="33.02" y2="15.875" width="0.1524" layer="21"/>
<wire x1="34.29" y1="15.875" x2="34.925" y2="16.51" width="0.1524" layer="21"/>
<wire x1="34.925" y1="16.51" x2="35.56" y2="15.875" width="0.1524" layer="21"/>
<wire x1="36.83" y1="15.875" x2="37.465" y2="16.51" width="0.1524" layer="21"/>
<wire x1="37.465" y1="16.51" x2="38.1" y2="15.875" width="0.1524" layer="21"/>
<wire x1="39.37" y1="15.875" x2="40.005" y2="16.51" width="0.1524" layer="21"/>
<wire x1="40.005" y1="16.51" x2="40.64" y2="15.875" width="0.1524" layer="21"/>
<wire x1="41.91" y1="15.875" x2="42.545" y2="16.51" width="0.1524" layer="21"/>
<wire x1="42.545" y1="16.51" x2="43.18" y2="15.875" width="0.1524" layer="21"/>
<wire x1="44.45" y1="15.875" x2="45.085" y2="16.51" width="0.1524" layer="21"/>
<wire x1="45.085" y1="16.51" x2="45.72" y2="15.875" width="0.1524" layer="21"/>
<wire x1="46.99" y1="15.875" x2="47.625" y2="16.51" width="0.1524" layer="21"/>
<wire x1="32.385" y1="16.51" x2="32.385" y2="17.78" width="0.1524" layer="21"/>
<wire x1="32.385" y1="17.78" x2="33.02" y2="18.415" width="0.1524" layer="21"/>
<wire x1="33.02" y1="18.415" x2="34.29" y2="18.415" width="0.1524" layer="21"/>
<wire x1="34.29" y1="18.415" x2="34.925" y2="17.78" width="0.1524" layer="21"/>
<wire x1="34.925" y1="17.78" x2="35.56" y2="18.415" width="0.1524" layer="21"/>
<wire x1="35.56" y1="18.415" x2="36.83" y2="18.415" width="0.1524" layer="21"/>
<wire x1="36.83" y1="18.415" x2="37.465" y2="17.78" width="0.1524" layer="21"/>
<wire x1="37.465" y1="17.78" x2="38.1" y2="18.415" width="0.1524" layer="21"/>
<wire x1="38.1" y1="18.415" x2="39.37" y2="18.415" width="0.1524" layer="21"/>
<wire x1="39.37" y1="18.415" x2="40.005" y2="17.78" width="0.1524" layer="21"/>
<wire x1="40.005" y1="17.78" x2="40.64" y2="18.415" width="0.1524" layer="21"/>
<wire x1="40.64" y1="18.415" x2="41.91" y2="18.415" width="0.1524" layer="21"/>
<wire x1="41.91" y1="18.415" x2="42.545" y2="17.78" width="0.1524" layer="21"/>
<wire x1="42.545" y1="17.78" x2="43.18" y2="18.415" width="0.1524" layer="21"/>
<wire x1="43.18" y1="18.415" x2="44.45" y2="18.415" width="0.1524" layer="21"/>
<wire x1="44.45" y1="18.415" x2="45.085" y2="17.78" width="0.1524" layer="21"/>
<wire x1="45.085" y1="17.78" x2="45.72" y2="18.415" width="0.1524" layer="21"/>
<wire x1="45.72" y1="18.415" x2="46.99" y2="18.415" width="0.1524" layer="21"/>
<wire x1="46.99" y1="18.415" x2="47.625" y2="17.78" width="0.1524" layer="21"/>
<wire x1="47.625" y1="17.78" x2="48.26" y2="18.415" width="0.1524" layer="21"/>
<wire x1="48.26" y1="18.415" x2="49.53" y2="18.415" width="0.1524" layer="21"/>
<wire x1="49.53" y1="18.415" x2="50.165" y2="17.78" width="0.1524" layer="21"/>
<wire x1="50.165" y1="17.78" x2="50.8" y2="18.415" width="0.1524" layer="21"/>
<wire x1="50.8" y1="18.415" x2="52.07" y2="18.415" width="0.1524" layer="21"/>
<wire x1="52.07" y1="18.415" x2="52.705" y2="17.78" width="0.1524" layer="21"/>
<wire x1="52.705" y1="17.78" x2="53.34" y2="18.415" width="0.1524" layer="21"/>
<wire x1="53.34" y1="18.415" x2="54.61" y2="18.415" width="0.1524" layer="21"/>
<wire x1="54.61" y1="18.415" x2="55.245" y2="17.78" width="0.1524" layer="21"/>
<wire x1="55.245" y1="16.51" x2="54.61" y2="15.875" width="0.1524" layer="21"/>
<wire x1="52.705" y1="16.51" x2="53.34" y2="15.875" width="0.1524" layer="21"/>
<wire x1="52.705" y1="16.51" x2="52.07" y2="15.875" width="0.1524" layer="21"/>
<wire x1="50.165" y1="16.51" x2="50.8" y2="15.875" width="0.1524" layer="21"/>
<wire x1="50.165" y1="16.51" x2="49.53" y2="15.875" width="0.1524" layer="21"/>
<wire x1="47.625" y1="16.51" x2="48.26" y2="15.875" width="0.1524" layer="21"/>
<wire x1="34.925" y1="17.78" x2="34.925" y2="16.51" width="0.1524" layer="21"/>
<wire x1="37.465" y1="17.78" x2="37.465" y2="16.51" width="0.1524" layer="21"/>
<wire x1="40.005" y1="17.78" x2="40.005" y2="16.51" width="0.1524" layer="21"/>
<wire x1="42.545" y1="17.78" x2="42.545" y2="16.51" width="0.1524" layer="21"/>
<wire x1="45.085" y1="17.78" x2="45.085" y2="16.51" width="0.1524" layer="21"/>
<wire x1="47.625" y1="17.78" x2="47.625" y2="16.51" width="0.1524" layer="21"/>
<wire x1="50.165" y1="17.78" x2="50.165" y2="16.51" width="0.1524" layer="21"/>
<wire x1="52.705" y1="17.78" x2="52.705" y2="16.51" width="0.1524" layer="21"/>
<wire x1="55.245" y1="17.78" x2="55.245" y2="16.51" width="0.1524" layer="21"/>
<wire x1="53.34" y1="15.875" x2="54.61" y2="15.875" width="0.1524" layer="21"/>
<wire x1="50.8" y1="15.875" x2="52.07" y2="15.875" width="0.1524" layer="21"/>
<wire x1="48.26" y1="15.875" x2="49.53" y2="15.875" width="0.1524" layer="21"/>
<wire x1="45.72" y1="15.875" x2="46.99" y2="15.875" width="0.1524" layer="21"/>
<wire x1="43.18" y1="15.875" x2="44.45" y2="15.875" width="0.1524" layer="21"/>
<wire x1="40.64" y1="15.875" x2="41.91" y2="15.875" width="0.1524" layer="21"/>
<wire x1="38.1" y1="15.875" x2="39.37" y2="15.875" width="0.1524" layer="21"/>
<wire x1="35.56" y1="15.875" x2="36.83" y2="15.875" width="0.1524" layer="21"/>
<wire x1="33.02" y1="15.875" x2="34.29" y2="15.875" width="0.1524" layer="21"/>
<wire x1="55.245" y1="17.78" x2="55.88" y2="18.415" width="0.1524" layer="21"/>
<wire x1="55.88" y1="18.415" x2="57.15" y2="18.415" width="0.1524" layer="21"/>
<wire x1="57.15" y1="18.415" x2="57.785" y2="17.78" width="0.1524" layer="21"/>
<wire x1="57.785" y1="16.51" x2="57.15" y2="15.875" width="0.1524" layer="21"/>
<wire x1="55.245" y1="16.51" x2="55.88" y2="15.875" width="0.1524" layer="21"/>
<wire x1="57.785" y1="17.78" x2="57.785" y2="16.51" width="0.1524" layer="21"/>
<wire x1="55.88" y1="15.875" x2="57.15" y2="15.875" width="0.1524" layer="21"/>
<wire x1="57.785" y1="16.51" x2="58.42" y2="15.875" width="0.1524" layer="21"/>
<wire x1="59.69" y1="15.875" x2="60.325" y2="16.51" width="0.1524" layer="21"/>
<wire x1="60.325" y1="16.51" x2="60.96" y2="15.875" width="0.1524" layer="21"/>
<wire x1="62.23" y1="15.875" x2="62.865" y2="16.51" width="0.1524" layer="21"/>
<wire x1="62.865" y1="16.51" x2="63.5" y2="15.875" width="0.1524" layer="21"/>
<wire x1="64.77" y1="15.875" x2="65.405" y2="16.51" width="0.1524" layer="21"/>
<wire x1="65.405" y1="16.51" x2="66.04" y2="15.875" width="0.1524" layer="21"/>
<wire x1="67.31" y1="15.875" x2="67.945" y2="16.51" width="0.1524" layer="21"/>
<wire x1="67.945" y1="16.51" x2="68.58" y2="15.875" width="0.1524" layer="21"/>
<wire x1="69.85" y1="15.875" x2="70.485" y2="16.51" width="0.1524" layer="21"/>
<wire x1="70.485" y1="16.51" x2="71.12" y2="15.875" width="0.1524" layer="21"/>
<wire x1="72.39" y1="15.875" x2="73.025" y2="16.51" width="0.1524" layer="21"/>
<wire x1="57.785" y1="17.78" x2="58.42" y2="18.415" width="0.1524" layer="21"/>
<wire x1="58.42" y1="18.415" x2="59.69" y2="18.415" width="0.1524" layer="21"/>
<wire x1="59.69" y1="18.415" x2="60.325" y2="17.78" width="0.1524" layer="21"/>
<wire x1="60.325" y1="17.78" x2="60.96" y2="18.415" width="0.1524" layer="21"/>
<wire x1="60.96" y1="18.415" x2="62.23" y2="18.415" width="0.1524" layer="21"/>
<wire x1="62.23" y1="18.415" x2="62.865" y2="17.78" width="0.1524" layer="21"/>
<wire x1="62.865" y1="17.78" x2="63.5" y2="18.415" width="0.1524" layer="21"/>
<wire x1="63.5" y1="18.415" x2="64.77" y2="18.415" width="0.1524" layer="21"/>
<wire x1="64.77" y1="18.415" x2="65.405" y2="17.78" width="0.1524" layer="21"/>
<wire x1="65.405" y1="17.78" x2="66.04" y2="18.415" width="0.1524" layer="21"/>
<wire x1="66.04" y1="18.415" x2="67.31" y2="18.415" width="0.1524" layer="21"/>
<wire x1="67.31" y1="18.415" x2="67.945" y2="17.78" width="0.1524" layer="21"/>
<wire x1="67.945" y1="17.78" x2="68.58" y2="18.415" width="0.1524" layer="21"/>
<wire x1="68.58" y1="18.415" x2="69.85" y2="18.415" width="0.1524" layer="21"/>
<wire x1="69.85" y1="18.415" x2="70.485" y2="17.78" width="0.1524" layer="21"/>
<wire x1="70.485" y1="17.78" x2="71.12" y2="18.415" width="0.1524" layer="21"/>
<wire x1="71.12" y1="18.415" x2="72.39" y2="18.415" width="0.1524" layer="21"/>
<wire x1="72.39" y1="18.415" x2="73.025" y2="17.78" width="0.1524" layer="21"/>
<wire x1="73.025" y1="17.78" x2="73.66" y2="18.415" width="0.1524" layer="21"/>
<wire x1="73.66" y1="18.415" x2="74.93" y2="18.415" width="0.1524" layer="21"/>
<wire x1="74.93" y1="18.415" x2="75.565" y2="17.78" width="0.1524" layer="21"/>
<wire x1="75.565" y1="17.78" x2="76.2" y2="18.415" width="0.1524" layer="21"/>
<wire x1="76.2" y1="18.415" x2="77.47" y2="18.415" width="0.1524" layer="21"/>
<wire x1="77.47" y1="18.415" x2="78.105" y2="17.78" width="0.1524" layer="21"/>
<wire x1="78.105" y1="17.78" x2="78.74" y2="18.415" width="0.1524" layer="21"/>
<wire x1="78.74" y1="18.415" x2="80.01" y2="18.415" width="0.1524" layer="21"/>
<wire x1="80.01" y1="18.415" x2="80.645" y2="17.78" width="0.1524" layer="21"/>
<wire x1="80.645" y1="16.51" x2="80.01" y2="15.875" width="0.1524" layer="21"/>
<wire x1="78.105" y1="16.51" x2="78.74" y2="15.875" width="0.1524" layer="21"/>
<wire x1="78.105" y1="16.51" x2="77.47" y2="15.875" width="0.1524" layer="21"/>
<wire x1="75.565" y1="16.51" x2="76.2" y2="15.875" width="0.1524" layer="21"/>
<wire x1="75.565" y1="16.51" x2="74.93" y2="15.875" width="0.1524" layer="21"/>
<wire x1="73.025" y1="16.51" x2="73.66" y2="15.875" width="0.1524" layer="21"/>
<wire x1="60.325" y1="17.78" x2="60.325" y2="16.51" width="0.1524" layer="21"/>
<wire x1="62.865" y1="17.78" x2="62.865" y2="16.51" width="0.1524" layer="21"/>
<wire x1="65.405" y1="17.78" x2="65.405" y2="16.51" width="0.1524" layer="21"/>
<wire x1="67.945" y1="17.78" x2="67.945" y2="16.51" width="0.1524" layer="21"/>
<wire x1="70.485" y1="17.78" x2="70.485" y2="16.51" width="0.1524" layer="21"/>
<wire x1="73.025" y1="17.78" x2="73.025" y2="16.51" width="0.1524" layer="21"/>
<wire x1="75.565" y1="17.78" x2="75.565" y2="16.51" width="0.1524" layer="21"/>
<wire x1="78.105" y1="17.78" x2="78.105" y2="16.51" width="0.1524" layer="21"/>
<wire x1="80.645" y1="17.78" x2="80.645" y2="16.51" width="0.1524" layer="21"/>
<wire x1="78.74" y1="15.875" x2="80.01" y2="15.875" width="0.1524" layer="21"/>
<wire x1="76.2" y1="15.875" x2="77.47" y2="15.875" width="0.1524" layer="21"/>
<wire x1="73.66" y1="15.875" x2="74.93" y2="15.875" width="0.1524" layer="21"/>
<wire x1="71.12" y1="15.875" x2="72.39" y2="15.875" width="0.1524" layer="21"/>
<wire x1="68.58" y1="15.875" x2="69.85" y2="15.875" width="0.1524" layer="21"/>
<wire x1="66.04" y1="15.875" x2="67.31" y2="15.875" width="0.1524" layer="21"/>
<wire x1="63.5" y1="15.875" x2="64.77" y2="15.875" width="0.1524" layer="21"/>
<wire x1="60.96" y1="15.875" x2="62.23" y2="15.875" width="0.1524" layer="21"/>
<wire x1="58.42" y1="15.875" x2="59.69" y2="15.875" width="0.1524" layer="21"/>
<wire x1="80.645" y1="17.78" x2="81.28" y2="18.415" width="0.1524" layer="21"/>
<wire x1="81.28" y1="18.415" x2="82.55" y2="18.415" width="0.1524" layer="21"/>
<wire x1="82.55" y1="18.415" x2="83.185" y2="17.78" width="0.1524" layer="21"/>
<wire x1="83.185" y1="16.51" x2="82.55" y2="15.875" width="0.1524" layer="21"/>
<wire x1="80.645" y1="16.51" x2="81.28" y2="15.875" width="0.1524" layer="21"/>
<wire x1="83.185" y1="17.78" x2="83.185" y2="16.51" width="0.1524" layer="21"/>
<wire x1="81.28" y1="15.875" x2="82.55" y2="15.875" width="0.1524" layer="21"/>
<wire x1="83.185" y1="16.51" x2="83.82" y2="15.875" width="0.1524" layer="21"/>
<wire x1="85.09" y1="15.875" x2="85.725" y2="16.51" width="0.1524" layer="21"/>
<wire x1="85.725" y1="16.51" x2="86.36" y2="15.875" width="0.1524" layer="21"/>
<wire x1="87.63" y1="15.875" x2="88.265" y2="16.51" width="0.1524" layer="21"/>
<wire x1="88.265" y1="16.51" x2="88.9" y2="15.875" width="0.1524" layer="21"/>
<wire x1="90.17" y1="15.875" x2="90.805" y2="16.51" width="0.1524" layer="21"/>
<wire x1="90.805" y1="16.51" x2="91.44" y2="15.875" width="0.1524" layer="21"/>
<wire x1="92.71" y1="15.875" x2="93.345" y2="16.51" width="0.1524" layer="21"/>
<wire x1="93.345" y1="16.51" x2="93.98" y2="15.875" width="0.1524" layer="21"/>
<wire x1="95.25" y1="15.875" x2="95.885" y2="16.51" width="0.1524" layer="21"/>
<wire x1="95.885" y1="16.51" x2="96.52" y2="15.875" width="0.1524" layer="21"/>
<wire x1="97.79" y1="15.875" x2="98.425" y2="16.51" width="0.1524" layer="21"/>
<wire x1="83.185" y1="17.78" x2="83.82" y2="18.415" width="0.1524" layer="21"/>
<wire x1="83.82" y1="18.415" x2="85.09" y2="18.415" width="0.1524" layer="21"/>
<wire x1="85.09" y1="18.415" x2="85.725" y2="17.78" width="0.1524" layer="21"/>
<wire x1="85.725" y1="17.78" x2="86.36" y2="18.415" width="0.1524" layer="21"/>
<wire x1="86.36" y1="18.415" x2="87.63" y2="18.415" width="0.1524" layer="21"/>
<wire x1="87.63" y1="18.415" x2="88.265" y2="17.78" width="0.1524" layer="21"/>
<wire x1="88.265" y1="17.78" x2="88.9" y2="18.415" width="0.1524" layer="21"/>
<wire x1="88.9" y1="18.415" x2="90.17" y2="18.415" width="0.1524" layer="21"/>
<wire x1="90.17" y1="18.415" x2="90.805" y2="17.78" width="0.1524" layer="21"/>
<wire x1="90.805" y1="17.78" x2="91.44" y2="18.415" width="0.1524" layer="21"/>
<wire x1="91.44" y1="18.415" x2="92.71" y2="18.415" width="0.1524" layer="21"/>
<wire x1="92.71" y1="18.415" x2="93.345" y2="17.78" width="0.1524" layer="21"/>
<wire x1="93.345" y1="17.78" x2="93.98" y2="18.415" width="0.1524" layer="21"/>
<wire x1="93.98" y1="18.415" x2="95.25" y2="18.415" width="0.1524" layer="21"/>
<wire x1="95.25" y1="18.415" x2="95.885" y2="17.78" width="0.1524" layer="21"/>
<wire x1="95.885" y1="17.78" x2="96.52" y2="18.415" width="0.1524" layer="21"/>
<wire x1="96.52" y1="18.415" x2="97.79" y2="18.415" width="0.1524" layer="21"/>
<wire x1="97.79" y1="18.415" x2="98.425" y2="17.78" width="0.1524" layer="21"/>
<wire x1="98.425" y1="17.78" x2="99.06" y2="18.415" width="0.1524" layer="21"/>
<wire x1="99.06" y1="18.415" x2="100.33" y2="18.415" width="0.1524" layer="21"/>
<wire x1="100.33" y1="18.415" x2="100.965" y2="17.78" width="0.1524" layer="21"/>
<wire x1="100.965" y1="16.51" x2="100.33" y2="15.875" width="0.1524" layer="21"/>
<wire x1="98.425" y1="16.51" x2="99.06" y2="15.875" width="0.1524" layer="21"/>
<wire x1="85.725" y1="17.78" x2="85.725" y2="16.51" width="0.1524" layer="21"/>
<wire x1="88.265" y1="17.78" x2="88.265" y2="16.51" width="0.1524" layer="21"/>
<wire x1="90.805" y1="17.78" x2="90.805" y2="16.51" width="0.1524" layer="21"/>
<wire x1="93.345" y1="17.78" x2="93.345" y2="16.51" width="0.1524" layer="21"/>
<wire x1="95.885" y1="17.78" x2="95.885" y2="16.51" width="0.1524" layer="21"/>
<wire x1="98.425" y1="17.78" x2="98.425" y2="16.51" width="0.1524" layer="21"/>
<wire x1="100.965" y1="17.78" x2="100.965" y2="16.51" width="0.1524" layer="21"/>
<wire x1="99.06" y1="15.875" x2="100.33" y2="15.875" width="0.1524" layer="21"/>
<wire x1="96.52" y1="15.875" x2="97.79" y2="15.875" width="0.1524" layer="21"/>
<wire x1="93.98" y1="15.875" x2="95.25" y2="15.875" width="0.1524" layer="21"/>
<wire x1="91.44" y1="15.875" x2="92.71" y2="15.875" width="0.1524" layer="21"/>
<wire x1="88.9" y1="15.875" x2="90.17" y2="15.875" width="0.1524" layer="21"/>
<wire x1="86.36" y1="15.875" x2="87.63" y2="15.875" width="0.1524" layer="21"/>
<wire x1="83.82" y1="15.875" x2="85.09" y2="15.875" width="0.1524" layer="21"/>
<wire x1="101.6" y1="22.225" x2="101.6" y2="-12.065" width="0.127" layer="21"/>
<wire x1="101.6" y1="-12.065" x2="29.845" y2="-12.065" width="0.127" layer="21"/>
<wire x1="29.845" y1="-12.065" x2="-37.465" y2="-12.065" width="0.127" layer="21"/>
<wire x1="-37.465" y1="-12.065" x2="-37.465" y2="1.905" width="0.127" layer="21"/>
<wire x1="-37.465" y1="1.905" x2="-37.465" y2="9.525" width="0.127" layer="21"/>
<wire x1="-37.465" y1="9.525" x2="-37.465" y2="22.225" width="0.127" layer="21"/>
<wire x1="-37.465" y1="22.225" x2="29.845" y2="22.225" width="0.127" layer="21"/>
<wire x1="29.845" y1="22.225" x2="101.6" y2="22.225" width="0.127" layer="21"/>
<wire x1="31.75" y1="-4.445" x2="32.385" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="32.385" y1="-2.54" x2="31.75" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="31.75" y1="-1.905" x2="32.385" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="32.385" y1="0" x2="31.75" y2="0.635" width="0.1524" layer="21"/>
<wire x1="31.75" y1="0.635" x2="32.385" y2="1.27" width="0.1524" layer="21"/>
<wire x1="32.385" y1="2.54" x2="31.75" y2="3.175" width="0.1524" layer="21"/>
<wire x1="31.75" y1="3.175" x2="32.385" y2="3.81" width="0.1524" layer="21"/>
<wire x1="32.385" y1="5.08" x2="31.75" y2="5.715" width="0.1524" layer="21"/>
<wire x1="31.75" y1="5.715" x2="32.385" y2="6.35" width="0.1524" layer="21"/>
<wire x1="32.385" y1="7.62" x2="31.75" y2="8.255" width="0.1524" layer="21"/>
<wire x1="31.75" y1="8.255" x2="32.385" y2="8.89" width="0.1524" layer="21"/>
<wire x1="32.385" y1="10.16" x2="31.75" y2="10.795" width="0.1524" layer="21"/>
<wire x1="31.75" y1="-4.445" x2="30.48" y2="-4.445" width="0.1524" layer="21"/>
<wire x1="30.48" y1="-4.445" x2="29.845" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="29.845" y1="-3.81" x2="29.845" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="29.845" y1="-2.54" x2="30.48" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="30.48" y1="-1.905" x2="29.845" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="29.845" y1="-1.27" x2="29.845" y2="0" width="0.1524" layer="21"/>
<wire x1="29.845" y1="0" x2="30.48" y2="0.635" width="0.1524" layer="21"/>
<wire x1="30.48" y1="0.635" x2="29.845" y2="1.27" width="0.1524" layer="21"/>
<wire x1="29.845" y1="1.27" x2="29.845" y2="2.54" width="0.1524" layer="21"/>
<wire x1="29.845" y1="2.54" x2="30.48" y2="3.175" width="0.1524" layer="21"/>
<wire x1="30.48" y1="3.175" x2="29.845" y2="3.81" width="0.1524" layer="21"/>
<wire x1="29.845" y1="3.81" x2="29.845" y2="5.08" width="0.1524" layer="21"/>
<wire x1="29.845" y1="5.08" x2="30.48" y2="5.715" width="0.1524" layer="21"/>
<wire x1="30.48" y1="5.715" x2="29.845" y2="6.35" width="0.1524" layer="21"/>
<wire x1="29.845" y1="6.35" x2="29.845" y2="7.62" width="0.1524" layer="21"/>
<wire x1="29.845" y1="7.62" x2="30.48" y2="8.255" width="0.1524" layer="21"/>
<wire x1="30.48" y1="8.255" x2="29.845" y2="8.89" width="0.1524" layer="21"/>
<wire x1="29.845" y1="8.89" x2="29.845" y2="10.16" width="0.1524" layer="21"/>
<wire x1="29.845" y1="10.16" x2="30.48" y2="10.795" width="0.1524" layer="21"/>
<wire x1="30.48" y1="10.795" x2="29.845" y2="11.43" width="0.1524" layer="21"/>
<wire x1="29.845" y1="11.43" x2="29.845" y2="12.7" width="0.1524" layer="21"/>
<wire x1="29.845" y1="12.7" x2="30.48" y2="13.335" width="0.1524" layer="21"/>
<wire x1="30.48" y1="13.335" x2="29.845" y2="13.97" width="0.1524" layer="21"/>
<wire x1="29.845" y1="13.97" x2="29.845" y2="15.24" width="0.1524" layer="21"/>
<wire x1="29.845" y1="15.24" x2="30.48" y2="15.875" width="0.1524" layer="21"/>
<wire x1="31.75" y1="15.875" x2="32.385" y2="15.24" width="0.1524" layer="21"/>
<wire x1="31.75" y1="13.335" x2="32.385" y2="13.97" width="0.1524" layer="21"/>
<wire x1="31.75" y1="13.335" x2="32.385" y2="12.7" width="0.1524" layer="21"/>
<wire x1="31.75" y1="10.795" x2="32.385" y2="11.43" width="0.1524" layer="21"/>
<wire x1="30.48" y1="-1.905" x2="31.75" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="30.48" y1="0.635" x2="31.75" y2="0.635" width="0.1524" layer="21"/>
<wire x1="30.48" y1="3.175" x2="31.75" y2="3.175" width="0.1524" layer="21"/>
<wire x1="30.48" y1="5.715" x2="31.75" y2="5.715" width="0.1524" layer="21"/>
<wire x1="30.48" y1="8.255" x2="31.75" y2="8.255" width="0.1524" layer="21"/>
<wire x1="30.48" y1="10.795" x2="31.75" y2="10.795" width="0.1524" layer="21"/>
<wire x1="30.48" y1="13.335" x2="31.75" y2="13.335" width="0.1524" layer="21"/>
<wire x1="30.48" y1="15.875" x2="31.75" y2="15.875" width="0.1524" layer="21"/>
<wire x1="32.385" y1="13.97" x2="32.385" y2="15.24" width="0.1524" layer="21"/>
<wire x1="32.385" y1="11.43" x2="32.385" y2="12.7" width="0.1524" layer="21"/>
<wire x1="32.385" y1="8.89" x2="32.385" y2="10.16" width="0.1524" layer="21"/>
<wire x1="32.385" y1="6.35" x2="32.385" y2="7.62" width="0.1524" layer="21"/>
<wire x1="32.385" y1="3.81" x2="32.385" y2="5.08" width="0.1524" layer="21"/>
<wire x1="32.385" y1="1.27" x2="32.385" y2="2.54" width="0.1524" layer="21"/>
<wire x1="32.385" y1="-1.27" x2="32.385" y2="0" width="0.1524" layer="21"/>
<wire x1="32.385" y1="-3.81" x2="32.385" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="29.21" y1="-4.445" x2="29.845" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="29.845" y1="-2.54" x2="29.21" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="29.21" y1="-1.905" x2="29.845" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="29.845" y1="0" x2="29.21" y2="0.635" width="0.1524" layer="21"/>
<wire x1="29.21" y1="0.635" x2="29.845" y2="1.27" width="0.1524" layer="21"/>
<wire x1="29.845" y1="2.54" x2="29.21" y2="3.175" width="0.1524" layer="21"/>
<wire x1="29.21" y1="3.175" x2="29.845" y2="3.81" width="0.1524" layer="21"/>
<wire x1="29.845" y1="5.08" x2="29.21" y2="5.715" width="0.1524" layer="21"/>
<wire x1="29.21" y1="5.715" x2="29.845" y2="6.35" width="0.1524" layer="21"/>
<wire x1="29.845" y1="7.62" x2="29.21" y2="8.255" width="0.1524" layer="21"/>
<wire x1="29.21" y1="8.255" x2="29.845" y2="8.89" width="0.1524" layer="21"/>
<wire x1="29.845" y1="10.16" x2="29.21" y2="10.795" width="0.1524" layer="21"/>
<wire x1="29.21" y1="-4.445" x2="27.94" y2="-4.445" width="0.1524" layer="21"/>
<wire x1="27.94" y1="-4.445" x2="27.305" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="27.305" y1="-3.81" x2="27.305" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="27.305" y1="-2.54" x2="27.94" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="27.94" y1="-1.905" x2="27.305" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="27.305" y1="-1.27" x2="27.305" y2="0" width="0.1524" layer="21"/>
<wire x1="27.305" y1="0" x2="27.94" y2="0.635" width="0.1524" layer="21"/>
<wire x1="27.94" y1="0.635" x2="27.305" y2="1.27" width="0.1524" layer="21"/>
<wire x1="27.305" y1="1.27" x2="27.305" y2="2.54" width="0.1524" layer="21"/>
<wire x1="27.305" y1="2.54" x2="27.94" y2="3.175" width="0.1524" layer="21"/>
<wire x1="27.94" y1="3.175" x2="27.305" y2="3.81" width="0.1524" layer="21"/>
<wire x1="27.305" y1="3.81" x2="27.305" y2="5.08" width="0.1524" layer="21"/>
<wire x1="27.305" y1="5.08" x2="27.94" y2="5.715" width="0.1524" layer="21"/>
<wire x1="27.94" y1="5.715" x2="27.305" y2="6.35" width="0.1524" layer="21"/>
<wire x1="27.305" y1="6.35" x2="27.305" y2="7.62" width="0.1524" layer="21"/>
<wire x1="27.305" y1="7.62" x2="27.94" y2="8.255" width="0.1524" layer="21"/>
<wire x1="27.94" y1="8.255" x2="27.305" y2="8.89" width="0.1524" layer="21"/>
<wire x1="27.305" y1="8.89" x2="27.305" y2="10.16" width="0.1524" layer="21"/>
<wire x1="27.305" y1="10.16" x2="27.94" y2="10.795" width="0.1524" layer="21"/>
<wire x1="27.94" y1="10.795" x2="27.305" y2="11.43" width="0.1524" layer="21"/>
<wire x1="27.305" y1="11.43" x2="27.305" y2="12.7" width="0.1524" layer="21"/>
<wire x1="27.305" y1="12.7" x2="27.94" y2="13.335" width="0.1524" layer="21"/>
<wire x1="27.94" y1="13.335" x2="27.305" y2="13.97" width="0.1524" layer="21"/>
<wire x1="27.305" y1="13.97" x2="27.305" y2="15.24" width="0.1524" layer="21"/>
<wire x1="27.305" y1="15.24" x2="27.94" y2="15.875" width="0.1524" layer="21"/>
<wire x1="29.21" y1="15.875" x2="29.845" y2="15.24" width="0.1524" layer="21"/>
<wire x1="29.21" y1="13.335" x2="29.845" y2="13.97" width="0.1524" layer="21"/>
<wire x1="29.21" y1="13.335" x2="29.845" y2="12.7" width="0.1524" layer="21"/>
<wire x1="29.21" y1="10.795" x2="29.845" y2="11.43" width="0.1524" layer="21"/>
<wire x1="27.94" y1="-1.905" x2="29.21" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="27.94" y1="0.635" x2="29.21" y2="0.635" width="0.1524" layer="21"/>
<wire x1="27.94" y1="3.175" x2="29.21" y2="3.175" width="0.1524" layer="21"/>
<wire x1="27.94" y1="5.715" x2="29.21" y2="5.715" width="0.1524" layer="21"/>
<wire x1="27.94" y1="8.255" x2="29.21" y2="8.255" width="0.1524" layer="21"/>
<wire x1="27.94" y1="10.795" x2="29.21" y2="10.795" width="0.1524" layer="21"/>
<wire x1="27.94" y1="13.335" x2="29.21" y2="13.335" width="0.1524" layer="21"/>
<wire x1="27.94" y1="15.875" x2="29.21" y2="15.875" width="0.1524" layer="21"/>
<wire x1="29.845" y1="15.24" x2="29.845" y2="22.225" width="0.1524" layer="21" style="shortdash"/>
<wire x1="29.845" y1="-12.065" x2="29.845" y2="-3.81" width="0.1524" layer="21" style="shortdash"/>
<wire x1="-29.845" y1="9.525" x2="-37.465" y2="9.525" width="0.1524" layer="21"/>
<wire x1="-29.845" y1="9.525" x2="-29.845" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-37.465" y1="1.905" x2="-29.845" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-37.465" y1="9.525" x2="-38.735" y2="9.525" width="0.1524" layer="21"/>
<wire x1="-38.735" y1="1.905" x2="-37.465" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-38.735" y1="9.525" x2="-38.735" y2="1.905" width="0.1524" layer="21"/>
<wire x1="90.805" y1="-1.6002" x2="91.44" y2="-2.2352" width="0.1524" layer="21"/>
<wire x1="92.71" y1="-2.2352" x2="93.345" y2="-1.6002" width="0.1524" layer="21"/>
<wire x1="90.805" y1="-0.3302" x2="91.44" y2="0.3048" width="0.1524" layer="21"/>
<wire x1="91.44" y1="0.3048" x2="92.71" y2="0.3048" width="0.1524" layer="21"/>
<wire x1="92.71" y1="0.3048" x2="93.345" y2="-0.3302" width="0.1524" layer="21"/>
<wire x1="93.345" y1="-0.3302" x2="93.98" y2="0.3048" width="0.1524" layer="21"/>
<wire x1="93.98" y1="0.3048" x2="95.25" y2="0.3048" width="0.1524" layer="21"/>
<wire x1="95.25" y1="0.3048" x2="95.885" y2="-0.3302" width="0.1524" layer="21"/>
<wire x1="95.885" y1="-0.3302" x2="96.52" y2="0.3048" width="0.1524" layer="21"/>
<wire x1="96.52" y1="0.3048" x2="97.79" y2="0.3048" width="0.1524" layer="21"/>
<wire x1="97.79" y1="0.3048" x2="98.425" y2="-0.3302" width="0.1524" layer="21"/>
<wire x1="98.425" y1="-0.3302" x2="99.06" y2="0.3048" width="0.1524" layer="21"/>
<wire x1="99.06" y1="0.3048" x2="100.33" y2="0.3048" width="0.1524" layer="21"/>
<wire x1="100.33" y1="0.3048" x2="100.965" y2="-0.3302" width="0.1524" layer="21"/>
<wire x1="100.965" y1="-1.6002" x2="100.33" y2="-2.2352" width="0.1524" layer="21"/>
<wire x1="98.425" y1="-1.6002" x2="99.06" y2="-2.2352" width="0.1524" layer="21"/>
<wire x1="98.425" y1="-1.6002" x2="97.79" y2="-2.2352" width="0.1524" layer="21"/>
<wire x1="95.885" y1="-1.6002" x2="96.52" y2="-2.2352" width="0.1524" layer="21"/>
<wire x1="95.885" y1="-1.6002" x2="95.25" y2="-2.2352" width="0.1524" layer="21"/>
<wire x1="93.345" y1="-1.6002" x2="93.98" y2="-2.2352" width="0.1524" layer="21"/>
<wire x1="90.805" y1="-0.3302" x2="90.805" y2="-1.6002" width="0.1524" layer="21"/>
<wire x1="93.345" y1="-0.3302" x2="93.345" y2="-1.6002" width="0.1524" layer="21"/>
<wire x1="95.885" y1="-0.3302" x2="95.885" y2="-1.6002" width="0.1524" layer="21"/>
<wire x1="98.425" y1="-0.3302" x2="98.425" y2="-1.6002" width="0.1524" layer="21"/>
<wire x1="100.965" y1="-0.3302" x2="100.965" y2="-1.6002" width="0.1524" layer="21"/>
<wire x1="99.06" y1="-2.2352" x2="100.33" y2="-2.2352" width="0.1524" layer="21"/>
<wire x1="96.52" y1="-2.2352" x2="97.79" y2="-2.2352" width="0.1524" layer="21"/>
<wire x1="93.98" y1="-2.2352" x2="95.25" y2="-2.2352" width="0.1524" layer="21"/>
<wire x1="91.44" y1="-2.2352" x2="92.71" y2="-2.2352" width="0.1524" layer="21"/>
<wire x1="93.345" y1="2.2098" x2="93.98" y2="2.8448" width="0.1524" layer="21"/>
<wire x1="93.98" y1="2.8448" x2="95.25" y2="2.8448" width="0.1524" layer="21"/>
<wire x1="95.25" y1="2.8448" x2="95.885" y2="2.2098" width="0.1524" layer="21"/>
<wire x1="95.885" y1="2.2098" x2="96.52" y2="2.8448" width="0.1524" layer="21"/>
<wire x1="96.52" y1="2.8448" x2="97.79" y2="2.8448" width="0.1524" layer="21"/>
<wire x1="97.79" y1="2.8448" x2="98.425" y2="2.2098" width="0.1524" layer="21"/>
<wire x1="98.425" y1="2.2098" x2="99.06" y2="2.8448" width="0.1524" layer="21"/>
<wire x1="99.06" y1="2.8448" x2="100.33" y2="2.8448" width="0.1524" layer="21"/>
<wire x1="100.33" y1="2.8448" x2="100.965" y2="2.2098" width="0.1524" layer="21"/>
<wire x1="100.965" y1="0.9398" x2="100.33" y2="0.3048" width="0.1524" layer="21"/>
<wire x1="98.425" y1="0.9398" x2="99.06" y2="0.3048" width="0.1524" layer="21"/>
<wire x1="98.425" y1="0.9398" x2="97.79" y2="0.3048" width="0.1524" layer="21"/>
<wire x1="95.885" y1="0.9398" x2="96.52" y2="0.3048" width="0.1524" layer="21"/>
<wire x1="95.885" y1="0.9398" x2="95.25" y2="0.3048" width="0.1524" layer="21"/>
<wire x1="93.345" y1="0.9398" x2="93.98" y2="0.3048" width="0.1524" layer="21"/>
<wire x1="93.345" y1="2.2098" x2="93.345" y2="0.9398" width="0.1524" layer="21"/>
<wire x1="95.885" y1="2.2098" x2="95.885" y2="0.9398" width="0.1524" layer="21"/>
<wire x1="98.425" y1="2.2098" x2="98.425" y2="0.9398" width="0.1524" layer="21"/>
<wire x1="100.965" y1="2.2098" x2="100.965" y2="0.9398" width="0.1524" layer="21"/>
<wire x1="93.345" y1="4.7498" x2="93.98" y2="5.3848" width="0.1524" layer="21"/>
<wire x1="93.98" y1="5.3848" x2="95.25" y2="5.3848" width="0.1524" layer="21"/>
<wire x1="95.25" y1="5.3848" x2="95.885" y2="4.7498" width="0.1524" layer="21"/>
<wire x1="95.885" y1="4.7498" x2="96.52" y2="5.3848" width="0.1524" layer="21"/>
<wire x1="96.52" y1="5.3848" x2="97.79" y2="5.3848" width="0.1524" layer="21"/>
<wire x1="97.79" y1="5.3848" x2="98.425" y2="4.7498" width="0.1524" layer="21"/>
<wire x1="98.425" y1="4.7498" x2="99.06" y2="5.3848" width="0.1524" layer="21"/>
<wire x1="99.06" y1="5.3848" x2="100.33" y2="5.3848" width="0.1524" layer="21"/>
<wire x1="100.33" y1="5.3848" x2="100.965" y2="4.7498" width="0.1524" layer="21"/>
<wire x1="100.965" y1="3.4798" x2="100.33" y2="2.8448" width="0.1524" layer="21"/>
<wire x1="98.425" y1="3.4798" x2="99.06" y2="2.8448" width="0.1524" layer="21"/>
<wire x1="98.425" y1="3.4798" x2="97.79" y2="2.8448" width="0.1524" layer="21"/>
<wire x1="95.885" y1="3.4798" x2="96.52" y2="2.8448" width="0.1524" layer="21"/>
<wire x1="95.885" y1="3.4798" x2="95.25" y2="2.8448" width="0.1524" layer="21"/>
<wire x1="93.345" y1="3.4798" x2="93.98" y2="2.8448" width="0.1524" layer="21"/>
<wire x1="93.345" y1="4.7498" x2="93.345" y2="3.4798" width="0.1524" layer="21"/>
<wire x1="95.885" y1="4.7498" x2="95.885" y2="3.4798" width="0.1524" layer="21"/>
<wire x1="98.425" y1="4.7498" x2="98.425" y2="3.4798" width="0.1524" layer="21"/>
<wire x1="100.965" y1="4.7498" x2="100.965" y2="3.4798" width="0.1524" layer="21"/>
<wire x1="93.345" y1="7.2898" x2="93.98" y2="7.9248" width="0.1524" layer="21"/>
<wire x1="93.98" y1="7.9248" x2="95.25" y2="7.9248" width="0.1524" layer="21"/>
<wire x1="95.25" y1="7.9248" x2="95.885" y2="7.2898" width="0.1524" layer="21"/>
<wire x1="95.885" y1="7.2898" x2="96.52" y2="7.9248" width="0.1524" layer="21"/>
<wire x1="96.52" y1="7.9248" x2="97.79" y2="7.9248" width="0.1524" layer="21"/>
<wire x1="97.79" y1="7.9248" x2="98.425" y2="7.2898" width="0.1524" layer="21"/>
<wire x1="98.425" y1="7.2898" x2="99.06" y2="7.9248" width="0.1524" layer="21"/>
<wire x1="99.06" y1="7.9248" x2="100.33" y2="7.9248" width="0.1524" layer="21"/>
<wire x1="100.33" y1="7.9248" x2="100.965" y2="7.2898" width="0.1524" layer="21"/>
<wire x1="100.965" y1="6.0198" x2="100.33" y2="5.3848" width="0.1524" layer="21"/>
<wire x1="98.425" y1="6.0198" x2="99.06" y2="5.3848" width="0.1524" layer="21"/>
<wire x1="98.425" y1="6.0198" x2="97.79" y2="5.3848" width="0.1524" layer="21"/>
<wire x1="95.885" y1="6.0198" x2="96.52" y2="5.3848" width="0.1524" layer="21"/>
<wire x1="95.885" y1="6.0198" x2="95.25" y2="5.3848" width="0.1524" layer="21"/>
<wire x1="93.345" y1="6.0198" x2="93.98" y2="5.3848" width="0.1524" layer="21"/>
<wire x1="93.345" y1="7.2898" x2="93.345" y2="6.0198" width="0.1524" layer="21"/>
<wire x1="95.885" y1="7.2898" x2="95.885" y2="6.0198" width="0.1524" layer="21"/>
<wire x1="98.425" y1="7.2898" x2="98.425" y2="6.0198" width="0.1524" layer="21"/>
<wire x1="100.965" y1="7.2898" x2="100.965" y2="6.0198" width="0.1524" layer="21"/>
<wire x1="93.345" y1="9.8298" x2="93.98" y2="10.4648" width="0.1524" layer="21"/>
<wire x1="93.98" y1="10.4648" x2="95.25" y2="10.4648" width="0.1524" layer="21"/>
<wire x1="95.25" y1="10.4648" x2="95.885" y2="9.8298" width="0.1524" layer="21"/>
<wire x1="95.885" y1="9.8298" x2="96.52" y2="10.4648" width="0.1524" layer="21"/>
<wire x1="96.52" y1="10.4648" x2="97.79" y2="10.4648" width="0.1524" layer="21"/>
<wire x1="97.79" y1="10.4648" x2="98.425" y2="9.8298" width="0.1524" layer="21"/>
<wire x1="98.425" y1="9.8298" x2="99.06" y2="10.4648" width="0.1524" layer="21"/>
<wire x1="99.06" y1="10.4648" x2="100.33" y2="10.4648" width="0.1524" layer="21"/>
<wire x1="100.33" y1="10.4648" x2="100.965" y2="9.8298" width="0.1524" layer="21"/>
<wire x1="100.965" y1="8.5598" x2="100.33" y2="7.9248" width="0.1524" layer="21"/>
<wire x1="98.425" y1="8.5598" x2="99.06" y2="7.9248" width="0.1524" layer="21"/>
<wire x1="98.425" y1="8.5598" x2="97.79" y2="7.9248" width="0.1524" layer="21"/>
<wire x1="95.885" y1="8.5598" x2="96.52" y2="7.9248" width="0.1524" layer="21"/>
<wire x1="95.885" y1="8.5598" x2="95.25" y2="7.9248" width="0.1524" layer="21"/>
<wire x1="93.345" y1="8.5598" x2="93.98" y2="7.9248" width="0.1524" layer="21"/>
<wire x1="93.345" y1="9.8298" x2="93.345" y2="8.5598" width="0.1524" layer="21"/>
<wire x1="95.885" y1="9.8298" x2="95.885" y2="8.5598" width="0.1524" layer="21"/>
<wire x1="98.425" y1="9.8298" x2="98.425" y2="8.5598" width="0.1524" layer="21"/>
<wire x1="100.965" y1="9.8298" x2="100.965" y2="8.5598" width="0.1524" layer="21"/>
<wire x1="93.345" y1="12.3698" x2="93.98" y2="13.0048" width="0.1524" layer="21"/>
<wire x1="93.98" y1="13.0048" x2="95.25" y2="13.0048" width="0.1524" layer="21"/>
<wire x1="95.25" y1="13.0048" x2="95.885" y2="12.3698" width="0.1524" layer="21"/>
<wire x1="95.885" y1="12.3698" x2="96.52" y2="13.0048" width="0.1524" layer="21"/>
<wire x1="96.52" y1="13.0048" x2="97.79" y2="13.0048" width="0.1524" layer="21"/>
<wire x1="97.79" y1="13.0048" x2="98.425" y2="12.3698" width="0.1524" layer="21"/>
<wire x1="98.425" y1="12.3698" x2="99.06" y2="13.0048" width="0.1524" layer="21"/>
<wire x1="99.06" y1="13.0048" x2="100.33" y2="13.0048" width="0.1524" layer="21"/>
<wire x1="100.33" y1="13.0048" x2="100.965" y2="12.3698" width="0.1524" layer="21"/>
<wire x1="100.965" y1="11.0998" x2="100.33" y2="10.4648" width="0.1524" layer="21"/>
<wire x1="98.425" y1="11.0998" x2="99.06" y2="10.4648" width="0.1524" layer="21"/>
<wire x1="98.425" y1="11.0998" x2="97.79" y2="10.4648" width="0.1524" layer="21"/>
<wire x1="95.885" y1="11.0998" x2="96.52" y2="10.4648" width="0.1524" layer="21"/>
<wire x1="95.885" y1="11.0998" x2="95.25" y2="10.4648" width="0.1524" layer="21"/>
<wire x1="93.345" y1="11.0998" x2="93.98" y2="10.4648" width="0.1524" layer="21"/>
<wire x1="93.345" y1="12.3698" x2="93.345" y2="11.0998" width="0.1524" layer="21"/>
<wire x1="95.885" y1="12.3698" x2="95.885" y2="11.0998" width="0.1524" layer="21"/>
<wire x1="98.425" y1="12.3698" x2="98.425" y2="11.0998" width="0.1524" layer="21"/>
<wire x1="100.965" y1="12.3698" x2="100.965" y2="11.0998" width="0.1524" layer="21"/>
<pad name="1" x="33.655" y="-5.715" drill="1" shape="octagon"/>
<pad name="2" x="36.195" y="-5.715" drill="1" shape="octagon"/>
<pad name="3" x="38.735" y="-5.715" drill="1" shape="octagon"/>
<pad name="4" x="41.275" y="-5.715" drill="1" shape="octagon"/>
<pad name="5" x="43.815" y="-5.715" drill="1" shape="octagon" rot="R90"/>
<pad name="6" x="46.355" y="-5.715" drill="1" shape="octagon"/>
<pad name="7" x="48.895" y="-5.715" drill="1" shape="octagon"/>
<pad name="8" x="51.435" y="-5.715" drill="1" shape="octagon"/>
<pad name="9" x="53.975" y="-5.715" drill="1" shape="octagon"/>
<pad name="10" x="56.515" y="-5.715" drill="1" shape="octagon"/>
<pad name="11" x="59.055" y="-5.715" drill="1" shape="octagon"/>
<pad name="12" x="61.595" y="-5.715" drill="1" shape="octagon"/>
<pad name="13" x="64.135" y="-5.715" drill="1" shape="octagon"/>
<pad name="14" x="66.675" y="-5.715" drill="1" shape="octagon"/>
<pad name="15" x="69.215" y="-5.715" drill="1" shape="octagon"/>
<pad name="16" x="71.755" y="-5.715" drill="1" shape="octagon"/>
<pad name="17" x="74.295" y="-5.715" drill="1" shape="octagon"/>
<pad name="18" x="76.835" y="-5.715" drill="1" shape="octagon"/>
<pad name="19" x="79.375" y="-5.715" drill="1" shape="octagon"/>
<pad name="20" x="81.915" y="-5.715" drill="1" shape="octagon"/>
<pad name="21" x="84.455" y="-5.715" drill="1" shape="octagon"/>
<pad name="22" x="86.995" y="-5.715" drill="1" shape="octagon"/>
<pad name="23" x="89.535" y="-5.715" drill="1" shape="octagon"/>
<pad name="24" x="92.075" y="-5.715" drill="1" shape="octagon"/>
<pad name="25" x="94.615" y="-5.715" drill="1" shape="octagon" rot="R90"/>
<pad name="26" x="97.155" y="-5.715" drill="1" shape="octagon"/>
<pad name="27" x="99.695" y="-5.715" drill="1" shape="octagon"/>
<pad name="28" x="33.655" y="17.145" drill="1" shape="octagon"/>
<pad name="29" x="36.195" y="17.145" drill="1" shape="octagon"/>
<pad name="30" x="38.735" y="17.145" drill="1" shape="octagon"/>
<pad name="31" x="41.275" y="17.145" drill="1" shape="octagon"/>
<pad name="32" x="43.815" y="17.145" drill="1" shape="octagon" rot="R90"/>
<pad name="33" x="46.355" y="17.145" drill="1" shape="octagon"/>
<pad name="34" x="48.895" y="17.145" drill="1" shape="octagon"/>
<pad name="35" x="51.435" y="17.145" drill="1" shape="octagon"/>
<pad name="36" x="53.975" y="17.145" drill="1" shape="octagon"/>
<pad name="37" x="56.515" y="17.145" drill="1" shape="octagon"/>
<pad name="38" x="59.055" y="17.145" drill="1" shape="octagon"/>
<pad name="39" x="61.595" y="17.145" drill="1" shape="octagon"/>
<pad name="40" x="64.135" y="17.145" drill="1" shape="octagon"/>
<pad name="41" x="66.675" y="17.145" drill="1" shape="octagon"/>
<pad name="42" x="69.215" y="17.145" drill="1" shape="octagon"/>
<pad name="43" x="71.755" y="17.145" drill="1" shape="octagon"/>
<pad name="44" x="74.295" y="17.145" drill="1" shape="octagon"/>
<pad name="45" x="76.835" y="17.145" drill="1" shape="octagon"/>
<pad name="46" x="79.375" y="17.145" drill="1" shape="octagon"/>
<pad name="47" x="81.915" y="17.145" drill="1" shape="octagon"/>
<pad name="48" x="84.455" y="17.145" drill="1" shape="octagon"/>
<pad name="49" x="86.995" y="17.145" drill="1" shape="octagon"/>
<pad name="50" x="89.535" y="17.145" drill="1" shape="octagon"/>
<pad name="51" x="92.075" y="17.145" drill="1" shape="octagon"/>
<pad name="52" x="94.615" y="17.145" drill="1" shape="octagon" rot="R90"/>
<pad name="53" x="97.155" y="17.145" drill="1" shape="octagon"/>
<pad name="54" x="99.695" y="17.145" drill="1" shape="octagon"/>
<pad name="55" x="92.075" y="-0.9652" drill="1" shape="octagon"/>
<pad name="56" x="94.615" y="-0.9652" drill="1" shape="octagon"/>
<pad name="57" x="97.155" y="-0.9652" drill="1" shape="octagon"/>
<pad name="58" x="99.695" y="-0.9652" drill="1" shape="octagon"/>
<pad name="59" x="94.615" y="1.5748" drill="1" shape="octagon"/>
<pad name="60" x="97.155" y="1.5748" drill="1" shape="octagon"/>
<pad name="61" x="99.695" y="1.5748" drill="1" shape="octagon"/>
<pad name="62" x="94.615" y="4.1148" drill="1" shape="octagon"/>
<pad name="63" x="97.155" y="4.1148" drill="1" shape="octagon"/>
<pad name="64" x="99.695" y="4.1148" drill="1" shape="octagon"/>
<pad name="65" x="94.615" y="6.6548" drill="1" shape="octagon"/>
<pad name="66" x="97.155" y="6.6548" drill="1" shape="octagon"/>
<pad name="67" x="99.695" y="6.6548" drill="1" shape="octagon"/>
<pad name="68" x="94.615" y="9.1948" drill="1" shape="octagon"/>
<pad name="69" x="97.155" y="9.1948" drill="1" shape="octagon"/>
<pad name="70" x="99.695" y="9.1948" drill="1" shape="octagon"/>
<pad name="71" x="94.615" y="11.7348" drill="1" shape="octagon"/>
<pad name="72" x="97.155" y="11.7348" drill="1" shape="octagon"/>
<pad name="73" x="99.695" y="11.7348" drill="1" shape="octagon"/>
<text x="34.29" y="-2.54" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="34.29" y="-3.81" size="1.27" layer="27">&gt;VALUE</text>
<text x="-16.51" y="4.445" size="1.778" layer="48">LPC-Link   side</text>
<text x="45.72" y="5.08" size="1.778" layer="48">LPC target side</text>
<text x="-6.35" y="5.715" size="1.016" layer="48">TM</text>
<text x="26.289" y="15.494" size="1.778" layer="48" rot="R180">3.3V</text>
<text x="26.289" y="12.954" size="1.778" layer="48" rot="R180">SWDIO/TMS</text>
<text x="26.289" y="10.414" size="1.778" layer="48" rot="R180">SWCLK/TCLK</text>
<text x="26.289" y="7.874" size="1.778" layer="48" rot="R180">SWO/TDO</text>
<text x="26.289" y="5.334" size="1.778" layer="48" rot="R180">NC/TDI</text>
<text x="26.289" y="2.794" size="1.778" layer="48" rot="R180">RESET</text>
<text x="26.289" y="0.254" size="1.778" layer="48" rot="R180">EXT-5V</text>
<text x="26.289" y="-2.286" size="1.778" layer="48" rot="R180">GND</text>
<text x="-35.56" y="5.08" size="1.778" layer="48">USB</text>
<rectangle x1="33.401" y1="-5.969" x2="33.909" y2="-5.461" layer="51"/>
<rectangle x1="35.941" y1="-5.969" x2="36.449" y2="-5.461" layer="51"/>
<rectangle x1="41.021" y1="-5.969" x2="41.529" y2="-5.461" layer="51"/>
<rectangle x1="38.481" y1="-5.969" x2="38.989" y2="-5.461" layer="51"/>
<rectangle x1="43.561" y1="-5.969" x2="44.069" y2="-5.461" layer="51"/>
<rectangle x1="46.101" y1="-5.969" x2="46.609" y2="-5.461" layer="51"/>
<rectangle x1="51.181" y1="-5.969" x2="51.689" y2="-5.461" layer="51"/>
<rectangle x1="56.261" y1="-5.969" x2="56.769" y2="-5.461" layer="51"/>
<rectangle x1="63.881" y1="-5.969" x2="64.389" y2="-5.461" layer="51"/>
<rectangle x1="53.721" y1="-5.969" x2="54.229" y2="-5.461" layer="51"/>
<rectangle x1="48.641" y1="-5.969" x2="49.149" y2="-5.461" layer="51"/>
<rectangle x1="66.421" y1="-5.969" x2="66.929" y2="-5.461" layer="51"/>
<rectangle x1="61.341" y1="-5.969" x2="61.849" y2="-5.461" layer="51"/>
<rectangle x1="58.801" y1="-5.969" x2="59.309" y2="-5.461" layer="51"/>
<rectangle x1="76.581" y1="-5.969" x2="77.089" y2="-5.461" layer="51"/>
<rectangle x1="68.961" y1="-5.969" x2="69.469" y2="-5.461" layer="51"/>
<rectangle x1="74.041" y1="-5.969" x2="74.549" y2="-5.461" layer="51"/>
<rectangle x1="81.661" y1="-5.969" x2="82.169" y2="-5.461" layer="51"/>
<rectangle x1="71.501" y1="-5.969" x2="72.009" y2="-5.461" layer="51"/>
<rectangle x1="79.121" y1="-5.969" x2="79.629" y2="-5.461" layer="51"/>
<rectangle x1="84.201" y1="-5.969" x2="84.709" y2="-5.461" layer="51"/>
<rectangle x1="86.741" y1="-5.969" x2="87.249" y2="-5.461" layer="51"/>
<rectangle x1="91.821" y1="-5.969" x2="92.329" y2="-5.461" layer="51"/>
<rectangle x1="89.281" y1="-5.969" x2="89.789" y2="-5.461" layer="51"/>
<rectangle x1="94.361" y1="-5.969" x2="94.869" y2="-5.461" layer="51"/>
<rectangle x1="96.901" y1="-5.969" x2="97.409" y2="-5.461" layer="51"/>
<rectangle x1="99.441" y1="-5.969" x2="99.949" y2="-5.461" layer="51"/>
<rectangle x1="33.401" y1="16.891" x2="33.909" y2="17.399" layer="51"/>
<rectangle x1="35.941" y1="16.891" x2="36.449" y2="17.399" layer="51"/>
<rectangle x1="41.021" y1="16.891" x2="41.529" y2="17.399" layer="51"/>
<rectangle x1="38.481" y1="16.891" x2="38.989" y2="17.399" layer="51"/>
<rectangle x1="43.561" y1="16.891" x2="44.069" y2="17.399" layer="51"/>
<rectangle x1="46.101" y1="16.891" x2="46.609" y2="17.399" layer="51"/>
<rectangle x1="51.181" y1="16.891" x2="51.689" y2="17.399" layer="51"/>
<rectangle x1="56.261" y1="16.891" x2="56.769" y2="17.399" layer="51"/>
<rectangle x1="63.881" y1="16.891" x2="64.389" y2="17.399" layer="51"/>
<rectangle x1="53.721" y1="16.891" x2="54.229" y2="17.399" layer="51"/>
<rectangle x1="48.641" y1="16.891" x2="49.149" y2="17.399" layer="51"/>
<rectangle x1="66.421" y1="16.891" x2="66.929" y2="17.399" layer="51"/>
<rectangle x1="61.341" y1="16.891" x2="61.849" y2="17.399" layer="51"/>
<rectangle x1="58.801" y1="16.891" x2="59.309" y2="17.399" layer="51"/>
<rectangle x1="76.581" y1="16.891" x2="77.089" y2="17.399" layer="51"/>
<rectangle x1="68.961" y1="16.891" x2="69.469" y2="17.399" layer="51"/>
<rectangle x1="74.041" y1="16.891" x2="74.549" y2="17.399" layer="51"/>
<rectangle x1="81.661" y1="16.891" x2="82.169" y2="17.399" layer="51"/>
<rectangle x1="71.501" y1="16.891" x2="72.009" y2="17.399" layer="51"/>
<rectangle x1="79.121" y1="16.891" x2="79.629" y2="17.399" layer="51"/>
<rectangle x1="84.201" y1="16.891" x2="84.709" y2="17.399" layer="51"/>
<rectangle x1="86.741" y1="16.891" x2="87.249" y2="17.399" layer="51"/>
<rectangle x1="91.821" y1="16.891" x2="92.329" y2="17.399" layer="51"/>
<rectangle x1="89.281" y1="16.891" x2="89.789" y2="17.399" layer="51"/>
<rectangle x1="94.361" y1="16.891" x2="94.869" y2="17.399" layer="51"/>
<rectangle x1="96.901" y1="16.891" x2="97.409" y2="17.399" layer="51"/>
<rectangle x1="99.441" y1="16.891" x2="99.949" y2="17.399" layer="51"/>
<rectangle x1="30.861" y1="-3.429" x2="31.369" y2="-2.921" layer="51" rot="R90"/>
<rectangle x1="30.861" y1="-0.889" x2="31.369" y2="-0.381" layer="51" rot="R90"/>
<rectangle x1="30.861" y1="4.191" x2="31.369" y2="4.699" layer="51" rot="R90"/>
<rectangle x1="30.861" y1="1.651" x2="31.369" y2="2.159" layer="51" rot="R90"/>
<rectangle x1="30.861" y1="6.731" x2="31.369" y2="7.239" layer="51" rot="R90"/>
<rectangle x1="30.861" y1="9.271" x2="31.369" y2="9.779" layer="51" rot="R90"/>
<rectangle x1="30.861" y1="14.351" x2="31.369" y2="14.859" layer="51" rot="R90"/>
<rectangle x1="30.861" y1="11.811" x2="31.369" y2="12.319" layer="51" rot="R90"/>
<rectangle x1="28.321" y1="-3.429" x2="28.829" y2="-2.921" layer="51" rot="R90"/>
<rectangle x1="28.321" y1="-0.889" x2="28.829" y2="-0.381" layer="51" rot="R90"/>
<rectangle x1="28.321" y1="4.191" x2="28.829" y2="4.699" layer="51" rot="R90"/>
<rectangle x1="28.321" y1="1.651" x2="28.829" y2="2.159" layer="51" rot="R90"/>
<rectangle x1="28.321" y1="6.731" x2="28.829" y2="7.239" layer="51" rot="R90"/>
<rectangle x1="28.321" y1="9.271" x2="28.829" y2="9.779" layer="51" rot="R90"/>
<rectangle x1="28.321" y1="14.351" x2="28.829" y2="14.859" layer="51" rot="R90"/>
<rectangle x1="28.321" y1="11.811" x2="28.829" y2="12.319" layer="51" rot="R90"/>
<rectangle x1="91.821" y1="-1.2192" x2="92.329" y2="-0.7112" layer="51"/>
<rectangle x1="96.901" y1="-1.2192" x2="97.409" y2="-0.7112" layer="51"/>
<rectangle x1="99.441" y1="-1.2192" x2="99.949" y2="-0.7112" layer="51"/>
<rectangle x1="94.361" y1="-1.2192" x2="94.869" y2="-0.7112" layer="51"/>
<rectangle x1="96.901" y1="1.3208" x2="97.409" y2="1.8288" layer="51"/>
<rectangle x1="99.441" y1="1.3208" x2="99.949" y2="1.8288" layer="51"/>
<rectangle x1="94.361" y1="1.3208" x2="94.869" y2="1.8288" layer="51"/>
<rectangle x1="96.901" y1="3.8608" x2="97.409" y2="4.3688" layer="51"/>
<rectangle x1="99.441" y1="3.8608" x2="99.949" y2="4.3688" layer="51"/>
<rectangle x1="94.361" y1="3.8608" x2="94.869" y2="4.3688" layer="51"/>
<rectangle x1="96.901" y1="6.4008" x2="97.409" y2="6.9088" layer="51"/>
<rectangle x1="99.441" y1="6.4008" x2="99.949" y2="6.9088" layer="51"/>
<rectangle x1="94.361" y1="6.4008" x2="94.869" y2="6.9088" layer="51"/>
<rectangle x1="96.901" y1="8.9408" x2="97.409" y2="9.4488" layer="51"/>
<rectangle x1="99.441" y1="8.9408" x2="99.949" y2="9.4488" layer="51"/>
<rectangle x1="94.361" y1="8.9408" x2="94.869" y2="9.4488" layer="51"/>
<rectangle x1="96.901" y1="11.4808" x2="97.409" y2="11.9888" layer="51"/>
<rectangle x1="99.441" y1="11.4808" x2="99.949" y2="11.9888" layer="51"/>
<rectangle x1="94.361" y1="11.4808" x2="94.869" y2="11.9888" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="LPCXPRESSO_LPC1769">
<wire x1="-26.67" y1="-33.02" x2="-21.59" y2="-33.02" width="0.4064" layer="94"/>
<wire x1="-24.765" y1="-6.985" x2="-24.765" y2="-8.255" width="0.254" layer="94" curve="180" cap="flat"/>
<wire x1="-24.765" y1="-9.525" x2="-24.765" y2="-10.795" width="0.254" layer="94" curve="180" cap="flat"/>
<wire x1="-24.765" y1="-12.065" x2="-24.765" y2="-13.335" width="0.254" layer="94" curve="180" cap="flat"/>
<wire x1="-24.765" y1="-1.905" x2="-24.765" y2="-3.175" width="0.254" layer="94" curve="180" cap="flat"/>
<wire x1="-24.765" y1="-4.445" x2="-24.765" y2="-5.715" width="0.254" layer="94" curve="180" cap="flat"/>
<wire x1="-24.765" y1="5.715" x2="-24.765" y2="4.445" width="0.254" layer="94" curve="180" cap="flat"/>
<wire x1="-24.765" y1="3.175" x2="-24.765" y2="1.905" width="0.254" layer="94" curve="180" cap="flat"/>
<wire x1="-24.765" y1="0.635" x2="-24.765" y2="-0.635" width="0.254" layer="94" curve="180" cap="flat"/>
<wire x1="-24.765" y1="10.795" x2="-24.765" y2="9.525" width="0.254" layer="94" curve="180" cap="flat"/>
<wire x1="-24.765" y1="8.255" x2="-24.765" y2="6.985" width="0.254" layer="94" curve="180" cap="flat"/>
<wire x1="-24.765" y1="18.415" x2="-24.765" y2="17.145" width="0.254" layer="94" curve="180" cap="flat"/>
<wire x1="-24.765" y1="15.875" x2="-24.765" y2="14.605" width="0.254" layer="94" curve="180" cap="flat"/>
<wire x1="-24.765" y1="13.335" x2="-24.765" y2="12.065" width="0.254" layer="94" curve="180" cap="flat"/>
<wire x1="-24.765" y1="23.495" x2="-24.765" y2="22.225" width="0.254" layer="94" curve="180" cap="flat"/>
<wire x1="-24.765" y1="20.955" x2="-24.765" y2="19.685" width="0.254" layer="94" curve="180" cap="flat"/>
<wire x1="-24.765" y1="31.115" x2="-24.765" y2="29.845" width="0.254" layer="94" curve="180" cap="flat"/>
<wire x1="-24.765" y1="28.575" x2="-24.765" y2="27.305" width="0.254" layer="94" curve="180" cap="flat"/>
<wire x1="-24.765" y1="26.035" x2="-24.765" y2="24.765" width="0.254" layer="94" curve="180" cap="flat"/>
<wire x1="-21.59" y1="38.1" x2="-21.59" y2="-33.02" width="0.4064" layer="94"/>
<wire x1="-26.67" y1="-33.02" x2="-26.67" y2="38.1" width="0.4064" layer="94"/>
<wire x1="-21.59" y1="38.1" x2="-26.67" y2="38.1" width="0.4064" layer="94"/>
<wire x1="-24.765" y1="36.195" x2="-24.765" y2="34.925" width="0.254" layer="94" curve="180" cap="flat"/>
<wire x1="-24.765" y1="33.655" x2="-24.765" y2="32.385" width="0.254" layer="94" curve="180" cap="flat"/>
<wire x1="-24.765" y1="-14.605" x2="-24.765" y2="-15.875" width="0.254" layer="94" curve="180" cap="flat"/>
<wire x1="-24.765" y1="-17.145" x2="-24.765" y2="-18.415" width="0.254" layer="94" curve="180" cap="flat"/>
<wire x1="-24.765" y1="-19.685" x2="-24.765" y2="-20.955" width="0.254" layer="94" curve="180" cap="flat"/>
<wire x1="-24.765" y1="-22.225" x2="-24.765" y2="-23.495" width="0.254" layer="94" curve="180" cap="flat"/>
<wire x1="-24.765" y1="-24.765" x2="-24.765" y2="-26.035" width="0.254" layer="94" curve="180" cap="flat"/>
<wire x1="-24.765" y1="-27.305" x2="-24.765" y2="-28.575" width="0.254" layer="94" curve="180" cap="flat"/>
<wire x1="-24.765" y1="-29.845" x2="-24.765" y2="-31.115" width="0.254" layer="94" curve="180" cap="flat"/>
<wire x1="34.29" y1="-33.02" x2="29.21" y2="-33.02" width="0.4064" layer="94"/>
<wire x1="32.385" y1="-6.985" x2="32.385" y2="-8.255" width="0.254" layer="94" curve="-180" cap="flat"/>
<wire x1="32.385" y1="-9.525" x2="32.385" y2="-10.795" width="0.254" layer="94" curve="-180" cap="flat"/>
<wire x1="32.385" y1="-12.065" x2="32.385" y2="-13.335" width="0.254" layer="94" curve="-180" cap="flat"/>
<wire x1="32.385" y1="-1.905" x2="32.385" y2="-3.175" width="0.254" layer="94" curve="-180" cap="flat"/>
<wire x1="32.385" y1="-4.445" x2="32.385" y2="-5.715" width="0.254" layer="94" curve="-180" cap="flat"/>
<wire x1="32.385" y1="5.715" x2="32.385" y2="4.445" width="0.254" layer="94" curve="-180" cap="flat"/>
<wire x1="32.385" y1="3.175" x2="32.385" y2="1.905" width="0.254" layer="94" curve="-180" cap="flat"/>
<wire x1="32.385" y1="0.635" x2="32.385" y2="-0.635" width="0.254" layer="94" curve="-180" cap="flat"/>
<wire x1="32.385" y1="10.795" x2="32.385" y2="9.525" width="0.254" layer="94" curve="-180" cap="flat"/>
<wire x1="32.385" y1="8.255" x2="32.385" y2="6.985" width="0.254" layer="94" curve="-180" cap="flat"/>
<wire x1="32.385" y1="18.415" x2="32.385" y2="17.145" width="0.254" layer="94" curve="-180" cap="flat"/>
<wire x1="32.385" y1="15.875" x2="32.385" y2="14.605" width="0.254" layer="94" curve="-180" cap="flat"/>
<wire x1="32.385" y1="13.335" x2="32.385" y2="12.065" width="0.254" layer="94" curve="-180" cap="flat"/>
<wire x1="32.385" y1="23.495" x2="32.385" y2="22.225" width="0.254" layer="94" curve="-180" cap="flat"/>
<wire x1="32.385" y1="20.955" x2="32.385" y2="19.685" width="0.254" layer="94" curve="-180" cap="flat"/>
<wire x1="32.385" y1="31.115" x2="32.385" y2="29.845" width="0.254" layer="94" curve="-180" cap="flat"/>
<wire x1="32.385" y1="28.575" x2="32.385" y2="27.305" width="0.254" layer="94" curve="-180" cap="flat"/>
<wire x1="32.385" y1="26.035" x2="32.385" y2="24.765" width="0.254" layer="94" curve="-180" cap="flat"/>
<wire x1="29.21" y1="38.1" x2="29.21" y2="-33.02" width="0.4064" layer="94"/>
<wire x1="34.29" y1="-33.02" x2="34.29" y2="38.1" width="0.4064" layer="94"/>
<wire x1="29.21" y1="38.1" x2="34.29" y2="38.1" width="0.4064" layer="94"/>
<wire x1="32.385" y1="36.195" x2="32.385" y2="34.925" width="0.254" layer="94" curve="-180" cap="flat"/>
<wire x1="32.385" y1="33.655" x2="32.385" y2="32.385" width="0.254" layer="94" curve="-180" cap="flat"/>
<wire x1="32.385" y1="-14.605" x2="32.385" y2="-15.875" width="0.254" layer="94" curve="-180" cap="flat"/>
<wire x1="32.385" y1="-17.145" x2="32.385" y2="-18.415" width="0.254" layer="94" curve="-180" cap="flat"/>
<wire x1="32.385" y1="-19.685" x2="32.385" y2="-20.955" width="0.254" layer="94" curve="-180" cap="flat"/>
<wire x1="32.385" y1="-22.225" x2="32.385" y2="-23.495" width="0.254" layer="94" curve="-180" cap="flat"/>
<wire x1="32.385" y1="-24.765" x2="32.385" y2="-26.035" width="0.254" layer="94" curve="-180" cap="flat"/>
<wire x1="32.385" y1="-27.305" x2="32.385" y2="-28.575" width="0.254" layer="94" curve="-180" cap="flat"/>
<wire x1="32.385" y1="-29.845" x2="32.385" y2="-31.115" width="0.254" layer="94" curve="-180" cap="flat"/>
<wire x1="-26.67" y1="-73.66" x2="34.29" y2="-73.66" width="0.4064" layer="94"/>
<wire x1="0.635" y1="-55.245" x2="0.635" y2="-56.515" width="0.254" layer="94" curve="180" cap="flat"/>
<wire x1="6.985" y1="-51.435" x2="7.62" y2="-50.8" width="0.254" layer="94" curve="90" cap="flat"/>
<wire x1="7.62" y1="-50.8" x2="6.985" y2="-50.165" width="0.254" layer="94" curve="90" cap="flat"/>
<wire x1="6.985" y1="-53.975" x2="7.62" y2="-53.34" width="0.254" layer="94" curve="90" cap="flat"/>
<wire x1="7.62" y1="-53.34" x2="6.985" y2="-52.705" width="0.254" layer="94" curve="90" cap="flat"/>
<wire x1="0.635" y1="-50.165" x2="0.635" y2="-51.435" width="0.254" layer="94" curve="180" cap="flat"/>
<wire x1="0.635" y1="-52.705" x2="0.635" y2="-53.975" width="0.254" layer="94" curve="180" cap="flat"/>
<wire x1="-12.065" y1="-50.165" x2="-12.065" y2="-51.435" width="0.254" layer="94" curve="180" cap="flat"/>
<wire x1="-12.065" y1="-52.705" x2="-12.065" y2="-53.975" width="0.254" layer="94" curve="180" cap="flat"/>
<wire x1="-12.065" y1="-55.245" x2="-12.065" y2="-56.515" width="0.254" layer="94" curve="180" cap="flat"/>
<wire x1="-24.765" y1="-52.705" x2="-24.765" y2="-53.975" width="0.254" layer="94" curve="180" cap="flat"/>
<wire x1="-24.765" y1="-55.245" x2="-24.765" y2="-56.515" width="0.254" layer="94" curve="180" cap="flat"/>
<wire x1="-24.765" y1="-47.625" x2="-24.765" y2="-48.895" width="0.254" layer="94" curve="180" cap="flat"/>
<wire x1="-24.765" y1="-50.165" x2="-24.765" y2="-51.435" width="0.254" layer="94" curve="180" cap="flat"/>
<wire x1="34.29" y1="-45.72" x2="34.29" y2="-73.66" width="0.4064" layer="94"/>
<wire x1="-26.67" y1="-73.66" x2="-26.67" y2="-45.72" width="0.4064" layer="94"/>
<wire x1="6.985" y1="-56.515" x2="7.62" y2="-55.88" width="0.254" layer="94" curve="90" cap="flat"/>
<wire x1="7.62" y1="-55.88" x2="6.985" y2="-55.245" width="0.254" layer="94" curve="90" cap="flat"/>
<wire x1="19.685" y1="-51.435" x2="20.32" y2="-50.8" width="0.254" layer="94" curve="90" cap="flat"/>
<wire x1="20.32" y1="-50.8" x2="19.685" y2="-50.165" width="0.254" layer="94" curve="90" cap="flat"/>
<wire x1="19.685" y1="-53.975" x2="20.32" y2="-53.34" width="0.254" layer="94" curve="90" cap="flat"/>
<wire x1="20.32" y1="-53.34" x2="19.685" y2="-52.705" width="0.254" layer="94" curve="90" cap="flat"/>
<wire x1="19.685" y1="-56.515" x2="20.32" y2="-55.88" width="0.254" layer="94" curve="90" cap="flat"/>
<wire x1="20.32" y1="-55.88" x2="19.685" y2="-55.245" width="0.254" layer="94" curve="90" cap="flat"/>
<wire x1="32.385" y1="-51.435" x2="32.385" y2="-50.165" width="0.254" layer="94" curve="180" cap="flat"/>
<wire x1="32.385" y1="-53.975" x2="32.385" y2="-52.705" width="0.254" layer="94" curve="180" cap="flat"/>
<wire x1="32.385" y1="-56.515" x2="32.385" y2="-55.245" width="0.254" layer="94" curve="180" cap="flat"/>
<wire x1="-26.67" y1="-45.72" x2="34.29" y2="-45.72" width="0.4064" layer="94"/>
<wire x1="-25.4" y1="-58.42" x2="-20.32" y2="-58.42" width="0.1524" layer="94"/>
<wire x1="-20.32" y1="-58.42" x2="-20.32" y2="-50.8" width="0.1524" layer="94"/>
<wire x1="-25.4" y1="-60.96" x2="-17.78" y2="-60.96" width="0.1524" layer="94"/>
<wire x1="-17.78" y1="-60.96" x2="-17.78" y2="-53.34" width="0.1524" layer="94"/>
<wire x1="-25.4" y1="-63.5" x2="-15.24" y2="-63.5" width="0.1524" layer="94"/>
<wire x1="-15.24" y1="-63.5" x2="-15.24" y2="-55.88" width="0.1524" layer="94"/>
<wire x1="-15.24" y1="-55.88" x2="-12.7" y2="-55.88" width="0.1524" layer="94"/>
<wire x1="-17.78" y1="-53.34" x2="-12.7" y2="-53.34" width="0.1524" layer="94"/>
<wire x1="-20.32" y1="-50.8" x2="-12.7" y2="-50.8" width="0.1524" layer="94"/>
<wire x1="-25.4" y1="-66.04" x2="-7.62" y2="-66.04" width="0.1524" layer="94"/>
<wire x1="-7.62" y1="-66.04" x2="-7.62" y2="-50.8" width="0.1524" layer="94"/>
<wire x1="-25.4" y1="-68.58" x2="-5.08" y2="-68.58" width="0.1524" layer="94"/>
<wire x1="-5.08" y1="-68.58" x2="-5.08" y2="-53.34" width="0.1524" layer="94"/>
<wire x1="-25.4" y1="-71.12" x2="-2.54" y2="-71.12" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-71.12" x2="-2.54" y2="-55.88" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-55.88" x2="0" y2="-55.88" width="0.1524" layer="94"/>
<wire x1="-5.08" y1="-53.34" x2="0" y2="-53.34" width="0.1524" layer="94"/>
<wire x1="-7.62" y1="-50.8" x2="0" y2="-50.8" width="0.1524" layer="94"/>
<wire x1="33.02" y1="-66.04" x2="15.24" y2="-66.04" width="0.1524" layer="94"/>
<wire x1="15.24" y1="-66.04" x2="15.24" y2="-50.8" width="0.1524" layer="94"/>
<wire x1="33.02" y1="-68.58" x2="12.7" y2="-68.58" width="0.1524" layer="94"/>
<wire x1="12.7" y1="-68.58" x2="12.7" y2="-53.34" width="0.1524" layer="94"/>
<wire x1="33.02" y1="-71.12" x2="10.16" y2="-71.12" width="0.1524" layer="94"/>
<wire x1="10.16" y1="-71.12" x2="10.16" y2="-55.88" width="0.1524" layer="94"/>
<wire x1="10.16" y1="-55.88" x2="7.62" y2="-55.88" width="0.1524" layer="94"/>
<wire x1="12.7" y1="-53.34" x2="7.62" y2="-53.34" width="0.1524" layer="94"/>
<wire x1="15.24" y1="-50.8" x2="7.62" y2="-50.8" width="0.1524" layer="94"/>
<wire x1="20.32" y1="-50.8" x2="27.94" y2="-50.8" width="0.1524" layer="94"/>
<wire x1="27.94" y1="-50.8" x2="27.94" y2="-58.42" width="0.1524" layer="94"/>
<wire x1="20.32" y1="-53.34" x2="25.4" y2="-53.34" width="0.1524" layer="94"/>
<wire x1="25.4" y1="-53.34" x2="25.4" y2="-60.96" width="0.1524" layer="94"/>
<wire x1="20.32" y1="-55.88" x2="22.86" y2="-55.88" width="0.1524" layer="94"/>
<wire x1="22.86" y1="-55.88" x2="22.86" y2="-63.5" width="0.1524" layer="94"/>
<wire x1="22.86" y1="-63.5" x2="33.02" y2="-63.5" width="0.1524" layer="94"/>
<wire x1="25.4" y1="-60.96" x2="33.02" y2="-60.96" width="0.1524" layer="94"/>
<wire x1="27.94" y1="-58.42" x2="33.02" y2="-58.42" width="0.1524" layer="94"/>
<text x="-20.32" y="38.1" size="1.778" layer="96">&gt;VALUE</text>
<text x="-20.32" y="40.64" size="1.778" layer="95">&gt;NAME</text>
<text x="-20.32" y="32.004" size="1.778" layer="97">VIN (4.5-5.5V)</text>
<text x="-20.32" y="34.544" size="1.778" layer="97">GND</text>
<text x="-20.32" y="29.464" size="1.778" layer="97">VBAT_IN</text>
<text x="-20.32" y="26.924" size="1.778" layer="97">RESET_N</text>
<text x="-20.32" y="24.384" size="1.778" layer="97">P0.9</text>
<text x="-20.32" y="21.844" size="1.778" layer="97">P0.8</text>
<text x="-20.32" y="19.304" size="1.778" layer="97">P0.7</text>
<text x="-20.32" y="16.764" size="1.778" layer="97">P0.6</text>
<text x="-20.32" y="14.224" size="1.778" layer="97">P0.0</text>
<text x="-20.32" y="9.144" size="1.778" layer="97">P0.18</text>
<text x="-20.32" y="4.064" size="1.778" layer="97">P0.15</text>
<text x="-20.32" y="6.604" size="1.778" layer="97">P0.17</text>
<text x="-20.32" y="1.524" size="1.778" layer="97">P0.16</text>
<text x="-20.32" y="-1.016" size="1.778" layer="97">P0.23</text>
<text x="-20.32" y="-3.556" size="1.778" layer="97">P0.24</text>
<text x="-20.32" y="-6.096" size="1.778" layer="97">P0.25</text>
<text x="-20.32" y="-8.636" size="1.778" layer="97">P0.26</text>
<text x="-20.32" y="11.684" size="1.778" layer="97">P0.1</text>
<text x="-20.32" y="-11.176" size="1.778" layer="97">P1.30</text>
<text x="-20.32" y="-13.716" size="1.778" layer="97">P1.31</text>
<text x="-20.32" y="-18.796" size="1.778" layer="97">P0.3</text>
<text x="-20.32" y="-16.256" size="1.778" layer="97">P0.2</text>
<text x="-20.32" y="-21.336" size="1.778" layer="97">P0.21</text>
<text x="-20.32" y="-23.876" size="1.778" layer="97">P0.22</text>
<text x="-20.32" y="-26.416" size="1.778" layer="97">P0.27</text>
<text x="-20.32" y="-28.956" size="1.778" layer="97">P0.28</text>
<text x="-20.32" y="-31.496" size="1.778" layer="97">P2.13</text>
<text x="15.24" y="32.004" size="1.778" layer="97">NC</text>
<text x="15.24" y="34.544" size="1.778" layer="97">3.3V</text>
<text x="15.24" y="29.464" size="1.778" layer="97">NC</text>
<text x="15.24" y="26.924" size="1.778" layer="97">NC</text>
<text x="15.24" y="24.384" size="1.778" layer="97">ETH_RDN</text>
<text x="15.24" y="21.844" size="1.778" layer="97">ETH_RDP</text>
<text x="15.24" y="19.304" size="1.778" layer="97">ETH_TDN</text>
<text x="15.24" y="16.764" size="1.778" layer="97">ETH_TDP</text>
<text x="15.24" y="14.224" size="1.778" layer="97">USB_DM</text>
<text x="15.24" y="11.684" size="1.778" layer="97">USB_DP</text>
<text x="15.24" y="6.604" size="1.778" layer="97">P0.5</text>
<text x="15.24" y="9.144" size="1.778" layer="97">P0.4</text>
<text x="15.24" y="4.064" size="1.778" layer="97">P0.10</text>
<text x="15.24" y="1.524" size="1.778" layer="97">P0.11</text>
<text x="15.24" y="-1.016" size="1.778" layer="97">P2.0</text>
<text x="15.24" y="-3.556" size="1.778" layer="97">P2.1</text>
<text x="15.24" y="-6.096" size="1.778" layer="97">P2.2</text>
<text x="15.24" y="-8.636" size="1.778" layer="97">P2.3</text>
<text x="15.24" y="-11.176" size="1.778" layer="97">P2.4</text>
<text x="15.24" y="-13.716" size="1.778" layer="97">P2.5</text>
<text x="15.24" y="-18.796" size="1.778" layer="97">P2.7</text>
<text x="15.24" y="-16.256" size="1.778" layer="97">P2.6</text>
<text x="15.24" y="-21.336" size="1.778" layer="97">P2.8</text>
<text x="15.24" y="-23.876" size="1.778" layer="97">P2.10</text>
<text x="15.24" y="-26.416" size="1.778" layer="97">P2.11</text>
<text x="15.24" y="-28.956" size="1.778" layer="97">P2.12</text>
<text x="15.24" y="-31.496" size="1.778" layer="97">GND</text>
<text x="-27.94" y="-36.576" size="1.778" layer="97">P2.9</text>
<text x="-27.94" y="-39.116" size="1.778" layer="97">P0.20</text>
<text x="-27.94" y="-44.196" size="1.778" layer="97">P4.29</text>
<text x="-27.94" y="-41.656" size="1.778" layer="97">P0.19</text>
<text x="-15.24" y="-39.116" size="1.778" layer="97">P4.28</text>
<text x="-15.24" y="-41.656" size="1.778" layer="97">P3.26</text>
<text x="-15.24" y="-44.196" size="1.778" layer="97">P3.25</text>
<text x="-2.54" y="-39.116" size="1.778" layer="97">P1.29</text>
<text x="-2.54" y="-41.656" size="1.778" layer="97">P1.28</text>
<text x="-2.54" y="-44.196" size="1.778" layer="97">P1.27</text>
<text x="7.62" y="-44.196" size="1.778" layer="97">P1.24</text>
<text x="7.62" y="-39.116" size="1.778" layer="97">P1.26</text>
<text x="17.78" y="-44.196" size="1.778" layer="97">P1.21</text>
<text x="7.62" y="-41.656" size="1.778" layer="97">P1.25</text>
<text x="17.78" y="-41.656" size="1.778" layer="97">P1.22</text>
<text x="17.78" y="-39.116" size="1.778" layer="97">P1.23</text>
<text x="30.48" y="-44.196" size="1.778" layer="97">P1.18</text>
<text x="30.48" y="-41.656" size="1.778" layer="97">P1.19</text>
<text x="30.48" y="-39.116" size="1.778" layer="97">P1.20</text>
<pin name="20" x="-30.48" y="-12.7" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="19" x="-30.48" y="-10.16" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="18" x="-30.48" y="-7.62" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="17" x="-30.48" y="-5.08" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="16" x="-30.48" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="15" x="-30.48" y="0" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="14" x="-30.48" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="13" x="-30.48" y="5.08" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="12" x="-30.48" y="7.62" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="11" x="-30.48" y="10.16" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="10" x="-30.48" y="12.7" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="9" x="-30.48" y="15.24" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="8" x="-30.48" y="17.78" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="7" x="-30.48" y="20.32" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="6" x="-30.48" y="22.86" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="5" x="-30.48" y="25.4" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="4" x="-30.48" y="27.94" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="3" x="-30.48" y="30.48" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="2" x="-30.48" y="33.02" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="1" x="-30.48" y="35.56" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="21" x="-30.48" y="-15.24" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="22" x="-30.48" y="-17.78" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="23" x="-30.48" y="-20.32" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="24" x="-30.48" y="-22.86" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="25" x="-30.48" y="-25.4" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="26" x="-30.48" y="-27.94" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="27" x="-30.48" y="-30.48" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="47" x="38.1" y="-12.7" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="46" x="38.1" y="-10.16" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="45" x="38.1" y="-7.62" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="44" x="38.1" y="-5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="43" x="38.1" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="42" x="38.1" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="41" x="38.1" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="40" x="38.1" y="5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="39" x="38.1" y="7.62" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="38" x="38.1" y="10.16" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="37" x="38.1" y="12.7" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="36" x="38.1" y="15.24" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="35" x="38.1" y="17.78" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="34" x="38.1" y="20.32" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="33" x="38.1" y="22.86" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="32" x="38.1" y="25.4" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="31" x="38.1" y="27.94" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="30" x="38.1" y="30.48" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="29" x="38.1" y="33.02" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="28" x="38.1" y="35.56" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="48" x="38.1" y="-15.24" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="49" x="38.1" y="-17.78" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="50" x="38.1" y="-20.32" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="51" x="38.1" y="-22.86" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="52" x="38.1" y="-25.4" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="53" x="38.1" y="-27.94" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="54" x="38.1" y="-30.48" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="66" x="38.1" y="-68.58" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="67" x="38.1" y="-71.12" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="64" x="-30.48" y="-71.12" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="63" x="-30.48" y="-68.58" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="62" x="-30.48" y="-66.04" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="61" x="-30.48" y="-63.5" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="60" x="-30.48" y="-60.96" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="59" x="-30.48" y="-58.42" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="58" x="-30.48" y="-55.88" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="57" x="-30.48" y="-53.34" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="56" x="-30.48" y="-50.8" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="55" x="-30.48" y="-48.26" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="65" x="38.1" y="-66.04" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="68" x="38.1" y="-58.42" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="69" x="38.1" y="-60.96" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="70" x="38.1" y="-63.5" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="73" x="38.1" y="-55.88" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="72" x="38.1" y="-53.34" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="71" x="38.1" y="-50.8" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="LPCXPRESSO_LPC1769" prefix="LPC" uservalue="yes">
<gates>
<gate name="G$1" symbol="LPCXPRESSO_LPC1769" x="0" y="0"/>
</gates>
<devices>
<device name="" package="LPCXPRESSO_LPC1769">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="11" pad="11"/>
<connect gate="G$1" pin="12" pad="12"/>
<connect gate="G$1" pin="13" pad="13"/>
<connect gate="G$1" pin="14" pad="14"/>
<connect gate="G$1" pin="15" pad="15"/>
<connect gate="G$1" pin="16" pad="16"/>
<connect gate="G$1" pin="17" pad="17"/>
<connect gate="G$1" pin="18" pad="18"/>
<connect gate="G$1" pin="19" pad="19"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="20" pad="20"/>
<connect gate="G$1" pin="21" pad="21"/>
<connect gate="G$1" pin="22" pad="22"/>
<connect gate="G$1" pin="23" pad="23"/>
<connect gate="G$1" pin="24" pad="24"/>
<connect gate="G$1" pin="25" pad="25"/>
<connect gate="G$1" pin="26" pad="26"/>
<connect gate="G$1" pin="27" pad="27"/>
<connect gate="G$1" pin="28" pad="28"/>
<connect gate="G$1" pin="29" pad="29"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="30" pad="30"/>
<connect gate="G$1" pin="31" pad="31"/>
<connect gate="G$1" pin="32" pad="32"/>
<connect gate="G$1" pin="33" pad="33"/>
<connect gate="G$1" pin="34" pad="34"/>
<connect gate="G$1" pin="35" pad="35"/>
<connect gate="G$1" pin="36" pad="36"/>
<connect gate="G$1" pin="37" pad="37"/>
<connect gate="G$1" pin="38" pad="38"/>
<connect gate="G$1" pin="39" pad="39"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="40" pad="40"/>
<connect gate="G$1" pin="41" pad="41"/>
<connect gate="G$1" pin="42" pad="42"/>
<connect gate="G$1" pin="43" pad="43"/>
<connect gate="G$1" pin="44" pad="44"/>
<connect gate="G$1" pin="45" pad="45"/>
<connect gate="G$1" pin="46" pad="46"/>
<connect gate="G$1" pin="47" pad="47"/>
<connect gate="G$1" pin="48" pad="48"/>
<connect gate="G$1" pin="49" pad="49"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="50" pad="50"/>
<connect gate="G$1" pin="51" pad="51"/>
<connect gate="G$1" pin="52" pad="52"/>
<connect gate="G$1" pin="53" pad="53"/>
<connect gate="G$1" pin="54" pad="54"/>
<connect gate="G$1" pin="55" pad="55"/>
<connect gate="G$1" pin="56" pad="56"/>
<connect gate="G$1" pin="57" pad="57"/>
<connect gate="G$1" pin="58" pad="58"/>
<connect gate="G$1" pin="59" pad="59"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="60" pad="60"/>
<connect gate="G$1" pin="61" pad="61"/>
<connect gate="G$1" pin="62" pad="62"/>
<connect gate="G$1" pin="63" pad="63"/>
<connect gate="G$1" pin="64" pad="64"/>
<connect gate="G$1" pin="65" pad="65"/>
<connect gate="G$1" pin="66" pad="66"/>
<connect gate="G$1" pin="67" pad="67"/>
<connect gate="G$1" pin="68" pad="68"/>
<connect gate="G$1" pin="69" pad="69"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="70" pad="70"/>
<connect gate="G$1" pin="71" pad="71"/>
<connect gate="G$1" pin="72" pad="72"/>
<connect gate="G$1" pin="73" pad="73"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0.2032" drill="0.4064">
<clearance class="0" value="0.2032"/>
</class>
<class number="1" name="USB" width="0.2032" drill="0.4064">
<clearance class="1" value="0.2032"/>
</class>
<class number="2" name="gnd" width="0.2032" drill="0.4064">
<clearance class="2" value="0.2032"/>
</class>
<class number="3" name="usbvcc" width="0.2032" drill="0.4064">
<clearance class="3" value="0.2032"/>
</class>
<class number="4" name="USBpair" width="0.254" drill="0.4064">
<clearance class="4" value="0.6096"/>
</class>
</classes>
<parts>
<part name="SPI" library="SmartPrj" deviceset="PINHD-2X3" device="" value="3x2M"/>
<part name="GND1" library="supply1" deviceset="GND" device=""/>
<part name="PWML" library="SmartPrj" deviceset="PINHD-1X8" device="" value="8x1F-H8.5"/>
<part name="PWMH" library="SmartPrj" deviceset="PINHD-1X10" device="" value="10x1F-H8.5"/>
<part name="GND5" library="supply1" deviceset="GND" device=""/>
<part name="P+4" library="supply1" deviceset="+5V" device=""/>
<part name="FID1" library="SmartPrj" deviceset="FIDUCIALMOUNT" device=""/>
<part name="FID4" library="SmartPrj" deviceset="FIDUCIALMOUNT" device=""/>
<part name="FID3" library="SmartPrj" deviceset="FIDUCIALMOUNT" device=""/>
<part name="POWER" library="SmartPrj" deviceset="PINHD-1X8" device="" value="8x1F-H8.5"/>
<part name="ADCL" library="SmartPrj" deviceset="PINHD-1X8" device="" value="8x1F-H8.5"/>
<part name="COMMUNICATION" library="SmartPrj" deviceset="PINHD-1X8" device="" value="8x1F-H8.5"/>
<part name="+3V3" library="supply1" deviceset="+3V3" device=""/>
<part name="ADCH" library="SmartPrj" deviceset="PINHD-1X8" device="" value="8x1F-H8.5"/>
<part name="FID2" library="SmartPrj" deviceset="FIDUCIALMOUNT" device=""/>
<part name="XIO" library="SmartPrj" deviceset="PINHD-2X18" device="" value="18x2F-H8.5"/>
<part name="GND6" library="supply1" deviceset="GND" device=""/>
<part name="P+12" library="supply1" deviceset="+5V" device=""/>
<part name="P+1" library="supply1" deviceset="+5V" device=""/>
<part name="FRAME1" library="frames" deviceset="A4L-LOC" device=""/>
<part name="LPC1" library="LPCXpresso_boards" deviceset="LPCXPRESSO_LPC1769" device=""/>
</parts>
<sheets>
<sheet>
<plain>
<text x="17.78" y="76.2" size="1.778" layer="91">0</text>
<text x="17.78" y="78.74" size="1.778" layer="91">1</text>
<text x="17.78" y="81.28" size="1.778" layer="91">2</text>
<text x="17.78" y="83.82" size="1.778" layer="91">3</text>
<text x="17.78" y="86.36" size="1.778" layer="91">4</text>
<text x="17.78" y="88.9" size="1.778" layer="91">5</text>
<text x="17.78" y="91.44" size="1.778" layer="91">6</text>
<text x="17.78" y="93.98" size="1.778" layer="91">7</text>
<text x="17.78" y="104.14" size="1.778" layer="91">8</text>
<text x="17.78" y="106.68" size="1.778" layer="91">9</text>
<text x="17.78" y="109.22" size="1.778" layer="91">10</text>
<text x="17.78" y="111.76" size="1.778" layer="91">11</text>
<text x="17.78" y="114.3" size="1.778" layer="91">12</text>
<text x="17.78" y="116.84" size="1.778" layer="91">13</text>
<text x="17.78" y="50.8" size="1.778" layer="91">15</text>
<text x="17.78" y="53.34" size="1.778" layer="91">16</text>
<text x="17.78" y="55.88" size="1.778" layer="91">17</text>
<text x="17.78" y="58.42" size="1.778" layer="91">18</text>
<text x="17.78" y="60.96" size="1.778" layer="91">19</text>
<text x="17.78" y="63.5" size="1.778" layer="91">20</text>
<text x="17.78" y="66.04" size="1.778" layer="91">21</text>
<text x="17.78" y="48.26" size="1.778" layer="91">14</text>
<text x="35.56" y="121.92" size="1.778" layer="91">51</text>
<text x="93.98" y="124.46" size="1.778" layer="91">52</text>
<text x="35.56" y="124.46" size="1.778" layer="91">53</text>
<text x="93.98" y="86.36" size="1.778" layer="91">22</text>
<text x="35.56" y="86.36" size="1.778" layer="91">23</text>
<text x="93.98" y="88.9" size="1.778" layer="91">24</text>
<text x="35.56" y="88.9" size="1.778" layer="91">25</text>
<text x="93.98" y="91.44" size="1.778" layer="91">26</text>
<text x="35.56" y="91.44" size="1.778" layer="91">27</text>
<text x="93.98" y="93.98" size="1.778" layer="91">28</text>
<text x="35.56" y="93.98" size="1.778" layer="91">29</text>
<text x="93.98" y="96.52" size="1.778" layer="91">30</text>
<text x="93.98" y="99.06" size="1.778" layer="91">32</text>
<text x="93.98" y="101.6" size="1.778" layer="91">34</text>
<text x="93.98" y="104.14" size="1.778" layer="91">36</text>
<text x="35.56" y="96.52" size="1.778" layer="91">31</text>
<text x="35.56" y="99.06" size="1.778" layer="91">33</text>
<text x="35.56" y="101.6" size="1.778" layer="91">35</text>
<text x="35.56" y="104.14" size="1.778" layer="91">37</text>
<text x="35.56" y="119.38" size="1.778" layer="91">49</text>
<text x="35.56" y="116.84" size="1.778" layer="91">47</text>
<text x="35.56" y="114.3" size="1.778" layer="91">45</text>
<text x="35.56" y="111.76" size="1.778" layer="91">43</text>
<text x="35.56" y="109.22" size="1.778" layer="91">41</text>
<text x="35.56" y="106.68" size="1.778" layer="91">39</text>
<text x="93.98" y="121.92" size="1.778" layer="91">50</text>
<text x="93.98" y="119.38" size="1.778" layer="91">48</text>
<text x="93.98" y="116.84" size="1.778" layer="91">46</text>
<text x="93.98" y="114.3" size="1.778" layer="91">44</text>
<text x="93.98" y="111.76" size="1.778" layer="91">42</text>
<text x="93.98" y="109.22" size="1.778" layer="91">40</text>
<text x="93.98" y="106.68" size="1.778" layer="91">38</text>
<text x="20.955" y="88.265" size="1.778" layer="97">TWI pullups</text>
<text x="22.86" y="66.04" size="1.6764" layer="91">TWCK1</text>
<text x="22.86" y="63.5" size="1.6764" layer="91">TWD1</text>
<text x="83.82" y="53.34" size="1.778" layer="91">IOREF</text>
<text x="17.78" y="127" size="1.6764" layer="91">TWCK0</text>
<text x="17.78" y="124.46" size="1.6764" layer="91">TWD0</text>
<text x="17.78" y="-5.08" size="1.778" layer="91">AD1</text>
<text x="17.78" y="-2.54" size="1.778" layer="91">AD2</text>
<text x="17.78" y="0" size="1.778" layer="91">AD3</text>
<text x="17.78" y="2.54" size="1.778" layer="91">AD4</text>
<text x="17.78" y="5.08" size="1.778" layer="91">AD5</text>
<text x="17.78" y="7.62" size="1.778" layer="91">AD6</text>
<text x="17.78" y="-7.62" size="1.778" layer="91">AD0</text>
<text x="17.78" y="121.92" size="1.778" layer="91">AREF</text>
<text x="17.78" y="10.16" size="1.778" layer="91">AD7</text>
<text x="22.86" y="86.36" size="1.778" layer="91">SD-CS</text>
<text x="22.86" y="78.74" size="1.778" layer="91">UTXD</text>
<text x="22.86" y="76.2" size="1.778" layer="91">URXD</text>
<text x="22.86" y="109.22" size="1.778" layer="91">ETH-CS</text>
<text x="-12.7" y="137.16" size="10.16" layer="94">Arduino   DUE pines de placa prototipo</text>
</plain>
<instances>
<instance part="SPI" gate="A" x="12.7" y="27.94"/>
<instance part="GND1" gate="1" x="35.56" y="22.86"/>
<instance part="PWML" gate="A" x="12.7" y="86.36" smashed="yes" rot="MR180">
<attribute name="NAME" x="6.985" y="98.425" size="1.778" layer="95" rot="MR180"/>
<attribute name="VALUE" x="6.35" y="73.66" size="1.778" layer="96" rot="MR180"/>
</instance>
<instance part="PWMH" gate="A" x="12.7" y="114.3" smashed="yes" rot="MR180">
<attribute name="NAME" x="7.62" y="132.08" size="1.778" layer="95" rot="MR180"/>
<attribute name="VALUE" x="1.27" y="101.6" size="1.778" layer="96" rot="MR180"/>
</instance>
<instance part="GND5" gate="1" x="58.42" y="33.02"/>
<instance part="P+4" gate="1" x="58.42" y="63.5" smashed="yes">
<attribute name="VALUE" x="60.325" y="66.04" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="FID1" gate="G$1" x="88.9" y="76.2"/>
<instance part="FID4" gate="G$1" x="96.52" y="68.58"/>
<instance part="FID3" gate="G$1" x="88.9" y="68.58"/>
<instance part="POWER" gate="A" x="81.28" y="45.72"/>
<instance part="ADCL" gate="A" x="12.7" y="2.54" smashed="yes" rot="MR180">
<attribute name="NAME" x="7.62" y="15.24" size="1.778" layer="95" rot="MR180"/>
<attribute name="VALUE" x="3.81" y="-10.16" size="1.778" layer="96" rot="MR180"/>
</instance>
<instance part="COMMUNICATION" gate="A" x="12.7" y="58.42" smashed="yes" rot="MR180">
<attribute name="NAME" x="1.905" y="71.12" size="1.778" layer="95" rot="MR180"/>
<attribute name="VALUE" x="3.81" y="45.72" size="1.778" layer="96" rot="MR180"/>
</instance>
<instance part="+3V3" gate="G$1" x="63.5" y="63.5" smashed="yes">
<attribute name="VALUE" x="66.04" y="66.04" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="ADCH" gate="A" x="63.5" y="2.54" smashed="yes" rot="MR180">
<attribute name="NAME" x="58.42" y="15.24" size="1.778" layer="95" rot="MR180"/>
<attribute name="VALUE" x="54.61" y="-10.16" size="1.778" layer="96" rot="MR180"/>
</instance>
<instance part="FID2" gate="G$1" x="96.52" y="76.2"/>
<instance part="XIO" gate="A" x="66.04" y="104.14" rot="R180"/>
<instance part="GND6" gate="1" x="78.74" y="132.08" rot="R90"/>
<instance part="P+12" gate="1" x="81.28" y="81.28" smashed="yes">
<attribute name="VALUE" x="83.185" y="83.82" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="P+1" gate="1" x="35.56" y="33.02" smashed="yes">
<attribute name="VALUE" x="37.465" y="35.56" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="FRAME1" gate="G$1" x="-20.32" y="-20.32"/>
<instance part="LPC1" gate="G$1" x="170.18" y="88.9"/>
</instances>
<busses>
</busses>
<nets>
<net name="+5V" class="0">
<segment>
<wire x1="78.74" y1="45.72" x2="58.42" y2="45.72" width="0.1524" layer="91"/>
<wire x1="58.42" y1="45.72" x2="58.42" y2="60.96" width="0.1524" layer="91"/>
<pinref part="P+4" gate="1" pin="+5V"/>
<pinref part="POWER" gate="A" pin="5"/>
</segment>
<segment>
<wire x1="60.96" y1="83.82" x2="53.34" y2="83.82" width="0.1524" layer="91"/>
<wire x1="53.34" y1="83.82" x2="53.34" y2="78.74" width="0.1524" layer="91"/>
<wire x1="53.34" y1="78.74" x2="76.2" y2="78.74" width="0.1524" layer="91"/>
<wire x1="76.2" y1="78.74" x2="76.2" y2="83.82" width="0.1524" layer="91"/>
<wire x1="76.2" y1="83.82" x2="68.58" y2="83.82" width="0.1524" layer="91"/>
<wire x1="81.28" y1="78.74" x2="76.2" y2="78.74" width="0.1524" layer="91"/>
<junction x="76.2" y="78.74"/>
<pinref part="XIO" gate="A" pin="2"/>
<pinref part="XIO" gate="A" pin="1"/>
<pinref part="P+12" gate="1" pin="+5V"/>
</segment>
<segment>
<pinref part="SPI" gate="A" pin="2"/>
<wire x1="17.78" y1="30.48" x2="35.56" y2="30.48" width="0.1524" layer="91"/>
<pinref part="P+1" gate="1" pin="+5V"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<wire x1="35.56" y1="25.4" x2="17.78" y2="25.4" width="0.1524" layer="91"/>
<pinref part="GND1" gate="1" pin="GND"/>
<pinref part="SPI" gate="A" pin="6"/>
</segment>
<segment>
<wire x1="10.16" y1="119.38" x2="-10.16" y2="119.38" width="0.1524" layer="91"/>
<label x="-7.62" y="119.38" size="1.778" layer="95"/>
<pinref part="PWMH" gate="A" pin="7"/>
</segment>
<segment>
<wire x1="78.74" y1="40.64" x2="73.66" y2="40.64" width="0.1524" layer="91"/>
<wire x1="78.74" y1="43.18" x2="73.66" y2="43.18" width="0.1524" layer="91"/>
<wire x1="73.66" y1="43.18" x2="73.66" y2="40.64" width="0.1524" layer="91"/>
<wire x1="73.66" y1="40.64" x2="58.42" y2="40.64" width="0.1524" layer="91"/>
<wire x1="58.42" y1="40.64" x2="58.42" y2="35.56" width="0.1524" layer="91"/>
<junction x="73.66" y="40.64"/>
<pinref part="POWER" gate="A" pin="7"/>
<pinref part="POWER" gate="A" pin="6"/>
<pinref part="GND5" gate="1" pin="GND"/>
</segment>
<segment>
<wire x1="60.96" y1="127" x2="53.34" y2="127" width="0.1524" layer="91"/>
<wire x1="53.34" y1="127" x2="53.34" y2="134.62" width="0.1524" layer="91"/>
<wire x1="53.34" y1="134.62" x2="76.2" y2="134.62" width="0.1524" layer="91"/>
<wire x1="76.2" y1="134.62" x2="76.2" y2="132.08" width="0.1524" layer="91"/>
<wire x1="76.2" y1="132.08" x2="76.2" y2="127" width="0.1524" layer="91"/>
<wire x1="76.2" y1="127" x2="68.58" y2="127" width="0.1524" layer="91"/>
<junction x="76.2" y="132.08"/>
<pinref part="XIO" gate="A" pin="36"/>
<pinref part="XIO" gate="A" pin="35"/>
<pinref part="GND6" gate="1" pin="GND"/>
</segment>
</net>
<net name="VIN" class="0">
<segment>
<wire x1="78.74" y1="38.1" x2="63.5" y2="38.1" width="0.1524" layer="91"/>
<label x="63.5" y="38.1" size="1.778" layer="95"/>
<pinref part="POWER" gate="A" pin="8"/>
</segment>
</net>
<net name="+3V3" class="0">
<segment>
<wire x1="78.74" y1="48.26" x2="68.58" y2="48.26" width="0.1524" layer="91"/>
<wire x1="68.58" y1="48.26" x2="63.5" y2="48.26" width="0.1524" layer="91"/>
<wire x1="63.5" y1="48.26" x2="63.5" y2="60.96" width="0.1524" layer="91"/>
<wire x1="78.74" y1="53.34" x2="68.58" y2="53.34" width="0.1524" layer="91"/>
<wire x1="68.58" y1="53.34" x2="68.58" y2="48.26" width="0.1524" layer="91"/>
<junction x="68.58" y="48.26"/>
<pinref part="+3V3" gate="G$1" pin="+3V3"/>
<pinref part="POWER" gate="A" pin="4"/>
<pinref part="POWER" gate="A" pin="2"/>
</segment>
</net>
<net name="PIN51" class="0">
<segment>
<wire x1="40.64" y1="121.92" x2="60.96" y2="121.92" width="0.1524" layer="91"/>
<label x="40.64" y="121.92" size="1.778" layer="95"/>
<pinref part="XIO" gate="A" pin="32"/>
</segment>
</net>
<net name="PIN49" class="0">
<segment>
<wire x1="40.64" y1="119.38" x2="60.96" y2="119.38" width="0.1524" layer="91"/>
<label x="40.64" y="119.38" size="1.778" layer="95"/>
<pinref part="XIO" gate="A" pin="30"/>
</segment>
</net>
<net name="PIN47" class="0">
<segment>
<wire x1="40.64" y1="116.84" x2="60.96" y2="116.84" width="0.1524" layer="91"/>
<label x="40.64" y="116.84" size="1.778" layer="95"/>
<pinref part="XIO" gate="A" pin="28"/>
</segment>
</net>
<net name="PIN45" class="0">
<segment>
<wire x1="40.64" y1="114.3" x2="60.96" y2="114.3" width="0.1524" layer="91"/>
<label x="40.64" y="114.3" size="1.778" layer="95"/>
<pinref part="XIO" gate="A" pin="26"/>
</segment>
</net>
<net name="PIN43" class="0">
<segment>
<wire x1="40.64" y1="111.76" x2="60.96" y2="111.76" width="0.1524" layer="91"/>
<label x="40.64" y="111.76" size="1.778" layer="95"/>
<pinref part="XIO" gate="A" pin="24"/>
</segment>
</net>
<net name="PIN41" class="0">
<segment>
<wire x1="40.64" y1="109.22" x2="60.96" y2="109.22" width="0.1524" layer="91"/>
<label x="40.64" y="109.22" size="1.778" layer="95"/>
<pinref part="XIO" gate="A" pin="22"/>
</segment>
</net>
<net name="PIN39" class="0">
<segment>
<wire x1="40.64" y1="106.68" x2="60.96" y2="106.68" width="0.1524" layer="91"/>
<label x="40.64" y="106.68" size="1.778" layer="95"/>
<pinref part="XIO" gate="A" pin="20"/>
</segment>
</net>
<net name="PIN37" class="0">
<segment>
<wire x1="40.64" y1="104.14" x2="60.96" y2="104.14" width="0.1524" layer="91"/>
<label x="40.64" y="104.14" size="1.778" layer="95"/>
<pinref part="XIO" gate="A" pin="18"/>
</segment>
</net>
<net name="PIN35" class="0">
<segment>
<wire x1="40.64" y1="101.6" x2="60.96" y2="101.6" width="0.1524" layer="91"/>
<label x="40.64" y="101.6" size="1.778" layer="95"/>
<pinref part="XIO" gate="A" pin="16"/>
</segment>
</net>
<net name="PIN33" class="0">
<segment>
<wire x1="40.64" y1="99.06" x2="60.96" y2="99.06" width="0.1524" layer="91"/>
<label x="40.64" y="99.06" size="1.778" layer="95"/>
<pinref part="XIO" gate="A" pin="14"/>
</segment>
</net>
<net name="PIN25" class="0">
<segment>
<wire x1="40.64" y1="88.9" x2="60.96" y2="88.9" width="0.1524" layer="91"/>
<label x="40.64" y="88.9" size="1.778" layer="95"/>
<pinref part="XIO" gate="A" pin="6"/>
</segment>
</net>
<net name="PIN24" class="0">
<segment>
<wire x1="91.44" y1="88.9" x2="68.58" y2="88.9" width="0.1524" layer="91"/>
<label x="76.2" y="88.9" size="1.778" layer="95"/>
<pinref part="XIO" gate="A" pin="5"/>
</segment>
</net>
<net name="PIN32" class="0">
<segment>
<wire x1="91.44" y1="99.06" x2="68.58" y2="99.06" width="0.1524" layer="91"/>
<label x="76.2" y="99.06" size="1.778" layer="95"/>
<pinref part="XIO" gate="A" pin="13"/>
</segment>
</net>
<net name="PIN34" class="0">
<segment>
<wire x1="91.44" y1="101.6" x2="68.58" y2="101.6" width="0.1524" layer="91"/>
<label x="76.2" y="101.6" size="1.778" layer="95"/>
<pinref part="XIO" gate="A" pin="15"/>
</segment>
</net>
<net name="PIN36" class="0">
<segment>
<wire x1="91.44" y1="104.14" x2="68.58" y2="104.14" width="0.1524" layer="91"/>
<label x="76.2" y="104.14" size="1.778" layer="95"/>
<pinref part="XIO" gate="A" pin="17"/>
</segment>
</net>
<net name="PIN38" class="0">
<segment>
<wire x1="91.44" y1="106.68" x2="68.58" y2="106.68" width="0.1524" layer="91"/>
<label x="76.2" y="106.68" size="1.778" layer="95"/>
<pinref part="XIO" gate="A" pin="19"/>
</segment>
</net>
<net name="PIN40" class="0">
<segment>
<wire x1="91.44" y1="109.22" x2="68.58" y2="109.22" width="0.1524" layer="91"/>
<label x="76.2" y="109.22" size="1.778" layer="95"/>
<pinref part="XIO" gate="A" pin="21"/>
</segment>
</net>
<net name="PIN42" class="0">
<segment>
<wire x1="91.44" y1="111.76" x2="68.58" y2="111.76" width="0.1524" layer="91"/>
<label x="76.2" y="111.76" size="1.778" layer="95"/>
<pinref part="XIO" gate="A" pin="23"/>
</segment>
</net>
<net name="PIN44" class="0">
<segment>
<wire x1="91.44" y1="114.3" x2="68.58" y2="114.3" width="0.1524" layer="91"/>
<label x="76.2" y="114.3" size="1.778" layer="95"/>
<pinref part="XIO" gate="A" pin="25"/>
</segment>
</net>
<net name="PIN46" class="0">
<segment>
<wire x1="91.44" y1="116.84" x2="68.58" y2="116.84" width="0.1524" layer="91"/>
<label x="76.2" y="116.84" size="1.778" layer="95"/>
<pinref part="XIO" gate="A" pin="27"/>
</segment>
</net>
<net name="PIN48" class="0">
<segment>
<wire x1="91.44" y1="119.38" x2="68.58" y2="119.38" width="0.1524" layer="91"/>
<label x="76.2" y="119.38" size="1.778" layer="95"/>
<pinref part="XIO" gate="A" pin="29"/>
</segment>
</net>
<net name="AD2" class="0">
<segment>
<wire x1="-10.16" y1="-2.54" x2="10.16" y2="-2.54" width="0.1524" layer="91"/>
<label x="-7.62" y="-2.54" size="1.778" layer="95"/>
<pinref part="ADCL" gate="A" pin="3"/>
</segment>
</net>
<net name="AD3" class="0">
<segment>
<wire x1="-10.16" y1="0" x2="10.16" y2="0" width="0.1524" layer="91"/>
<label x="-7.62" y="0" size="1.778" layer="95"/>
<pinref part="ADCL" gate="A" pin="4"/>
</segment>
</net>
<net name="AD4" class="0">
<segment>
<wire x1="-10.16" y1="2.54" x2="10.16" y2="2.54" width="0.1524" layer="91"/>
<label x="-7.62" y="2.54" size="1.778" layer="95"/>
<pinref part="ADCL" gate="A" pin="5"/>
</segment>
</net>
<net name="AD5" class="0">
<segment>
<wire x1="-10.16" y1="5.08" x2="10.16" y2="5.08" width="0.1524" layer="91"/>
<label x="-7.62" y="5.08" size="1.778" layer="95"/>
<pinref part="ADCL" gate="A" pin="6"/>
</segment>
</net>
<net name="AD6" class="0">
<segment>
<wire x1="-10.16" y1="7.62" x2="10.16" y2="7.62" width="0.1524" layer="91"/>
<label x="-7.62" y="7.62" size="1.778" layer="95"/>
<pinref part="ADCL" gate="A" pin="7"/>
</segment>
</net>
<net name="AD7" class="0">
<segment>
<wire x1="-10.16" y1="10.16" x2="10.16" y2="10.16" width="0.1524" layer="91"/>
<label x="-7.62" y="10.16" size="1.778" layer="95"/>
<pinref part="ADCL" gate="A" pin="8"/>
</segment>
</net>
<net name="SCL0-3" class="0">
<segment>
<wire x1="10.16" y1="66.04" x2="-7.62" y2="66.04" width="0.1524" layer="91"/>
<label x="-5.08" y="66.04" size="1.778" layer="95"/>
<pinref part="COMMUNICATION" gate="A" pin="8"/>
</segment>
</net>
<net name="SDA0-3" class="0">
<segment>
<wire x1="-7.62" y1="63.5" x2="10.16" y2="63.5" width="0.1524" layer="91"/>
<label x="-5.08" y="63.5" size="1.778" layer="95"/>
<pinref part="COMMUNICATION" gate="A" pin="7"/>
</segment>
</net>
<net name="RXD0" class="0">
<segment>
<wire x1="-7.62" y1="50.8" x2="10.16" y2="50.8" width="0.1524" layer="91"/>
<label x="-5.08" y="50.8" size="1.778" layer="95"/>
<pinref part="COMMUNICATION" gate="A" pin="2"/>
</segment>
</net>
<net name="TXD0" class="0">
<segment>
<wire x1="-7.62" y1="48.26" x2="10.16" y2="48.26" width="0.1524" layer="91"/>
<label x="-5.08" y="48.26" size="1.778" layer="95"/>
<pinref part="COMMUNICATION" gate="A" pin="1"/>
</segment>
</net>
<net name="RXD1" class="0">
<segment>
<wire x1="-7.62" y1="55.88" x2="10.16" y2="55.88" width="0.1524" layer="91"/>
<label x="-5.08" y="55.88" size="1.778" layer="95"/>
<pinref part="COMMUNICATION" gate="A" pin="4"/>
</segment>
</net>
<net name="TXD1" class="0">
<segment>
<wire x1="-7.62" y1="53.34" x2="10.16" y2="53.34" width="0.1524" layer="91"/>
<label x="-5.08" y="53.34" size="1.778" layer="95"/>
<pinref part="COMMUNICATION" gate="A" pin="3"/>
</segment>
</net>
<net name="TXD2" class="0">
<segment>
<wire x1="-7.62" y1="58.42" x2="10.16" y2="58.42" width="0.1524" layer="91"/>
<label x="-5.08" y="58.42" size="1.778" layer="95"/>
<pinref part="COMMUNICATION" gate="A" pin="5"/>
</segment>
</net>
<net name="SDA1" class="0">
<segment>
<wire x1="10.16" y1="124.46" x2="-10.16" y2="124.46" width="0.1524" layer="91"/>
<label x="-7.62" y="124.46" size="1.778" layer="95"/>
<pinref part="PWMH" gate="A" pin="9"/>
</segment>
</net>
<net name="SCL1" class="0">
<segment>
<wire x1="-10.16" y1="127" x2="10.16" y2="127" width="0.1524" layer="91"/>
<label x="-7.62" y="127" size="1.778" layer="95"/>
<pinref part="PWMH" gate="A" pin="10"/>
</segment>
</net>
<net name="MISO" class="0">
<segment>
<wire x1="-15.24" y1="30.48" x2="10.16" y2="30.48" width="0.1524" layer="91"/>
<label x="-12.7" y="30.48" size="1.778" layer="95"/>
<pinref part="SPI" gate="A" pin="1"/>
</segment>
</net>
<net name="MOSI" class="0">
<segment>
<wire x1="17.78" y1="27.94" x2="35.56" y2="27.94" width="0.1524" layer="91"/>
<label x="22.86" y="27.94" size="1.778" layer="95"/>
<pinref part="SPI" gate="A" pin="4"/>
</segment>
</net>
<net name="SPCK" class="0">
<segment>
<wire x1="10.16" y1="27.94" x2="-15.24" y2="27.94" width="0.1524" layer="91"/>
<label x="-12.7" y="27.94" size="1.778" layer="95"/>
<pinref part="SPI" gate="A" pin="3"/>
</segment>
</net>
<net name="SS1/PWM4" class="0">
<segment>
<wire x1="-10.16" y1="86.36" x2="10.16" y2="86.36" width="0.1524" layer="91"/>
<pinref part="PWML" gate="A" pin="5"/>
<label x="-7.62" y="86.36" size="1.778" layer="95"/>
</segment>
</net>
<net name="PWM11" class="0">
<segment>
<wire x1="10.16" y1="111.76" x2="-10.16" y2="111.76" width="0.1524" layer="91"/>
<label x="-7.62" y="111.76" size="1.778" layer="95"/>
<pinref part="PWMH" gate="A" pin="4"/>
</segment>
</net>
<net name="PWM12" class="0">
<segment>
<wire x1="-10.16" y1="114.3" x2="10.16" y2="114.3" width="0.1524" layer="91"/>
<label x="-7.62" y="114.3" size="1.778" layer="95"/>
<pinref part="PWMH" gate="A" pin="5"/>
</segment>
</net>
<net name="PWM9" class="0">
<segment>
<wire x1="-10.16" y1="106.68" x2="10.16" y2="106.68" width="0.1524" layer="91"/>
<label x="-7.62" y="106.68" size="1.778" layer="95"/>
<pinref part="PWMH" gate="A" pin="2"/>
</segment>
</net>
<net name="PWM8" class="0">
<segment>
<wire x1="-10.16" y1="104.14" x2="10.16" y2="104.14" width="0.1524" layer="91"/>
<label x="-7.62" y="104.14" size="1.778" layer="95"/>
<pinref part="PWMH" gate="A" pin="1"/>
</segment>
</net>
<net name="PWM7" class="0">
<segment>
<wire x1="-10.16" y1="93.98" x2="10.16" y2="93.98" width="0.1524" layer="91"/>
<pinref part="PWML" gate="A" pin="8"/>
<label x="-7.62" y="93.98" size="1.778" layer="95"/>
</segment>
</net>
<net name="PWM6" class="0">
<segment>
<wire x1="-10.16" y1="91.44" x2="10.16" y2="91.44" width="0.1524" layer="91"/>
<pinref part="PWML" gate="A" pin="7"/>
<label x="-7.62" y="91.44" size="1.778" layer="95"/>
</segment>
</net>
<net name="PWM2" class="0">
<segment>
<wire x1="-10.16" y1="81.28" x2="10.16" y2="81.28" width="0.1524" layer="91"/>
<pinref part="PWML" gate="A" pin="3"/>
<label x="-7.62" y="81.28" size="1.778" layer="95"/>
</segment>
</net>
<net name="PWM5" class="0">
<segment>
<wire x1="10.16" y1="88.9" x2="-10.16" y2="88.9" width="0.1524" layer="91"/>
<pinref part="PWML" gate="A" pin="6"/>
<label x="-7.62" y="88.9" size="1.778" layer="95"/>
</segment>
</net>
<net name="RX" class="0">
<segment>
<wire x1="10.16" y1="76.2" x2="-10.16" y2="76.2" width="0.1524" layer="91"/>
<label x="-7.62" y="76.2" size="1.778" layer="95"/>
<pinref part="PWML" gate="A" pin="1"/>
</segment>
</net>
<net name="TX" class="0">
<segment>
<wire x1="-10.16" y1="78.74" x2="10.16" y2="78.74" width="0.1524" layer="91"/>
<label x="-7.62" y="78.74" size="1.778" layer="95"/>
<pinref part="PWML" gate="A" pin="2"/>
</segment>
</net>
<net name="PIN50" class="0">
<segment>
<wire x1="91.44" y1="121.92" x2="68.58" y2="121.92" width="0.1524" layer="91"/>
<label x="76.2" y="121.92" size="1.778" layer="95"/>
<pinref part="XIO" gate="A" pin="31"/>
</segment>
</net>
<net name="AD1" class="0">
<segment>
<wire x1="-10.16" y1="-5.08" x2="10.16" y2="-5.08" width="0.1524" layer="91"/>
<label x="-7.62" y="-5.08" size="1.778" layer="95"/>
<pinref part="ADCL" gate="A" pin="2"/>
</segment>
</net>
<net name="AD0" class="0">
<segment>
<wire x1="-10.16" y1="-7.62" x2="10.16" y2="-7.62" width="0.1524" layer="91"/>
<label x="-7.62" y="-7.62" size="1.778" layer="95"/>
<pinref part="ADCL" gate="A" pin="1"/>
</segment>
</net>
<net name="AD8" class="0">
<segment>
<wire x1="38.1" y1="-7.62" x2="60.96" y2="-7.62" width="0.1524" layer="91"/>
<label x="40.64" y="-7.62" size="1.778" layer="95"/>
<pinref part="ADCH" gate="A" pin="1"/>
</segment>
</net>
<net name="AD9" class="0">
<segment>
<wire x1="38.1" y1="-5.08" x2="60.96" y2="-5.08" width="0.1524" layer="91"/>
<label x="40.64" y="-5.08" size="1.778" layer="95"/>
<pinref part="ADCH" gate="A" pin="2"/>
</segment>
</net>
<net name="AD10" class="0">
<segment>
<wire x1="38.1" y1="-2.54" x2="60.96" y2="-2.54" width="0.1524" layer="91"/>
<label x="40.64" y="-2.54" size="1.778" layer="95"/>
<pinref part="ADCH" gate="A" pin="3"/>
</segment>
</net>
<net name="AREF" class="0">
<segment>
<wire x1="-10.16" y1="121.92" x2="10.16" y2="121.92" width="0.1524" layer="91"/>
<label x="-7.62" y="121.92" size="1.778" layer="95"/>
<pinref part="PWMH" gate="A" pin="8"/>
</segment>
</net>
<net name="MASTER-RESET" class="0">
<segment>
<wire x1="10.16" y1="25.4" x2="-15.24" y2="25.4" width="0.1524" layer="91"/>
<label x="-12.7" y="25.4" size="1.778" layer="95"/>
<pinref part="SPI" gate="A" pin="5"/>
</segment>
<segment>
<label x="45.72" y="50.8" size="1.778" layer="95"/>
<pinref part="POWER" gate="A" pin="3"/>
<wire x1="78.74" y1="50.8" x2="45.72" y2="50.8" width="0.1524" layer="91"/>
</segment>
</net>
<net name="DAC1" class="0">
<segment>
<pinref part="ADCH" gate="A" pin="6"/>
<wire x1="38.1" y1="5.08" x2="60.96" y2="5.08" width="0.1524" layer="91"/>
<label x="40.64" y="5.08" size="1.778" layer="95"/>
</segment>
</net>
<net name="CANTX0" class="0">
<segment>
<pinref part="ADCH" gate="A" pin="8"/>
<wire x1="38.1" y1="10.16" x2="60.96" y2="10.16" width="0.1524" layer="91"/>
<label x="40.64" y="10.16" size="1.778" layer="95"/>
</segment>
</net>
<net name="CANRX0" class="0">
<segment>
<pinref part="ADCH" gate="A" pin="7"/>
<wire x1="38.1" y1="7.62" x2="60.96" y2="7.62" width="0.1524" layer="91"/>
<label x="40.64" y="7.62" size="1.778" layer="95"/>
</segment>
</net>
<net name="CANTX1/IO" class="0">
<segment>
<wire x1="40.64" y1="124.46" x2="60.96" y2="124.46" width="0.1524" layer="91"/>
<label x="40.64" y="124.46" size="1.778" layer="95"/>
<pinref part="XIO" gate="A" pin="34"/>
</segment>
</net>
<net name="DAC0(CANRX1)" class="0">
<segment>
<pinref part="ADCH" gate="A" pin="5"/>
<wire x1="38.1" y1="2.54" x2="60.96" y2="2.54" width="0.1524" layer="91"/>
<label x="40.64" y="2.54" size="1.778" layer="95"/>
</segment>
</net>
<net name="RXD2" class="0">
<segment>
<wire x1="-7.62" y1="60.96" x2="10.16" y2="60.96" width="0.1524" layer="91"/>
<label x="-5.08" y="60.96" size="1.778" layer="95"/>
<pinref part="COMMUNICATION" gate="A" pin="6"/>
</segment>
</net>
<net name="AD14(RXD3)" class="0">
<segment>
<wire x1="91.44" y1="124.46" x2="68.58" y2="124.46" width="0.1524" layer="91"/>
<label x="76.2" y="124.46" size="1.778" layer="95"/>
<pinref part="XIO" gate="A" pin="33"/>
</segment>
</net>
<net name="AD11(TXD3)" class="0">
<segment>
<pinref part="ADCH" gate="A" pin="4"/>
<wire x1="60.96" y1="0" x2="38.1" y2="0" width="0.1524" layer="91"/>
<label x="40.64" y="0" size="1.778" layer="95"/>
</segment>
</net>
<net name="SS0/PWM10" class="0">
<segment>
<wire x1="10.16" y1="109.22" x2="-10.16" y2="109.22" width="0.1524" layer="91"/>
<label x="-7.62" y="109.22" size="1.778" layer="95"/>
<pinref part="PWMH" gate="A" pin="3"/>
</segment>
</net>
<net name="PWM3" class="0">
<segment>
<wire x1="-10.16" y1="83.82" x2="10.16" y2="83.82" width="0.1524" layer="91"/>
<label x="-7.62" y="83.82" size="1.778" layer="95"/>
<pinref part="PWML" gate="A" pin="4"/>
</segment>
</net>
<net name="PWM13" class="0">
<segment>
<wire x1="-10.16" y1="116.84" x2="10.16" y2="116.84" width="0.1524" layer="91"/>
<label x="-7.62" y="116.84" size="1.778" layer="95"/>
<pinref part="PWMH" gate="A" pin="6"/>
</segment>
</net>
<net name="PIN30" class="0">
<segment>
<wire x1="91.44" y1="96.52" x2="68.58" y2="96.52" width="0.1524" layer="91"/>
<label x="76.2" y="96.52" size="1.778" layer="95"/>
<pinref part="XIO" gate="A" pin="11"/>
</segment>
</net>
<net name="PIN28" class="0">
<segment>
<wire x1="91.44" y1="93.98" x2="68.58" y2="93.98" width="0.1524" layer="91"/>
<label x="76.2" y="93.98" size="1.778" layer="95"/>
<pinref part="XIO" gate="A" pin="9"/>
</segment>
</net>
<net name="PIN26" class="0">
<segment>
<wire x1="91.44" y1="91.44" x2="68.58" y2="91.44" width="0.1524" layer="91"/>
<label x="76.2" y="91.44" size="1.778" layer="95"/>
<pinref part="XIO" gate="A" pin="7"/>
</segment>
</net>
<net name="PIN22" class="0">
<segment>
<wire x1="91.44" y1="86.36" x2="68.58" y2="86.36" width="0.1524" layer="91"/>
<label x="76.2" y="86.36" size="1.778" layer="95"/>
<pinref part="XIO" gate="A" pin="3"/>
</segment>
</net>
<net name="PIN31" class="0">
<segment>
<wire x1="40.64" y1="96.52" x2="60.96" y2="96.52" width="0.1524" layer="91"/>
<label x="40.64" y="96.52" size="1.778" layer="95"/>
<pinref part="XIO" gate="A" pin="12"/>
</segment>
</net>
<net name="PIN29" class="0">
<segment>
<wire x1="40.64" y1="93.98" x2="60.96" y2="93.98" width="0.1524" layer="91"/>
<label x="40.64" y="93.98" size="1.778" layer="95"/>
<pinref part="XIO" gate="A" pin="10"/>
</segment>
</net>
<net name="PIN27" class="0">
<segment>
<wire x1="40.64" y1="91.44" x2="60.96" y2="91.44" width="0.1524" layer="91"/>
<label x="40.64" y="91.44" size="1.778" layer="95"/>
<pinref part="XIO" gate="A" pin="8"/>
</segment>
</net>
<net name="PIN23" class="0">
<segment>
<wire x1="40.64" y1="86.36" x2="60.96" y2="86.36" width="0.1524" layer="91"/>
<label x="40.64" y="86.36" size="1.778" layer="95"/>
<pinref part="XIO" gate="A" pin="4"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
</eagle>
